var A_;
A_ = {
    devel:true,
    forms:{
        /**
         *
         * @param form
         * @param layout
         */
        send:function (form, layout) {
            if (!layout) {
                layout = 'default'
            }
            /**
             * tinymce yeni sürümde kaydetmeden önce son durumunu textarea ya aktarmak için böyle bişi lazımmış.
             */
            if(typeof(tinyMCE)!=='undefined'){
                tinyMCE.triggerSave();
            }
            /**//**/
            //Form verileri
            var datas = form.serialize();
            //İstek yapılacak dinamik dosya
            var url = form.attr('action');
            //İsteğin türü
            var method = form.attr('method');
            url = '/ajax.php?' + url;
            //İşlem başarıyla sonuçlandığında çalıştırılacak method
            var fnc = form.attr('function');
            //İşlem başarıyla sonuçlandığın formu sıfırlamak ister misin ?
            var reset = form.attr('reset');
            //Önyükleme animasyonu
            var loader = form.attr('loader');
            if (!loader) loader = "#loader";
            //İşlem başarıyla sonuçlandığında sayfayı yönlendireceği yer.
            var forward = form.attr('forward');
            //Uyarı mesajları gösterilsin mi ?
            var alert = form.attr('alert');
            //Form gönderimi başladığında pasif olmasını istenen kısım (gönder tuşu gibi)
            var disable = form.attr('disable');
            //Formun sonucunu jAlert ile değilde fancybox yada başka birşeyler göstermek isteyebiliriz diye. (_fancybox , _blank)
            var target = form.attr('target');
            //Formun sonucunda yüklenecek olan modülün geçirileceği div idsi
            //Otomatik olarak formun içindeki ilk divin idsini alır.
            var module = $("div", form).attr('id');
            $.ajax({
                type:method,
                data:datas,
                url:url,
                async:false,
                beforeSend:function () {
                    if (loader) {
                        $(loader).show();
                    }
                    if (disable) {
                        $(disable).attr('disabled', 'disabled').animate({
                            opacity:0.3
                        });
                    }
                },
                success:function (data) {
                    try{
                        data = JSON.parse(data);
                    }catch (e){
                        data = {
                            type:"fatal",
                            msg:"Sonuç formatı hatalı!"
                        }
                    }
                    if (data.type === "success") {
                        if(!data.xtra){
                         data.xtra=new Array();
                        }
                        if (fnc) {
                            eval(fnc); //eval is evil
                            if (A_.devel) console.log('AfterSubmit çalışıtırıldı.');
                            if (loader) {
                                $(loader).hide();
                            }
                            if (data.xtra.redirect) {
                                window.location = data.xtra.redirect;
                            } else if (forward) {
                                window.location = forward;
                            }
                        } else {
                            switch (target) {
                                case '_blank':
                                    alert(data.msg);
                                    break;
                                case '_fancybox':
                                    $.fancybox(data.msg);
                                    break;
                                case '_module':
                                    $("#" + module).html(data.msg);
                                    if (fnc) {
                                        eval(fnc);//eval is evil :)
                                    }
                                    break;
                                default:
                                    if (alert !== 'false') {
                                        jAlert(data.msg, data.title, function (r) {
                                            if (A_.devel) console.log('Uyarı dönüşü alındı.');
                                            if (reset !== 'false') {
                                                A_.forms.reset();
                                            }
                                            if (data.xtra.redirect) {
                                                window.location = data.xtra.redirect;
                                            }
                                            else if (forward) {
                                                window.location = forward;
                                            }
                                        });
                                    } else {
                                        if (reset !== 'false') A_.forms.reset();

                                        if (data.xtra.redirect) {
                                            window.location = data.xtra.redirect;
                                        }
                                        else if (forward) {
                                            window.location = forward;
                                        }
                                    }
                                    break;
                            }
                            if (loader) {
                                $(loader).hide();
                            }
                            if (reset && disable) {
                                $(disable).removeAttr('disabled').animate({
                                    opacity:1.0
                                });
                            }

                        }
                        return false;
                        } else {
                        if (A_.devel) console.log('Bir hata ile karşılaşıldı.');
                        if (loader) {
                            $(loader).hide();
                        }
                        if (disable) {
                            $(disable).removeAttr('disabled').animate({
                                opacity:1.0
                            });
                        }
                        jAlert(data.msg,data.title);
                        return false;
                    }
                }
            });
        },
        reset:function () {
            if (A_.devel) console.log('Formlar sıfırlanacak.');
            var formcount = $("form").size();
            for (var i = 0; i < formcount; i++) {
                var elements = $("form").get(i).elements;
                //console.dir(elements);
                for (var s = 0; s < elements.length; s++) {
                    var fieldtype = elements[s].type;
                    switch (fieldtype) {
                        case "text":
                        case "password":
                        case "textarea":
                        case "email":
                        case "number":
                        //case "hidden":
                        case "color":
                        case "date":
                        case "datetime":
                        case "datetime-local":
                        case "range":
                        case "tel":
                        case "time":
                        case "url":
                        case "week":
                            elements[s].value = "";
                            break;
                        case "radio":
                        case "checkbox":
                            if (elements[s].checked) {
                                elements[s].checked = false;
                            }
                            break;
                        case "select":
                            elements[s].selectedIndex = 0;
                            break;
                        default:
                            if (fieldtype != 'submit') {
                                elements[s].value = "";
                            }
                            break;
                    }
                }
                if (A_.devel) console.log(i + ' numaralı form sıfırlandı.')
                if (A_.devel) console.log('jqTransform sıfırlanacak.');
                var f = $("form").get(i);
                var sel;
                $('.jqTransformSelectWrapper select', f).each(function () {
                    sel = (this.selectedIndex < 0) ? 0 : this.selectedIndex;
                    $('ul', $(this).parent()).each(function () {
                        $('a:eq(' + sel + ')', this).click();
                    });
                });
                $('a.jqTransformCheckbox, a.jqTransformRadio', f).removeClass('jqTransformChecked');
                $('input:checkbox, input:radio', f).each(function () {
                    if (this.checked) {
                        $('a', $(this).parent()).addClass('jqTransformChecked');
                    }
                });

            }
        },
        resetDoms:function () {
            for (i = 0; i < aA_uments.length; i++) {
                var el = $("#" + aA_uments[i]);
                el.html('');
                el.attr('value', '');
                if (A_.devel) {
                    console.log(i + ' numaralı eleman sıfırlandı. IDsi: ' + el.attr('id'));
                }
            }
        },
        /**
         *
         * @param form
         */
        disableForm:function(form){
            form.find('input').each(
                function(){
                    $(this).attr('disabled','disabled');
                    //güvenlik için
                    $(this).removeAttr('name');
                    if(A_.devel===true){
                        console.log('Tüm inputlar kilitlendi')
                    }
                }
            )
            form.find('select').each(function(){
                $(this).attr('disabled','disabled');
                $(this).removeAttr('name');
                if(A_.devel===true){
                    console.log('Tüm selectler kilitlendi')
                }
            })
            form.find('textarea').each(function(){
                $(this).attr('disabled','disabled');
                $(this).removeAttr('name');
                if(this.devel===true){
                    console.log('Tüm textarealar kilitlendi')
                }
            })
            form.find('.addRelation').remove();
            form.find('.reset_autocomplete').remove();
            form.find("input[type='submit']").remove();
        }

    },
    html:{
        /**
         * Geri sayım aparatı.
         * @param integer t
         * @param string el
         */
        remaining:function (t, el) {
            var s, x, d, date, hour, minute, seconds, days;
            days = parseInt(t / 86400);
            if (t > 86400) {
                s = t % 86400;
                x = t - s;
                d = parseInt(x / 86400);
            } else {
                s = t;
                d = 0;
            }
            date = new Date(s * 1000); // GMT +2'ye eşitlemek için yapıyoruz.
            hour = date.getHours() + d;
            minute = A_.html.strPad(date.getMinutes());
            seconds = A_.html.strPad(date.getSeconds());
            $("#" + el).html(days + " Gün " + hour + ":" + minute + ":" + seconds);
            setTimeout("A_.html.remaining(" + ( t - 1 ) + ",'" + el + "');", 1000);
        },
        /**
         *
         * @param val
         * @return {String}
         */
        strPad:function strpad(val) {
            return (!isNaN(val) && val.toString().length === 1) ? "0" + val : val;
        },
        /**
         * Input'a en fazla girilebilecek karakter sayısı
         * @param integer len En fazla karakter sayısı
         * @param string el HTML Dom
         * @param event
         * @return {Boolean}
         */
        maxLength:function (len, el, event) {
            if (($(el).val()).length > 70 && event.keyCode != 8) {
                return false;
            }
        },
        /**
         * Anchor için ajax üzerinden veri gönderimi.
         * @param HTMLDom el
         * @return {Boolean}
         * @author Emre YILMAZ <aybarscengaver@gmail.com>
         */
        ajaxQuery:function (el) {
            el.on('click',function(){
                //Ajax url
                var url = $(this).attr('href');
                //İşlem bitiminde çalıştırılacak fonksiyon
                var _function = $(this).attr('function');
                //İşlem bitiminde phpden dönen uyarı mesajı | true,false

                var _forward = $(this).attr('data-forward');
                var alert = $(this).attr('alert');
                url = '/ajax.php?' + url;
                $.ajax({
                    url:url,
                    type:'POST',
                    error:function () {
                        jAlert('Beklenmedik hata!');
                    },
                    success:function (data) {
                        try{
                            data = JSON.parse(data);
                        }catch(e){
                            data = {
                                type:'fatal',
                                msg:'Sonuç formatı hatalı!'
                            }
                        }

                        if (data.type === 'success') {
                            if (alert !== 'false') {
                                jAlert(data.msg, data.title, function () {
                                    if (_function) {
                                        eval(_function);//eval is evil :)
                                    } else if (data.extra.redirect) {
                                        window.location = data.extra.redirect;
                                    }
                                    return false;
                                })
                            } else {
                                if (_function) {
                                    eval(_function);//eval is evil :)
                                }
                                if (data.extra.redirect) {
                                    window.location = data.extra.redirect;
                                    return false;
                                }
                                if (A_.devel) {
                                    console.log('.');
                                }
                            }
                            if(_forward){
                                window.location = _forward;
                                return false;
                            }

                        } else {
                            jAlert(data.msg, data.title, function () {
                                return false;
                            });
                        }

                    }
                });
                return false;
            })
        },
        /**
         */
        loader:function (selector) {
            $("body").bind("DOMSubtreeModified", function (e) {
                $("body").find(selector).each(function () {
                    //Tuğrul'a special thanx :)
                    if($(this).attr('data-is-live') == 1)
                        return;
                    var event = $(this).attr('data-event-type');
                    $(this).on(event, function () {
                        var request_url = $(this).attr('data-url');
                        var insert_type = $(this).attr('data-insert-type');
                        var value = $(this).val();
                        var container_div = $(this).attr('data-container-id');
                        $.ajax({
                            type:'POST',
                            url:'/ajax.php?' + request_url,
                            data:'data=' + value,
                            success:function (result) {
                                if (result.type === 'success') {
                                    if (insert_type === "append") {
                                        $("#" + container_div).append(result.msg);
                                    } else {
                                        $("#" + container_div).html(result.msg);
                                    }
                                } else {
                                    jAlert(result.msg, "Hata");
                                    return false;
                                }
                            },
                            dataType:'JSON'
                        });
                    }).attr('data-is-live','1');
                });
            });
        },
        /**
         *
         * @param checkGroup
         * @param elem
         */
        checkAll:function (checkGroup,elem) {
            $("input." + checkGroup).each(
                function () {
                    $(this).attr('checked', 'checked')
                    $(this).parent().children('a').addClass('jqTransformChecked');
                }
            );
            elem.attr('type', 'uncheckall');
        },
        /**
         *
         * @param checkGroup
         * @param elem
         */
        unchechAll:function (checkGroup,elem) {
            $("input."+checkGroup).each(
                function () {
                    $(this).removeAttr('checked');
                    $(this).parent().children('a').removeClass('jqTransformChecked');
                }
            );
            elem.attr('type', 'checkall');

        },
        pager: function (container, totalCount, limit, target, after_url, current_page) {
            var pageCount = Math.floor(totalCount / limit);
            if(pageCount<=1){

            }else{
                for (i = 0; i <= pageCount; i++) {
                    if (pageCount > 15) {
                        var first = parseInt(current_page) - 7;

                        var last = parseInt(current_page) + 7;
                        if (7 > (pageCount - current_page)) {
                            last = pageCount;
                        }
                    }
                    if (( i >= first && i <= last) || (i == 0 || i == pageCount)) {
                        var link = document.createElement('a');
                        link.setAttribute('class', 'uibutton confirm');
                        if (current_page == i) {
                            link.setAttribute('class', 'uibutton special');
                        }

                        link.setAttribute('href', target + i + (after_url ? after_url : ''));
                        var x = i + 1;
                        if (i == 0) {
                            link.textContent = "Ilk Sayfa";
                        } else if (i == pageCount) {
                            link.textContent = "Son Sayfa";
                        } else {
                            link.textContent = x;
                        }
                        container.append(link);
                    }
                }
            }

        }
    },
    share:{
        /**
         * Facebook Paylaşım betiği
         * @param el
         */
        facebook:function (el) {
            $(el).on('click', function () {
                var uri = $(this).attr('uri');
                if (uri == '' || uri == undefined) {
                    var uri = encodeURIComponent(location.href);
                }
                var title = $(this).attr('title');
                if (title = '' || title == undefined) {
                    var title = encodeURIComponent(document.title);
                }
                var d = document;
                f = 'http://www.facebook.com/share';
                l = d.location;
                e = encodeURIComponent;
                p = '.php?src=bm&u=' + uri + '&t=' + title;
                try {
                    if (!/^(.*\.)?facebook\.[^.]*$/.test(l.host))
                        throw(0);
                    share_internal_bookmarklet(p)
                } catch (z) {
                    a = function () {
                        if (!window.open(f + 'r' + p, 'sharer', 'toolbar=0,status=0,resizable=1,width=626,height=436'))
                            l.href = f + p
                    };
                    if (/Firefox/.test(navigator.userAgent))
                        setTimeout(a, 0);
                    else {
                        a()
                    }
                }
                void(0)
            });
        },
        /**
         * Twitter Paylaşım Betiği
         * @param el
         */
        twitter:function (el) {
            $(el).on('click', function () {
                var uri = $(this).attr('uri');
                if (uri == '' || uri == undefined) {
                    var uri = encodeURIComponent(location.href);
                }
                var title = $(this).attr('title');
                if (title = '' || title == undefined) {
                    var title = encodeURIComponent(document.title);
                }
                window.open('http://twitter.com/share?url=' + uri, 'twiter', 'toolbar=no,width=550,height=550');
            });
        },
        /**
         * Friend Feed Paylaşım Betiği
         * @param el
         */
        friendfeed:function (el) {
            $(el).on('click', function () {
                var uri = $(this).attr('uri');
                var e = document.createElement('script');
                e.setAttribute('type', 'text/javascript');
                e.setAttribute('src', 'http://friendfeed.com/share/bookmarklet/javascript');
                document.body.appendChild(e)
            });
        },
        /**
         * Delicious Paylaşım Betiği
         * @param el
         */
        delicious:function (el) {
            $(el).on('click', function () {
                var uri = $(this).attr('uri');
                if (uri == '' || uri == undefined) {
                    var uri = encodeURIComponent(location.href);
                }
                var title = $(this).attr('title');
                if (title = '' || title == undefined) {
                    var title = encodeURIComponent(document.title);
                }
                window.open('http://www.delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url=' + uri + '&amp;title=' + title, 'delicious', 'toolbar=no,width=550,height=550');
                return false;
            });
        }
    }
};
