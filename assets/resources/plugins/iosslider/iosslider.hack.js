function slideChange(e)
{
    jQuery(".selectors .item").removeClass("selected");
    jQuery(".selectors .item:eq("+(e.currentSlideNumber-1)+")").addClass("selected")
}
function slideComplete(e)
{
    if(!e.slideChanged)return false;
    captionEffects(e)
}
function captionEffects(e)
{
    var t=jQuery(e.sliderObject).find(".caption"),n=jQuery(e.currentSlideObject).find(".caption"),r,i;
    t.find(".title_big, .title_small, .more").attr("style","");
    t.find(".main_title").css(
        {
            "margin-left":"",opacity:0
        }
    );
    if(n.length>0)
    {
        if(n.attr("class").indexOf("fromright")<=0)
        {
            r=
            {
                left:0,opacity:1
            };
            i=
            {
                opacity:1,"margin-left":0
            }
        }
        else
        {
            r=
            {
                right:0,opacity:1
            };
            i=
            {
                opacity:1,"margin-right":0
            }
        }
    }
    n.find(".more").animate(r,300,"easeOutQuint");
    n.find(".title_big").delay(100).animate(r,400,"easeOutQuint");
    n.find(".title_small").delay(200).animate(r,400,"easeOutQuint");
    n.find(".main_title").delay(300).animate(i,400,"easeOutQuint")
}
function sliderLoaded(e,t)
{
    var n=e.sliderContainerObject;
    if(t.hideControls)n.addClass("hideControls");
    if(t.hideCaptions)n.addClass("hideCaptions");
    setTimeout(function()
        {
            n.css("background-image","none")
        }
        ,1e3);
    captionEffects(e);
    slideChange(e)
}
(function(e)
{
    e(window).load(function(t)
        {
            function h(e,t)
            {
                function n(e)
                {
                    return("0"+parseInt(e).toString(16)).slice(-2)
                }
                e=e.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
                return(t?"":"#")+n(e[1])+n(e[2])+n(e[3])
            }
            function p(e)
            {
                if(e.substr(0,1)==="#")
                {
                    return e
                }
                var t=/(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(e);
                var n=parseInt(t[2]);
                var r=parseInt(t[3]);
                var i=parseInt(t[4]);
                var s=i|r<<8|n<<16;
                return t[1]+"#"+s.toString(16)
            }
            var n=500,r="easeInExpo",i=e(".selectorsBlock.thumbs"),s=i.find("img"),o=i.find(".thumbTrayButton"),u=5,a=s.length>=u?u:s.length,f=s.width()*a+(a-1)*15,l=o.find("span"),c=function()
            {
                i.stop().animate(
                    {
                        bottom:"-100px"
                    }
                    ,n,r);
                l.removeClass("icon-minus").addClass("icon-plus")
            };
            i.css(
                {
                    width:f,"margin-left":-(Math.floor(f/2)+15)
                }
            ).animate(
                {
                    opacity:1
                }
                ,300,function()
                {
                    setTimeout(function()
                        {
                            c()
                        }
                        ,1500)
                }
            );
            o.click(function(t)
                {
                    t.preventDefault();
                    e(this).toggleClass("opened");
                    if(o.hasClass("opened"))
                    {
                        i.stop().animate(
                            {
                                bottom:"-5px"
                            }
                            ,n,r);
                        l.removeClass("icon-plus").addClass("icon-minus")
                    }
                    else
                    {
                        c()
                    }
                }
            );
            i.mouseleave(function(e)
                {
                    c();
                    o.toggleClass("opened")
                }
            );
            var d=e(".iosSlider.faded"),v=e(".iosSlider.faded .slider .item > img"),m=Math.floor(e(".iosSlider.faded .slider img").height()/2.5),g=e("body").css("background-color"),y=d.height()-m;
            var b=e('<style type="text/css" />').appendTo("head");
            v.each(function(t,n)
                {
                    var r=e(this),
                        i='<div class="fadeMask" style="height: '+m+"px; top:"+y+'px; "></div>';
                    e(i).insertAfter(r).animate(
                        {
                            opacity:1
                        }
                        ,800,"easeInExpo")
                }
            )
        }
    )
}
    )(jQuery)