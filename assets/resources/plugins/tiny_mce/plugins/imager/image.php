<?php
	require_once    $_SERVER['DOCUMENT_ROOT']."/config/config.php";
use Core\Config; 
    $config_ = config::init(); 
    $config['uploadDir']=$config_->folders['document_root'] . 'uploads/';
    $config['url'] = $config_->folders['upload_dir']['frontend'];
    $config['fileTypes']=array('jpg','png','gif');
    $z="";

    if(isset($_GET['folder'])){
        echo $_GET['folder'];
        $config['uploadDir'] = $config_->folders['document_root'] . 'uploads/'.$_GET['folder'];
        $z = $_GET['folder'].'/';
        $config['url'] = $config_->folders['upload_dir']['frontend'] .'/'.$_GET['folder'];
    }
?> 
<html>
    <head>
        <title>{#imager_dlg.title}</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
        <script src="../../tiny_mce_popup.js" type="text/javascript"></script>
        <script src="js/imager.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <meta http-equiv="Content-type" content="text/html;charset=utf-8"/>
    </head>
</html>
<h1>{#imager_dlg.title}</h1>
<fieldset>
    <legend>{#imager_dlg.header}</legend>
    <?php
        if($_POST){
            $fileName = $_FILES['image']['name'];
            $ext = explode('.',$fileName);
            $ext = $ext[count($ext)-1];
            if(in_array($ext,$config['fileTypes'])){
                $newName = uniqid().'.'.$ext;
                $upload = move_uploaded_file($_FILES['image']['tmp_name'],$config['uploadDir'].'/'.$newName);
                if(!$upload){
                    echo '<i>{#imager_dlg.errora}!</i>';
                }else{
                    echo '<b>{#imager_dlg.completed}, {#imager_dlg.filename} : <a href="'.$config['url'].'/'.$newName.'" class="selective" alt="'.$config['url'].$newName.'">'.$newName.'</a></b>';
                }
            }else{
                echo '<i>{#imager_dlg.error}.{#imager_dlg.msg1}</i>';
            }
        }
    ?>
    <form action="<?=$_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
        <input type="file" name="image"/><br/>
        <input type="submit" name="submit" value="{#imager_dlg.submit}"/>
    </form>
</fieldset>
<fieldset>
    <legend>{#imager_dlg.header2}</legend>
    <?php
        $dirs = scandir($config['uploadDir']);
    ?>
    <div class="files">
    <?php
    echo "<h1>Dosyalar</h1>";
    foreach($dirs as $dir){
        if($dir!=='.' && $dir!=='..'){
//                echo $config['uploadDir'].'/'.$dir."<br/>";
            if(is_file($config['uploadDir'].'/'.$dir)){
//                    echo 'file';
                $ext = explode('.',$dir);
                $ext = $ext[count($ext)-1];
                if(in_array($ext, $config['fileTypes'])){
                    echo '<a href="'.$config['url'].'/'.$dir.'" width="50" height="50" class="selective" alt="'.$config['url'].'/'.$dir.'"><img src="'.$config['url'].'/'.$dir.'" width="50px"/></a>';
                }
            }
        }
    }
    ?>
    </div>
    <div class="folders_">
    <?php
    echo "<h1>Klasörler</h1><div class='folders'>";
    foreach($dirs as $dir){
        if($dir!=='.' && $dir!=='..'){
//                echo $config['uploadDir'].'/'.$dir."<br/>";
            if(is_dir($config['uploadDir'].'/'.$dir)){
                echo "<a href='?folder={$z}{$dir}'>{$dir}</a><br/>";
            }
        }
    }
    echo '</div>';

    ?>
    </div>
    <img class="image" width="150"/>
    
</fieldset>
<br/>

<style type="text/css">
    .folders{
        margin:5px;
        line-height: 15px;
    }
    .folders a{
        text-decoration: none;
        background-image: url(images/folder.png);
        padding-left: 20px;
        background-repeat: no-repeat;
    }

    .folders_{
        margin:5px;
        border: 1px solid #0480be;
        padding:3px;
    }
    .files{
        margin:5px;
        border: 1px solid #0480be;
        padding:3px;
    }
</style>