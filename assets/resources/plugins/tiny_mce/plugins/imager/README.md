#TinyMCE Imager#

* Install     
* Setup     

##Install##
Checkout the library from git@github.com/theylmz:Imager.git and copy Imager dir to the tiny_mce/plugins directory.

##Setup##

***Open image.php in Imager directory. Set that values:***
- $config['upload_dir'] = full path of your upload directory
- $config['url'] = http file path in your upload directory.
- $config['fileTypes'] = Allowed file extensions.

***In your javascript file:***
* Tinymce setting

        tinyMCE.init({
            mode:'specific_textareas',
            editor_selector:'text_editor',
            plugins:'imager',
            theme_advanced_buttons3 : "imager,hr,removeformat,visualaid,separator,sub,sup,separator,charmap",
            language:'en',
        });
* Must be add language parameter.
* Must be add imager button.

note: upload dir must be have writing permission
