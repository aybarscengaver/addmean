var ImagerDialog ={
    init:function(){
        $(".selective").live("click",function(){
            var file = $(this).attr('alt');
            ImagerDialog.insert(file);
        })
        $(".selective").live("mouseenter",function(){
            var file = $(this).attr('href');
            $(".image").attr('src',file);
            $(".image").show();
            $(document).mousemove(function(e){
                $(".image").css({left:e.pageX+5});
                $(".image").css({top:e.pageY+5});
            })
        })
        $(".selective").live("mouseleave",function(){
            $(".image").hide();
        })
    },
    insert:function(a){
        var data = '<img src="'+a+'">';
        var ed = tinyMCEPopup.editor;
        ed.execCommand('mceInsertContent',false,(data),{skip_undo : 1})
        tinyMCEPopup.close();
    }
}
tinyMCEPopup.onInit.add(ImagerDialog.init,ImagerDialog);