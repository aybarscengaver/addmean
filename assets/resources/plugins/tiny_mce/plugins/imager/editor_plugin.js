(function(){
    tinymce.PluginManager.requireLangPack('imager');
    tinymce.create("tinymce.plugins.Imager", {
        init: function(a,b){
            a.addCommand("wyrImager",function(){
                a.windowManager.open({
                    file:b + "/image.php",
                    width: 480 ,
                    height:385,
                    inline:1
                },
                {
                    plugin_url:b
                })
            });
            //TODO language pack
            a.addButton("imager",{
                    title:"Imager",
                    cmd:"wyrImager",
                    image:b + '/images/upload.png'
            });
        },
        getInfo: function(){
            return {
                longname: "Image Upload Plugin",
                author: "Emre YILMAZ",
                authorurl: "http://blog.theylmz.com",
                infourl: "http://blog.theylmz.com",
                version: 1
                }
        }
    });
    tinymce.PluginManager.add("imager",tinymce.plugins.Imager);
})();