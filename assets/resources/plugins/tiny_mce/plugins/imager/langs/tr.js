tinyMCE.addI18n('tr.imager_dlg',{
    title:'Resim Yükleyici v0.2',
    header : 'Yükleyici',
    error : 'Hata',
    completed : 'Tamamlandı',
    filename : 'Dosya Adı',
    msg1 : 'Dosya tipi desteklenmiyor.',
    submit : 'Gönder',
    header2 : 'Dosya Listesi'  ,
    folders:'Klasörler',
    files:'Dosyalar'
});