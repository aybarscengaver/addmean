tinyMCE.addI18n('en.imager_dlg',{
    title:'Image Uploader v0.2',
    header : 'Uploader',
    error : 'Error',
    completed : 'Completed',
    filename : 'File name',
    msg1 : 'File type is unsupported.',
    submit : 'Submit',
    header2 : 'File List',
    folders:'Folders',
    files:'Files'
});