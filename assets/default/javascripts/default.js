head.ready(function () {
    $('form[target!="^_normal"]').validationEngine({
        onValidationComplete: function (form, status) {
            if (status) {
                return A_.forms.send(form);
            }
        }
    });

    $("#language").on('change',function() {
        var val = $(this).val();
        document.location.href="/language/change/"+val;

    });
});