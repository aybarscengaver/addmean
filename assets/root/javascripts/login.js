function _login() {
    var data = $("#formLogin").serialize();
    return $.ajax({
        type:'POST',
        data:data,
        url:'/ajax.php?root/user/login',
        async:false,
        success:function (data) {
            data = jQuery.parseJSON(data);
            return data;
        }
    });
}
$(document).ready(function () {
    onfocus();
    $(".on_off_checkbox").iphoneStyle();
    $('.tip a ').tipsy({gravity:'sw'});
    $('#login').show().animate({   opacity:1 }, 2000);
    $('.logo').show().animate({   opacity:1, top:'32%'}, 800, function () {
        $('.logo').show().delay(1200).animate({   opacity:1, top:'1%' }, 300, function () {
            $('.formLogin').animate({   opacity:1, left:'0' }, 300);
            $('.userbox').animate({ opacity:0 }, 200).hide();
        });
    })
});

$('.userload').on('click', function (e) {
    $('.formLogin').animate({   opacity:1, left:'0' }, 300);
    $('.userbox').animate({ opacity:0 }, 200, function () {
        $('.userbox').hide();
    });
});

$("#forgetpass").on('click',function(){
    showError("Coming soon..",300)
});

$('#but_login').on('click', function (e) {
    if (document.formLogin.username.value == "" || document.formLogin.password.value == "") {
        showError("Lütfen kullanıcı adı ve şifrenizi giriniz.", 500);
        $('.inner').jrumble({ x:4, y:0, rotation:0 });
        $('.inner').trigger('startRumble');
        setTimeout('$(".inner").trigger("stopRumble")', 500);
        setTimeout('hideTop()', 5000);
        return false;
    }

    var send = _login();
    var data = jQuery.parseJSON(send.responseText);

    if (data.type == 'success') {
        hideTop();
        loading('Bekleyiniz...', 1);
        setTimeout("unloading()", 2000);
        setTimeout("Login()", 2500);
    } else {
        showError(data.msg + " ", 500);
        $('.inner').jrumble({ x:4, y:0, rotation:0 });
        $('.inner').trigger('startRumble');
        setTimeout('$(".inner").trigger("stopRumble")', 500);
        setTimeout('hideTop()', 5000);
        return false;
    }


});


function Login() {
    $("#login").animate({   opacity:1, top:'49%' }, 200, function () {
        $('.userbox').show().animate({ opacity:1 }, 500);
        $("#login").animate({   opacity:0, top:'60%' }, 500, function () {
            $(this).fadeOut(200, function () {
                $(".text_success").slideDown();
                $("#successLogin").animate({opacity:1, height:"200px"}, 500);
            });
        })
    })
    ;
    setTimeout("window.location.href='"+$("#formLogin").attr('forward')+"'", 3000);
}

$('#alertMessage').on('click', function () {
    hideTop();
});

function showError(str) {
    $('#alertMessage').addClass('error').html(str).stop(true, true).show().animate({ opacity:1, right:'0'}, 500);

}

function showSuccess(str) {
    $('#alertMessage').removeClass('error').html(str).stop(true, true).show().animate({ opacity:1, right:'0'}, 500);
}


function onfocus() {
    if ($(window).width() > 480) {
        $('.tip input').tipsy({ trigger:'focus', gravity:'w', live:true});
    } else {
        $('.tip input').tipsy("hide");
    }
}

function hideTop() {
    $('#alertMessage').animate({ opacity:0, right:'-20'}, 500, function () {
        $(this).hide();
    });
}

function loading(name, overlay) {
    $('body').append('<div id="overlay"></div><div id="preloader">' + name + '..</div>');
    if (overlay == 1) {
        $('#overlay').css('opacity', 0.1).fadeIn(function () {
            $('#preloader').fadeIn();
        });
        return  false;
    }
    $('#preloader').fadeIn();
}

function unloading() {
    $('#preloader').fadeOut('fast', function () {
        $('#overlay').fadeOut();
    });
}