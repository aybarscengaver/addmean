<?php
/**
 * User: aybarscengaver
 * Date: 11.05.2013
 * Time: 04:28
 * File: addmean.autoload.php
 */
class laravel{
    public static function loader(){
       include_once __DIR__.'/../libs/Laravel/vendor/autoload.php';

        $app = new \Illuminate\Foundation\Application();
        $app->redirectIfTrailingSlash();
        $env = $app->detectEnvironment(array(
            'local'=>'your-machine-name'
        ));
        $app->bindInstallPaths(array(
            'app'=>__DIR__.'/../libs/Laravel/app',
            'public'=>__DIR__.'/../../public/',
            'base'=>__DIR__.'/../../libs/Laravel',
            'storage'=>__DIR__.'/../libs/Laravel/app/storage'
        ));
        $framework = __DIR__.'/../libs/Laravel/vendor/laravel/framework/src/';
        require $framework.'/Illuminate/Foundation/start.php';
        return $app;
    }
}