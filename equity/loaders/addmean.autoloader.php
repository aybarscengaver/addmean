<?php
/**
 * User: aybarscengaver
 * Date: 11.05.2013
 * Time: 04:28
 * File: addmean.autoload.php
 * Package: didimtaxis
 */

class addmean{
    public static function autoload($name){
        $filename = __DIR__.'/../libs/'.str_replace('\\','/',$name).'.php';
        if(is_file($filename)){
            require $filename;
        }
    }
}