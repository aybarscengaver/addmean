<?php
/**
 * User: aybarscengaver
 * Date: 02.04.2013
 * Time: 19:37
 * File: autoloader.php
 * Package: AddMean
 */

namespace Core;
class Autoloader {
    private $framework,$cms,$autoloaders;
    public function __construct($cms,$framework=null){
        $this->cms=$cms;
        $this->framework=$framework;
        $this->autoloaders = $this->check_dir();
        
    }
    public function load(){
        if(array_key_exists($this->cms,$this->autoloaders)){
            //Cms autoloader'ı yükle
            require_once( $this->autoloaders[$this->cms] );
            $cms = $this->cms;
            spl_autoload_register("$cms::autoload",false);
        }
        if(array_key_exists($this->framework,$this->autoloaders)){
            require_once ( $this->autoloaders[$this->framework]);
            $fw = $this->framework;
            $fw::loader();
        }
    }


    public function check_dir(){
        $dir_handler = opendir(__DIR__);
        $autoloaders = array();
        while(false!==($file=readdir($dir_handler))){
            $x = explode('.',$file);
            if(count($x)>2){
                if($x[1]=="autoloader"){
                    $autoloaders[$x[0]]=__DIR__.'/'.$file;
                }
            }
        }
        return $autoloaders;
    }

}