<?php
/**
 * User: wyrus
 * Date: 8/6/12
 * Time: 2:58 PM
 * Company: Deli
 */
class m_statics {

    public static function status($v = null) {
        $data = array();
        $data[1] = "Onay Bekliyor";
        $data[2] = "Aktif";
        $data[3] = "Pasif";
        return self::response($data, $v);
    }

    public static function module_types($value = null) {
        $types = array();
        $types[1] = "Tam Modül";
        $types[2] = "Sadece Veritabanı";
        return self::response($types, $value);

    }

    public static function media_types($value = null) {
        $types = array();
        $types[1] = "Resim";
        $types[2] = "Diğer Dosyalar";
        $types[3] = "Ses Dosyası";
        $types[4] = "Sıkıştırılmış Dosya";
        return self::response($types, $value);

    }

    public static function image_modification_types($value = null) {
        $types = array();
        $types[0] = "İşlem Yapma";
        $types[1] = "Yeniden Boyutlandır";
        $types[2] = "Kırp";
        return self::response($types, $value);
    }

    public static function field_types($value = null) {
        $types = array();
        $types[1] = "text";
        $types[2] = "select";
        $types[3] = "textarea";
        $types[4] = "hierarchy";
        $types[5] = "checkbox";
        $types[6] = "editor";
        $types[7] = "image_uploader";
        $types[8] = "file_uploader";
        $types[9] = "multi_select";
//        $types[10]= "checkbox_group";
//        $types[11]= "radio_group";
        $types[12] = "multi_uploader";
//        $types[13]= "captcha";
//        $types[14]= "color_picker";
        $types[15] = "autocomplete";
        $types[16] = "number";
        $types[17] = "money";
//        $types[18]= "data_list";
        $types[19] = "date_time_picker";
        $types[20] = "date_picker";
        $types[21] = "phone";
        $types[22] = "password";
        $types[23] = "select_search";
        $types[24] = "captcha";
        $types[25] = "email";

        return self::response($types, $value);
    }

    public static function column_types_sql($value = null) {
        $types = array();
        $types[1] = "VARCHAR(255)";
        $types[2] = "INT(10)";
        $types[3] = "TEXT";
        $types[4] = "INT(10)";
        $types[5] = "ENUM('0','1')";
        $types[6] = "TEXT";
        $types[7] = "VARCHAR(255)";
        $types[8] = "VARCHAR(255)";
        $types[9] = "VARCHAR(255)";
        $types[10] = "VARCHAR(255)";
        $types[11] = "VARCHAR(255)";
        $types[12] = "TEXT";
        $types[13] = "VARCHAR(255)";
        $types[14] = "VARCHAR(6)"; //#123123;
        $types[15] = "INT(10)";
        $types[16] = "INT(10)";
        $types[17] = "FLOAT(10)";
        $types[18] = "INT(10)";
        $types[19] = "TIMESTAMP"; //TimeStamp olacak
        $types[20] = "VARCHAR(20)"; //Strıng olacak
        $types[21] = "VARCHAR(20)";
        $types[22] = "VARCHAR(32)";
        $types[23] = "INT(10)";
        $types[24] = "VARCHAR(32)";
        $types[25] = "VARCHAR(255)";
        return self::response($types, $value);
    }

    public static function modifiers($value = null) {
        $modifiers = get_class_methods('modifiers');
        return self::response($modifiers, $value);
    }

    public static function setting_groups($value = null) {
        $group = array();
        $group[1] = "Eposta Ayarları";
        $group[2] = "Dosya Sunucusu Ayarları";
        $group[3] = "SSH Ayarları";
        $group[4] = "Genel Ayarlar";
        return self::response($group, $value);
    }

    public static function relation_types($value = null) {
        $type = array();
        $type[1] = "Karmaşık İlişki"; //many to many
        $type[2] = "Doğrusal İlişki"; //one to many

        return self::response($type, $value);
    }

    public static function user_levels($value = null) {
        $levels = array();
        $levels[1] = "Standart Kullanıcı";
        $levels[2] = "Yönetici";
        $levels[3] = "Geliştirici";
        return self::response($levels, $value);
    }

    public static function language_database_collations($value = null) {
        $lang = array();
        $lang[] = "utf8_general_ci";
        $lang['icelandic'] = "utf8_icelandic_ci";
        $lang['latvian'] = "utf8_latvian_ci";
        $lang['romanian'] = "utf8_romanian_ci";
        $lang['slovenian'] = "utf8_slovenian_ci";
        $lang['polish'] = "utf8_polish_ci";
        $lang['estonian'] = "utf8_estonian_ci";
        $lang['spanish'] = "utf8_spanish_ci";
        $lang['swedish'] = "utf8_swedish_ci";
        $lang['turkish'] = "utf8_turkish_ci";
        $lang['czech'] = "utf8_czech_ci";
        $lang['danish'] = "utf8_danish_ci";
        $lang['lithuanian'] = "utf8_lithuanian_ci";
        $lang['slovak'] = "utf8_slovak_ci";
        $lang['spanish2'] = "utf8_spanish2_ci";
        $lang['roman'] = "utf8_roman_ci";
        $lang['persian'] = "utf8_persian_ci";
        $lang['esperanto'] = "utf8_esperanto_ci";
        $lang['hungarian'] = "utf8_hungarian_ci";
        $lang['sinhala'] = "utf8_sinhala_ci";
        return self::response($lang, $value);
    }

    public static function applications($value = null) {
        $apps = array();
        $apps[1] = "Yönetim Paneli";
        $apps[2] = "Ön Yüz";
        $apps[3] = "Mobil";
        $apps[4] = "Facebook App";
        $apps[5] = "E-Ticaret";
        return self::response($apps, $value);
    }

    public static function file_extensions($value) {
        $types = array(1 => 'gif', 2 => 'jpeg', 3 => 'png', 4 => 'swf', 5 => 'psd', 6 => 'bmp', 7 => 'tiff_ii', 8 => 'tiff_mm', 9 => 'jpc', 10 => 'jp2', 11 => 'jpx', 12 => 'jb2', 13 => 'swc', 14 => 'iff', 15 => 'wbmp', 16 => 'xmb', 17 => 'ico');
        return self::response($types, $value);
    }

    public static function mimetypes_extensions($value) {
        $mime_types = array('a' => 'application/octet-stream', 'acgi' => 'text/html', 'aif' => 'audio/aiff', 'asp' => 'text/asp', 'avi' => 'video/avi', 'avs' => 'video/avs-video', 'bcpio' => 'application/x-bcpio', 'bmp' => 'image/bmp', 'book' => 'application/book', 'bz' => 'application/x-bzip', 'bz2' => 'application/x-bzip2', 'css' => 'text/css', 'doc' => 'application/msword', 'dwg' => 'image/vnd.dwg', 'gif' => 'image/gif', 'gz' => 'application/x-gzip', 'gzip' => 'application/x-gzip', 'help' => 'application/x-helpfile', 'hta' => 'application/hta', 'htc' => 'text/x-component', 'html' => 'text/html', 'htt' => 'text/webviewhtml', 'ico' => 'image/x-icon', 'ief' => 'image/ief', 'jpg' => 'image/jpeg', 'jps' => 'image/x-jps', 'mime' => 'www/mime', 'mov' => 'video/quicktime', 'movie' => 'video/x-sgi-movie', 'mp3' => 'audio/mpeg3', 'png' => 'image/png', 'pps' => 'application/vnd.ms-powerpoint', 'ppt' => 'application/mspowerpoint', 'py' => 'text/x-script.phyton', 'pyc' => 'applicaiton/x-bytecode.python', 'rtf' => 'text/richtext', 'sh' => 'application/x-bsh', 'swf' => 'application/x-shockwave-flash', 'tar' => 'application/x-tar', 'tgz' => 'application/gnutar', 'tif' => 'image/tiff', 'tiff' => 'image/tiff', 'txt' => 'text/plain', 'vcs' => 'text/x-vcalendar', 'wav' => 'audio/wav', 'word' => 'application/msword', 'xls' => 'application/excel', 'xml' => 'text/xml', 'zip' => 'application/zip',);
        return self::response($mime_types, $value);
    }

    public static function languages($value = null) {
        $l = array('tr' => 'tr_TR', 'en' => 'en_US','ru'=>'ru_RU');
        return self::response($l, $value);
    }

    /**
     * Genel bi döndürgeç.
     * @static
     *
     * @param      $data
     * @param null $value
     *
     * @return int|string
     */
    private static function response($data, $value = null) {
        if ($value) {
            foreach ($data as $k => $v) {
                if ($v == $value) {
                    return $k;
                } elseif ($k == $value) {
                    return $v;
                }
            }
        }
        return $data;
    }


    /**
     * Projeye özel sabitler
     */


    public function message_status($value = null) {
        $status = array();
        $status[1] = "Okunmadı";
        $status[2] = "Okundu";
        $status[3] = "Gereksiz";
        return self::response($status,$value);
    }

    /**
     *
     */

}
