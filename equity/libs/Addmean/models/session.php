<?php
/**
 * User: aybarscengaver
 * Date: 24.04.2013
 * Time: 00:22
 * File: session.php
 * Package: AddMean
 */
class m_session{
    private static $singleton;
    private $session;

    public function __construct(){
        $this->session=&$_SESSION;
    }

    public function __get($name){
        if(isset($this->session[$name])){
            return $this->session[$name];
        }
    }

    public static function singleton(){
        if(!isset(self::$singleton)){
            self::$singleton=new m_session();
        }
        return self::$singleton;
    }

    public function __set($name,$value){
        $this->session[$name] = $value;
    }

    public function __unset($name){
        if(isset($this->session[$name])){
            unset($this->session[$name]);
        }else{
            throw new Exception('Geçersiz oturum');
        }
    }
}