<?php
/**
 * User: delirehberi
 * Date: 17.01.2013
 * Time: 00:23
 * Filename: path.php
 * Description:
 * Usage:
 */
class m_path extends model{
    protected $_table_name='file_settings';
    private static $keys =array();
    public static function get_path($key,$original=false){
          if(isset(self::$keys[$key])&&!empty(self::$keys[$key])&&!$original){
              return self::$keys[$key];
          }
        $file_alias = utils::file_alias($key);

        if($file_alias){
            if($original){
                self::$keys[$key.'-original'] = theme::$config->folders['public_dir'].trim(theme::$config->folders['upload_dir']['origins_frontend'],'/').'/'.trim($file_alias->folder,'/').'/';
                return self::$keys[$key.'-original'];
            }else{
                self::$keys[$key] = theme::$config->folders['public_dir'].trim(theme::$config->folders['upload_dir']['frontend'],'/').'/'.trim($file_alias->folder,'/').'/';
                return self::$keys[$key];

            }
        }
        return 0;
    }
}