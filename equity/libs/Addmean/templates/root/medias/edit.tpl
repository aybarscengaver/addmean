<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/medias/list">Dosya Ayarları</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            İçerik Düzenle - Dosya Ayarları
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">
        </div>
        <form id="module-create" action="root/medias/edit/{$data->id}" method="post">
        {"TANIM"|section:"text#name.full|name__required__none[Ayar Adı|]":$data->name}
        {"ANAHTAR"|section:"text#key.full|key__required__none[Dosya yüklemesi sırasında bu anahtar ile ayarlara ulaşılacaktır.|Türkçe karakter, boşluk vs kullanılamaz. Kullanılsa da otomatik düzeltilir.]":$data->key}
        {"TÜR"|section:"select#type.full|type__required__none[Bu ayarın hangi tür dosya yüklemelerinde kullanılabileceğini belirtir.|][media_types]":$data->type}
        {"YÜKLEME KLASÖRÜ"|section:"text#folder.full|folder__required__none[Dosyaların hangi klasöre yükleneceği| Kök dizinden itibaren]":$data->folder}
        {"UYGULA"|section:"select#modifier.full|modifier__optional__none[Bu ayarın hangi tür dosya yüklemelerinde kullanılabileceğini belirtir.|][image_modification_types]":$data->modifier}
        {"UZANTILAR"|section:"text#extensions.full|extensions__required__none[Hangi uzantılara sahip dosyaların yüklenmesine izin verileceğini belirtir.|][]":$data->extensions}
        {"YÜKSEKLİK"|section:"number#image_height.full|image_height__optional__none[|Pixel][]":$data->image_height}
        {"GENİŞLİK"|section:"number#image_width.full|image_width__optional__none[|Pixel][]":$data->image_width}
        {"DOSYA BOYUTU"|section:"number#max_file_size.full|max_file_size__optional__none[Yüklenebilecek en fazla dosya boyutu|Megabayt]":$data->max_file_size}
        {"YENİDEN ADLANDIR"|section:"checkbox#rename.full|rename__optional__none[|][]":$data->rename}
            {"ALT AYARLAR"|section:"multi_select#sub_settings.full|sub_settings__optional__none[|][file_settings.name&id]":$data->sub_settings}

            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">DÜZENLE</a>
            </div>
        </form>
    </div>
</div>