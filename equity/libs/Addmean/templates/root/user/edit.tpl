<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/user/list">Kullanıcılar</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            İçerik Düzenle - Kullanıcılar
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">

        </div>
        <form id="module-create" action="root/user/edit/{$data->id}" method="post" reset="false">
        {"İSİM"|section:"text#name.full|name__required__none[Kullanıcının gerçek ismi|]":$data->name}
        {"SOYİSİM"|section:"text#surname.full|surname__required__none[Kullanıcının gerçek soyismi|]":$data->surname}
        {"EPOSTA"|section:"email#email.full|email__required__none[Giriş yapmak için kullanılacak eposta adresi|]":$data->email}
        {"ŞİFRE"|section:"password#password.full|password__optional__md5_modifier[Giriş yapmak için kullanılacak eposta adresi|]"}
        {"SEVİYE"|section:"select#level.full|level__required__none_modifier[|][user_levels]":$data->level}
        {"DURUM"|section:"select#status.full|status__required__none[|][status]":$data->status}

            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">DÜZENLE</a>
            </div>
        </form>
    </div>
</div>