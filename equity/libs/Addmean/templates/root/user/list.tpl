<div class="widget">
    <div class="header"><span><span class="ico  gray spreadsheet"></span> Kullanıcılar </span>

        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal add" href="/root/user/create">yeni ekle</a>
        </div>
    </div>
    <!-- End header -->
    <div class="content">
        <form>
            <ul class="dataSet">
                <li>users</li>
                <li>root/user/delete</li>
            </ul>
            <div role="grid" class="dataTables_wrapper" id="static_wrapper"></div>
            <table id="static" class="display   ">
                <thead>
                <tr role="row">
                    <th class="sorting_disabled">İsim Soyisim</th>
                    <th class="sorting_disabled">Eposta</th>
                    <th class="sorting_disabled">Durum</th>
                    <th class="sorting_disabled">İşlem</th>
                </tr>
                </thead>

                <tbody role="alert" aria-live="polite" aria-relevant="all">
                {foreach $data as $key=>$value}
                <tr class="{cycle values='odd,even'}">
                    <td>{$value->name} {$value->surname}</td>
                    <td>{$value->email}</td>
                    <td>{$value->status|statics:'status'}</td>
                    <td align="left">
                                      <span class="tip">
                                          <a href="/root/user/edit/{$value->id}" title="Düzenle">
                                              <img src="/assets/root/images/icon/icon_edit.png">
                                          </a>
                                      </span>
                                      <span class="tip">
                                          <a title="Sil" name="ID:{$value->id}" class="Delete" id="{$value->id}">
                                              <img src="/assets/root/images/icon/icon_delete.png">
                                          </a>
                                      </span>

                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </form>


    </div>
</div>