<html>
<head>
    <title>{$pageTitle}</title>
    <meta name="keywords" content="{$pageKeywords}">
    <meta name="description" content="{$pageDescription}">
    <meta http-equiv="Content-Type" content="text/html;charset={$config->system.charset}">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/assets/resources/javascripts/head.js"></script>
{styles layout="root"}
{plugins layout="root"}
{scripts layout="root"}

    <style type="text/css">
        html {
            background-image: none;
        }
        label.iPhoneCheckLabelOn span {
            padding-left:0px
        }
        #versionBar {
            background-color:#212121;
            position:fixed;
            width:100%;
            height:35px;
            bottom:0;
            left:0;
            text-align:center;
            line-height:35px;
            z-index:11;
            -webkit-box-shadow: black 0px 10px 10px -10px inset;
            -moz-box-shadow: black 0px 10px 10px -10px inset;
            box-shadow: black 0px 10px 10px -10px inset;
        }
        .copyright{
            text-align:center; font-size:10px; color:#CCC;
        }
        .copyright a{
            color:#A31F1A; text-decoration:none
        }
        body{
            background-image:url(/assets/root/images/bg/pick_bg.jpg) !important;
            background-repeat: repeat-x !important;
        }
    </style>
</head>
<body>
<div id="alertMessage"></div>
<div id="successLogin"></div>
<div class="text_success"><img src="/assets/root/images/loadder/loader_green.gif"  alt="addMean" /><span>{t}Please wait...{/t}</span></div>

<div id="login" >
<div class="ribbon"></div>
    <div class="inner">
        <div class="logo" style="background-image:none !important"></div>
        <div class="formLogin">
            <form name="formLogin"  id="formLogin" action="root/user/login" method="post" target="_normal" alert="false" forward="{$return}">

                <div class="tip">
                    <input name="username" type="text"  id="username_id"  title="{t}Username{/t}"   />
                </div>
                <div class="tip">
                    <input name="password" type="password" id="password"   title="{t}Password{/t}"  />
                </div>

                <div class="loginButton">
                    {*<div style="float:left; margin-left:-9px;">
                        <input type="checkbox" id="on_off" name="remember" class="on_off_checkbox"  value="1" />
                        <span class="f_help">{t}Remember Me{/t}</span>
                    </div>*}
                    <div style="float:right; padding:3px 0; margin-right:-12px;">
                        <div>
                            <ul class="uibutton-group">
                                <li><a class="uibutton normal" href="#" id="but_login" >{t}Login{/t}</a></li>
                                <li><a class="uibutton normal" href="#" id="forgetpass">{t}Forgot Password{/t}</a></li>

                            </ul>
                        </div>

                    </div>
                    <div class="clear"></div>
                </div>

            </form>
        </div>
    </div>
    <div class="clear"></div>
    <div class="shadow"></div>
</div>

<!--Login div-->-
</body>
</html>


