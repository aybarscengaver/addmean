<div class="section">
    <label for="{$params.id}">
    {$label}
        <small>{$params.description}</small>
    </label>

    <div>
        <textarea name="{$params.name}"
                  id="{$params.id}"
                  class="{$params.class}"
                  cols="30" rows="10">{$params.selected}</textarea>
        <span class="f_help">{$params.help_text}
        </span>
    </div>
</div>
