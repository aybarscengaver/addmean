<?php
/**
 * User: delirehberi
 * Date: 03.01.2013
 * Time: 10:40
 * Filename: index.php
 * Description: Bu dosya mecburi değil, eğer tpl üzerinde görüntülenecek alanın kullanımında php işlemi gerekecekse, önce
 * burada düzenlemeler yapılıyor datalarla alakalı, bir nevi hook mantığında çalışıyor burası.
 * Usage:
 */
class password_dom extends doms{
    public $_name = "Email Alanı";
    public function init($params,$theme){

        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        return parent::init($params,$theme);

    }
}