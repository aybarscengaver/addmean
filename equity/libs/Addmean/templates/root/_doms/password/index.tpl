<div class="section">
    <label for="{$params.id}">
    {$label}
        <small>{$params.description}</small>
    </label>

    <div>
        <input type="password"
               name="{$params.name}__{$params.validate|default:'optional'}__{$params.modifier|default:'none'}"
               id="{$params.id}" class="{$params.class} validate[{$params.validate}" value="">
        <span class="f_help">{$params.help_text}</span>
    </div>
</div>

