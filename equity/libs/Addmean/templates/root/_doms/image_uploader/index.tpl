<div class="section">
    <label for="{$params.id}">
    {$label}
        <small>{$params.description} [{$params.file_alias|get_sizes}]</small>
    </label>

    <div>



        <input type="hidden" id="{$params.id}" name="{$params.name}" class="validate[{$params.validate}]" value="{$params.selected}">
        <img class="demoImage" id="img_{$params.id}"
             src="{if $params.selected}{$config->folders.upload_dir.frontend}{$params.file_folder}{$params.selected}{else}/assets/root/images/placeholder-image.jpg{/if}"
             onerror="this.src='/assets/root/images/placeholder-image.jpg';getElementById('{$params.id}').value=''" alt="">
        {if $params.selected}
            <a class="deleteImage" id="deleteImage_{$params.id}" href="javascript:void(0)"></a>
        {/if}
        {*Yukarıdaki on error kısmında eleman içeriğini temizleme aslında iyi birşey ama buradaki hatalı içerik getirme sorunu hala bir sorun. @todo *}
        <div class="fix"><br><br></div>

        <div id="tabs_{$params.id}">
            <ul>
                <li><a href="#tabs1">Dosya</a></li>
                <li><a href="#tabs2">Url</a></li>
                {*<li><a href="#tabs3">Google</a></li>*}
            </ul>

            <div id="tabs1">
                <!--File-->
                <input type="file"
                       id="{$params.id}_uploader" class="file {$params.class}" value="{$params.selected}">
                <!--/File-->
            </div>
            <div id="tabs2">
                <!--Url-->
                <input type="url" class="full" name="{$params.id}_upload_from_url" id="{$params.id}_upload_from_url"/>
                <br>
                <a class="upload_from_url uibutton special"
                        data-from="{$params.id}_upload_from_url"
                        href="#" data-alias="{$params.file_alias}"
                        data-image-preview="img_{$params.id}"
                        data-to="{$params.id}"
                        data-main-folder="{$config->folders.upload_dir.frontend}{$params.file_folder}"
                        >İçeri Aktar</a>
                <!--/Url-->
            </div>
            {*<div id="tabs3">Google</div>*}
        </div>
        <span class="f_help">{$params.help_text}</span>
    </div>
</div>

<script type="text/javascript">
    head.ready(function(){
        $("#tabs_{$params.id}").tabs()
        $('#{$params.id}_uploader').uploadify({
            swf:'/assets/resources/plugins/uploadify/uploadify.swf',
            uploader:'/ajax.php?root/medias/uploader/{$params.file_alias}',
            cancelImg:'/assets/resources/plugins/uploadify/cancel.png',
            buttonText:'Dosya Sec',
            width:125,
            height:35,
            pagepath:'/',
            scriptAccess:'allways',
            auto:true,
            multi:false,
            onUploadSuccess:function(file,data,response){
                var data = $.parseJSON(data);
                if(data.type=='success'){
                    $("#{$params.id}").val(data.extra.filename);
                    $("#img_{$params.id}").attr('src','{$config->folders.upload_dir.frontend}{$params.file_folder}'+data.extra.filename);
                }else{
                    jAlert(data.msg,'Hata');
                }
            }
        });

    });


</script>
