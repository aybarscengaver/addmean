<?php
/**
 * User: delirehberi
 * Date: 03.01.2013
 * Time: 10:42
 * Filename: index.php
 * Description:
 * Usage:
 */

use Core\Config;

class image_uploader_dom extends doms
{
    public $_name = "Resim Yükleyici";

    public function beforeInit(&$params)
    {
        $file_alias = utils::file_alias($params['file_alias']);
        if (!$file_alias) {
            throw new Exception(__("Resim ayarı seçilmemiş!"));
        }
        $params['file_alias'] = $file_alias->key;
        $params['file_folder'] = $file_alias->folder;
    }

    public function init($params, $theme)
    {
        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        return parent::init($params,$theme);
    }

    public function getValue($params, $value)
    {
        $file_alias = $params->file_alias;
        $alias = $this->model->getById($file_alias, "*", 'file_settings');
        $column_name = $params->column_name;
        $config = Config::init();

        $value = '/uploads/' . trim($alias->folder, '/') . '/' . $value->$column_name;
        return $value;
    }


}