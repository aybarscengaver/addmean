<div class="section">
    <label for="{$params.id}">
    {$label}
        <small>{$params.description}</small>
    </label>

    <div>
        <input type="text"
               id="autocomplete_{$params.id}" class="{$params.class} validate[{$params.validate}]" value="{$params.text}">
        <input type="hidden" name="{$params.name}" id="{$params.id}" value="{$params.selected}">
        <span class="f_help">{$params.help_text}</span>
    </div>
</div>
<script type="text/javascript">
    head.ready(function(){
        $( "#autocomplete_{$params.id}" ).autocomplete({
            source: "/ajax.php?{$module['application']}/{$module['table_name']}/search&params={($params|serialize)|urlencode}",
            minLength: 2,
            select: function( event, ui ) {
                $("#{$params.id}").val(ui.item.id);
            }
        });
    });
</script>