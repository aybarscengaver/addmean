<?php
/**
 * Created by JetBrains PhpStorm.
 * User: delirehberi
 * Date: 11/02/13
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */

class autocomplete_dom extends doms
{
    public $_name = "Otomatik Tamamlama";

    public function beforeInit(&$params)
    {
        if(isset($params['selected'])){
            preg_match_all('~([^\.]+)\.([^\&]+)\&([^\?]+)\?(.*)~',$params['value'],$tmp);
            list($all,$table_name,$value_row,$key_row,$where_clause) = $tmp;
            $db = db::singleton();
            $db->table=$table_name[0];
            $db->rows="{$table_name[0]}.{$value_row[0]}";
            $db->where="{$key_row[0]}='{$params['selected']}'";
            $db->limit=1;
            $data = $db->read();
            if($data){
                $x = $value_row[0];
                $params['text'] = $data->$x;
            }else{
                $params['text'] = "";
            }
        }
    }

    public function init($params, $theme)
    {
        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        $theme->assign('module',parent::$module);
        return parent::init($params,$theme);
    }

    public function getValue($params, $value)
    {
        $x = $params->column_name;
        $db = db::singleton();
        $db->table = $params->table_name;
        $db->where = "{$params->value_row}='{$value->$x}'";
        $db->rows = "{$params->table_name}.{$params->key_row}";
        $db->limit=1;
        $data = $db->read();
        $x = $params->key_row;
        return $data->$x;
    }

}