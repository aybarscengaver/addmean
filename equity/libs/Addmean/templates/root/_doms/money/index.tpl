<div class="section">
    <label for="{$params.id}">
    {$label}
        <small>{$params.description}</small>
    </label>

    <div>
        <input type="text"
               name="{$params.name}"
               id="{$params.id}" class="spinner {$params.class} validate[{$params.validate}]" value="{$params.selected}" data-min="0">
        <span class="f_help">{$params.help_text}</span>
    </div>
</div>

