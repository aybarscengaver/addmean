<div class="section">
    <label for="{$params.id}">
        {$label}
        <small>{$params.description}</small>
    </label>
    <div>
        <select name="{$params.name}" id="{$params.id}" class="{$params.class} validate[{$params.validate}]">
            <option value="">Seçiniz</option>
            {foreach $params.value as $k=>$v}
                <option value="{$k}" {if $params.selected==$k}selected="selected" {/if}>{$v}</option>
            {/foreach}
        </select>
        <span class="f_help">
        {$params.help_text}
        </span>
    </div>
</div>