<?php
/**
 * User: delirehberi
 * Date: 03.01.2013
 * Time: 10:42
 * Filename: index.php
 * Description:
 * Usage:
 */

class select_dom extends doms
{
    public $_name = "Seçim kutusu";

    public function beforeInit(&$params)
    {
        if (strpos($params['value'], '|')) {
            //Is string data
            $params['value'] = explode('|', $params['value']);
        } else if (strpos($params['value'], '.') && strpos($params['value'], '&')) {
            $params = $this->getTableData($params);
        } else {
            //Is static method
            $v = $params['value'];
            if(is_callable(array('m_statics',$v))){
                $params['value'] = m_statics::$v();
            }else{
                $params['value'] = array();
            }
        }
    }

    public function init($params, $theme)
    {
        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        return parent::init($params,$theme);

    }

    public function getValue($params, $field)
    {
        $column_name = $params->column_name;
        $value = $field->$column_name;
        $manual_data = strpos($params->values,'|');
        if($manual_data&&empty($params->table_name)){     //manual data
              $values = explode('|',$params->values);
            return $values[$value];
        }elseif(!empty($params->table_name)){  //dynamic data
            $db = DB::singleton();
            $db->where="{$params->value_row}='{$value}'";
            $db->limit=1;
            $db->table=$params->table_name;
            $values = $db->read();
            $value_row = $params->key_row;

            $value= $values->$value_row;

        }elseif(!empty($params->values)){ //static data
            $static = $params->values;
            $value= m_statics::$static($value);
        }
        //asd|asd|asd|asd ise manuel giriş
        //asdads ise static veri
        //boş ve table_name falan doluysa veritabanından çekiyor.
        if(is_array($value)){
            $value=_("Geçersiz değer!");
        }
        return  $value;
    }

}