<?php
/**
 * User: delirehberi
 * Date: 03.01.2013
 * Time: 10:42
 * Filename: index.php
 * Description:
 * Usage:
 */

class hierarchy_dom extends doms
{
    public $_name = "Seçim kutusu";

    public function beforeInit(&$params)
    {
        $params = $this->getTableData($params,true);
    }

    public function init($params, $theme)
    {
        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        return parent::init($params,$theme);
        return $theme;
    }

    public function getValue($params, $value)
    {
        /**
         *
         */
        $parent_column = $params->column_name;
        $values=utils::getParentWay($params->table_name,$value->$parent_column);
        $values = array_reverse($values);
        $value = array();
        foreach($values as  $key=>$val){
            $x = $params->key_row;
            $value[] = $val->$x;
        }
        $value = join(' > ',$value);
        return $value;
    }



}