<div class="section">
    <label for="{$params.id}">
    {$label}
        <small>{$params.description}</small>
    </label>
    <div>

        <select name="{$params.name}" id="{$params.id}" class="chzn-select {$params.class} validate[{$params.validate}]">
            <option value="">Seçiniz</option>
            {$params|write_hierarchic:'option':$params.selected}
        </select>
        <span class="f_help">
        {$params.help_text}
        </span>
    </div>
</div>
