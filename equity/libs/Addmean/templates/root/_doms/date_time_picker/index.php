<?php
/**
 * User: delirehberi
 * Date: 03.01.2013
 * Time: 10:40
 * Filename: index.php
 * Description:
 * Usage:
 */
class date_time_picker_dom extends doms{
    public $_name = "Tarih Zaman Seçici";
    public function init($params,$theme){

        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        return parent::init($params,$theme);
    }
}
