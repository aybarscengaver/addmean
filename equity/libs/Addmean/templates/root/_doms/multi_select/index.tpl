{assign var=values value=$params.selected|json_decode}
<div class="section">
    <label for="{$params.id}">
        {$label}
        <small>{$params.description}</small>
    </label>
    <div>
        <select name="{$params.name}[]" id="{$params.id}" multiple="multiple"  style="width:300px" class="{$params.class}  chzn-select validate[{$params.validate}]">
            <option value="">Seçiniz</option>
            {foreach $params.value as $k=>$v}
                <option value="{$k}" {if isset($values) && in_array($k,$values)}selected="selected" {/if}>{$v}</option>
            {/foreach}
        </select>
        <span class="f_help">
            {$params.help_text}
        </span>
    </div>
</div>

