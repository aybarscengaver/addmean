<div class="section">
    <label>{$label}<small>{$params.description}</small></label>
    <div>
        <input class="ck" type="checkbox" {if $params.selected==1}checked="checked" {/if} value="1" name="{$params.name}" id="{$params.id}">
    </div>
</div>
<div class="fix"></div>
