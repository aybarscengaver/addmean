<div class="section">
    <label for="{$params.id}">
    {$label}
        <small>{$params.description}</small>
    </label>

    <div>
        <input type="text"
               name="{$params.name}"
               id="{$params.id}" class="date {$params.class} validate[{$params.validate}]" value="{$params.selected}">
        <span class="f_help">{$params.help_text}</span>
    </div>
</div>

