<div class="section">
    <label for="{$params.id}">
        {$label}
        <small>{$params.description}</small>
    </label>

    <div>
        <input type="hidden" id="{$params.id}" name="{$params.name}" class="validate[{$params.validate}]"
               value="{$params.selected}">

        <a href="" id="img_{$params.id}_href">
            <span id="img_{$params.id}">
            {if $params.selected}{$config->folders.upload_dir.frontend}{$params.file_folder}{$params.selected}{/if}
        </span>
        </a>
        {if $params.selected}
            <a class="deleteImage" id="deleteImage_{$params.id}" href="javascript:void(0)"></a>
        {/if}
        {*Yukarıdaki on error kısmında eleman içeriğini temizleme aslında iyi birşey ama buradaki hatalı içerik getirme sorunu hala bir sorun. @todo *}
        <div class="fix"><br><br></div>

        <div id="tabs_{$params.id}">
            <ul>
                <li><a href="#tabs1">Dosya</a></li>
                {*<li><a href="#tabs3">Google</a></li>*}
            </ul>

            <div id="tabs1">
                <!--File-->
                <input type="file"
                       id="{$params.id}_uploader" class="file {$params.class}" value="{$params.selected}">
                <!--/File-->
            </div>

        </div>
        <span class="f_help">{$params.help_text}</span>
    </div>
</div>

<script type="text/javascript">
    head.ready(function () {
        $("#tabs_{$params.id}").tabs()
        $('#{$params.id}_uploader').uploadify({
            swf: '/assets/resources/plugins/uploadify/uploadify.swf',
            uploader: '/ajax.php?root/medias/uploader/{$params.file_alias}',
            cancelImg: '/assets/resources/plugins/uploadify/cancel.png',
            buttonText: 'Dosya Sec',
            width: 125,
            height: 35,
            pagepath: '/',
            scriptAccess: 'allways',
            auto: true,
            multi: false,
            fileSizeLimit: 1024 * 1024 * 10,
            onUploadSuccess: function (file, data, response) {
                var data = $.parseJSON(data);
                if (data.type == 'success') {
                    $("#{$params.id}").val(data.extra.filename);
                    $("#img_{$params.id}").text('{$config->folders.upload_dir.frontend}{$params.file_folder}' + data.extra.filename);
                    $("#img_{$params.id}_href").attr('href','{$config->folders.upload_dir.frontend}{$params.file_folder}' + data.extra.filename);
                } else {
                    jAlert(data.msg, 'Hata');
                }
            }
        });

    });


</script>