<?php
/**
 * User: delirehberi
 * Date: 03.01.2013
 * Time: 10:40
 * Filename: index.php
 * Description:
 * Usage:
 */
use recaptcha\Captcha;

class captcha_dom extends doms{
    public $_name = "Güvenlik Kodu";
    public function init($params,$theme){
        $width = '100';
        $height = '30';
        $characters = '6';
        $imagepath = theme::$config->folders['upload_dir']['backend'].'captcha';
        $imageurl = '';
        $fontpath = theme::$config->folders['document_root'].'/assets/resources/fonts/monofont.ttf';
        $captcha = new Captcha();
        $params['captcha'] = "data:image/jpg;base64,".$captcha->create_captchaImage($fontpath,$imagepath,$imageurl,$width,$height,$characters);
        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        return parent::init($params,$theme);
    }
}
