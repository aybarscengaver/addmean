<div class="section">
    <label for="{$params.id}">{$label}
        <small>{$params.description}</small>
    </label>

    <div><textarea class="text_editor"
                   name="{$params.name}"
                   cols="65" rows="20"
                   id="{$params.id}">{$params.selected}</textarea>
    </div>
</div>
