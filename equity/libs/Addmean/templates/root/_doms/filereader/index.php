<?php
/**
 * User: delirehberi
 * Date: 10.01.2013
 * Time: 11:48
 * Filename: filereader.php
 * Description:
 * Usage:
 */
class filereader_dom extends doms{
    public function beforeInit(&$params){
        $url = new Url();
        $url->renderHtaccess();
        $file = $params['selected'];
        $file_content = file_get_contents($file);
        $params['selected'] = $file_content;
    }
}