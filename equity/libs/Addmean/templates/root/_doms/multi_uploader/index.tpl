<div class="section">
    {*<label for="{$params.id}">*}
    {*{$label}*}
        {*<small>{$params.description}</small>*}
    {*</label>*}

    <div>
        <ul id="{$params.id}_images" class="files"></ul>
        <div class="fix"><br></div>
        <input type="file"
               id="{$params.id}_uploader" class="file {$params.class}" value="{$params.selected}">

        <div id="{$params.id}_inputs"></div>
        <span class="f_help">{$params.help_text}</span>
    </div>
</div>

<script type="text/javascript">
    head.ready(function(){
        var x = 0;
        $('#{$params.id}_uploader').uploadify({
            swf:'/assets/resources/plugins/uploadify/uploadify.swf',
            uploader:'/ajax.php?root/medias/uploader/{$params.file_alias}',
            cancelImg:'/assets/resources/plugins/uploadify/cancel.png',
            buttonText:'Dosya Sec',
            width:125,
            height:35,
            pagepath:'/',
            scriptAccess:'allways',
            auto:true,
            multi:true,
            uploadLimit:9,
            onUploadSuccess:function(file,data,response){
               var data = $.parseJSON(data);
                if(data.type=='success'){
                    $("#{$params.id}").val(data.extra.filename);
                    var image = ""
                    +"<li><img src='{$config->folders.upload_dir.frontend}{$params.file_folder}"+data.extra.filename+"'" +
                            " width='78px' height='78px'/></li>";
                    $("#{$params.id}_images").append(image);
                    var input="" +
                            '<input type="hidden" id="{$params.id}'+x+'" ' +
                    'name="{$params.name}[]" class="validate[{$params.validate}]" value="'+data.extra.filename+'">';
                    $("#{$params.id}_inputs").append(input);
                    x++;
                }else{
                    jAlert(data.msg,'Hata');
                }
            }
        });

    })
</script>