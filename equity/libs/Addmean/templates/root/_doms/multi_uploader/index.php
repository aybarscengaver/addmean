<?php
/**
 * User: delirehberi
 * Date: 03.01.2013
 * Time: 10:42
 * Filename: index.php
 * Description:
 * Usage:
 */

class multi_uploader_dom extends doms
{
    public $_name = "Çoklu Yükleyici";

    public function beforeInit(&$params)
    {
        $file_alias = utils::file_alias($params['file_alias']);
        $params['file_alias'] = $file_alias->key;
        $params['file_folder'] = $file_alias->folder;
    }

    public function init($params, $theme)
    {
        $theme->assign('params', $params);
        $theme->assign('label', $params['label']);
        return parent::init($params,$theme);

    }



}