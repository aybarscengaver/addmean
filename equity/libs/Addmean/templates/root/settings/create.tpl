<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/settings/list">Ayarlar</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            İçerik Ekle - Ayarlar
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">

        </div>
        <form id="module-create" action="root/settings/create" method="post">
        {"TANIM"|section:"text#name.full|name__required__none[Ayar Adı|]"}
        {"ANAHTAR"|section:"text#key.full|key__required__none[Ayara ulaşmak için kullanılacaktır.|Türkçe karakter, boşluk vs kullanılamaz. Kullanılsa da otomatik düzeltilir.]"}
        {"GRUP"|section:"select#group.full|group__required__none[Bu ayarın hangi ayar grubuna dahil olduğunu belirtir.|][setting_groups]"}
        {"DEĞER"|section:"textarea#value.full|value__required__none[|][]"}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">EKLE</a>
            </div>
        </form>
    </div>
</div>