<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/settings/list">Ayarlar</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            Dosya Düzenle - {$file}
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">
         Bu kısımda gördüğünüz dosya içeriği düzenlenip kayıt edilemez. Sayfayı açtığınızda otomatik olarak dosya gerekiyorsa
            tekrar render edilir. Örneğin htaccess dosyası yeni eklenen modüllere göre tekrar oluşturulur.
        </div>
        <form id="module-create" action="root/settings/parser/{$file}" method="post">
        {"DOSYA İÇERİĞİ"|section:"filereader#file.full|file__required__none[|]":$config->files.htaccess}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">EKLE</a>
            </div>
        </form>
    </div>
</div>