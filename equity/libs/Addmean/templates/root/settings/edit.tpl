<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/settings/list">Ayarlar</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            İçerik Düzenle - Ayarlar
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">

        </div>
        <form id="module-create" action="root/settings/edit/{$data->id}" method="post" reset="false">
        {"TANIM"|section:"text#name.full|name__required__none[Ayar Adı|]":$data->name}
        {"ANAHTAR"|section:"text#key.full|key__required__none[Ayara ulaşmak için kullanılacaktır.|Türkçe karakter, boşluk vs kullanılamaz. Kullanılsa da otomatik düzeltilir.]":$data->key}
        {"GRUP"|section:"select#group.full|group__required__none[Bu ayarın hangi ayar grubuna dahil olduğunu belirtir.|][setting_groups]":$data->group}
        {"DEĞER"|section:"textarea#value.full|value__required__none[|][]":$data->value}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">KAYDET</a>
            </div>
        </form>
    </div>
</div>