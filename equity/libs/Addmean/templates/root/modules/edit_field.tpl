<div class="widget">
    <div class="header">
        <span>
            <span class="ico gray add"></span>
            Özellik Düzenle - {$module->name}
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">
            Modüle özellik ekleme işlemini kolaylıkla yapın.
        </div>
        <form id="module-create" action="root/modules/edit_field/{$field->id}/{$module->id}" method="post" reset="false">
        {'SÜTUN ADI'|section:'text#column_name.full|column_name__required__none[Tabloda oluşturulacak sütun için isim|]':$field->column_name}
        {'ETİKET'|section:'text#label.full|label__required__none[Özelliğin etiketi|]':$field->label}
        {'TÜR'|section:'select#type.full|type__required__none[Özelliğin hangi tür içeriğe sahip olacağı|][field_types]':$field->type}

        {'DEĞERLER'|section:'text#values.full|values__optional__none[Eğer select türünde bir özellik eklenecekse kullanılır, bir static ile ilişkilendirilmek istenirse sadece o metodun adının yazılması yeterli olur.| Pipe (|) işareti ile ayrılarak yazılmalıdır]':$field->values}

        {'ÖNTANIMLI DEĞER'|section:'text#default_value.full|default_value__optional__none[Hiç bir değer girilmediğinde otomatik atanacak değer|]'}

        {'DOSYA ANAHTARI'|section:'select#file_alias.full|file_alias__optional__none[Bir dosya yükleyici yada resim yükleyici seçildiyse bu alana hangi ayarlar ile yükleneceği yazılacak.|][file_settings.name&id]':$field->file_alias}

        {'TABLO ADI'|section:'text#table_name.full|table_name__optional__none[Select türü için veriler tablodan alınacaksa tablo adı yazılmalıdır|]':$field->table_name}
        {'DEĞER SüTUNU'|section:'text#key_row.full|key_row__optional__none[Select türü için anahtar olarak tablodan çekilecek sütun adı|]':$field->key_row}
        {'ETİKET SüTUNU'|section:'text#value_row.full|value_row__optional__none[Select türü için değer olarak tablodan çekilecek sütun|]':$field->value_row}
        {'KOŞUL'|section:'text#where_clause.full|where_clause__optional__none[Tablodan veri çekilirken kullanılacak koşul|]':$field->where_clause}

        {'İŞLE'|section:'select#modifier.full|modifier__optional__none[Girilen veri herhangi bir işlemden geçecek mi?|][modifiers]':$field->modifier}
        {'ZORUNLU ALAN'|section:'checkbox#required. |required__optional__none[Bu alanın doldurulması zorunlu mu?|]':$field->required}
        {'YARDIM METNİ'|section:'text#help_text.full|help_text__optional__none[|Bu yazıyı gördüğünüz yerde çıkacak metin]':$field->help_text}
        {'AÇIKLAMA'|section:'text#description.full|description__optional__none[Bu yazıyı gördüğünüz yerde çıkacak metin|]':$field->description}

        {'LİSTELEMEDE GÖZÜKSÜN'|section:'checkbox#list. |list__optional__none[Listeleme sayfasında bu sütun görüntülensin mi ?|]':$field->list}

        {'FİLTRE OLSUN'|section:'checkbox#filter. |filter__optional__none[Filtreleme işlemine dahil olsun mu?|]':$field->filter}
        {'DÜZENLEMEDE GÖZÜKSÜN'|section:'checkbox#edit. |edit__optional__none[Ekleme,düzenleme ekranında bu alan görünsün mü?|][]':$field->edit}

        {'ÖNYÜZ FORMDA GÖZÜKSÜN'|section:'checkbox#frontend_form. |frontend_form__optional__none[Site önyüzünde oluşturulacak formda bu alan görüntülensin mi ?|]':$field->frontend_form}

            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">DÜZENLE</a>
            </div>
        </form>
    </div>
</div>