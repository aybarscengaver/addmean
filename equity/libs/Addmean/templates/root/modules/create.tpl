<div class="widget">
    <div class="header">
        <span>
            <span class="ico gray add"></span>
            Modül Oluştur
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">
            Modül ekleme işlemini kolaylıkla yapın.
        </div>
        <form id="module-create" action="root/modules/create" method="post">
            {'MODÜL ADI'|section:'text#module_name.full|name__required__none[Modülün için gerekli isim|Alias otomatik oluşturulacaktır]'}
            {*type#id.class|name.[validate]~Description^HelpText~*}
            {'TABLO ADI'|section:'text#table_name.full|table_name__required__none[Modülün kullanacağı tablo adı|Eğer aynı isimde bir tablo varsa yeni bir tane oluşturulmayacaktır.]'}
            {'SEO'|section:'text#seo.full|seo__optional__none[Modülün önyüzde yüklenmesi sırasında kullanıcı dostu bağlantı adresi|Belirtilmez ise otomatik olarak oluşturulur.]'}
            {'TÜR'|section:'select#type.full|type__optional__none[Modülün hangi kısım için kullanıldığı|][module_types]'}
            {'SIRALAMA'|section:'checkbox#order.full|order__optional__checkbox_modifier[İçerikler arasında sıralama|]'}
            {'ÇOKLU DİL'|section:'multi_select#multi_language.full|multi_language__optional__multiselect_modifier[|][languages]'}
            {'DURUM'|section:'select#status.full|status__required__none[|][status]'}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">OLUŞTUR</a>
            </div>
        </form>
    </div>
</div>