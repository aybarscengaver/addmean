<div class="widget">
    <div class="header">
        <span>
            <span class="ico gray pencil"></span>
            İçerik Düzenle - {$module->name}
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">

        </div>
        <form id="module-create" action="root/{$module->table_name}/edit/{$data->id}" method="post" reset="false"
              forward="/root/{$module->table_name}/edit/{$data->id}">
            {if $language_key}
                <input type="hidden" name="l" value="{$language_key}"/>
            {/if}
            <!-- object -->
        {foreach $module_fields as $key=>$value}
            {if $value->edit==1}
                {assign var='column_name' value=$value->column_name}
                {$value|section:'':$data->$column_name}
            {/if}
        {/foreach}
            <!-- object -->
        {if isset($module_relations['many_to_many'])}
            <!-- Many To Many - Karmaşık İlişki -->
            {foreach $module_relations['many_to_many'] as $rel}
                {$rel|many_to_many:$data->id}
            {/foreach}
            <!-- /Many To Many - Karmaşık İlişki -->
        {/if}

        {if isset($module_relations['one_to_many'])}
            <!--OneToMany Relation -->
            {foreach $module_relations['one_to_many'] as $rel}
                <div class="widget">
                    <div class="header">
        <span>
            <span class="ico gray target"></span>
           Bağlantılı İçerikler - {$rel.info->module_name}
        </span>
                    </div>
                    {foreach $rel.datas as $key=>$value}
                        <div class="content">
                            <ul class="dataSet">
                                <li>{$rel.info->module_table_name}</li>
                                <li>root/{$rel.info->module_table_name}/delete</li>
                            </ul>
                            <a class="uibutton special relationDelete" id="{$value->id}"
                               name="ID : {$value->id}">Sil</a>
                            <input type="hidden" name="{$rel.info->module_table_name}[{$value->id}][id][{php} echo form::$count;{/php}]" value="{$value->id}"/>
                            {foreach $rel.fields as $key2=>$value2}
                                {if $value2->edit==1}
                                    {assign var=z value=$value2->column_name}
                                    {$value2|section:(($rel.info->module_table_name)):$value->$z:$value->id}
                                {/if}
                            {/foreach}
                        </div>
                    {/foreach}
                    <div class="content">
                    {* Boş bir tane bağlantılı içerik mutlaka olacak *}
                    {for $i=1 to $rel.info->repeat_count}
                        {foreach $rel.fields as $key=>$value}
                            {$value|section:($rel.info->module_table_name):false:($i|cat:'_new')}
                        {/foreach}
                    {/for}
                    </div>

                </div>
            {/foreach}
            <!-- /OneToMany Relation -->
        {/if}

            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">KAYDET</a>
            </div>
        </form>
    </div>
</div>