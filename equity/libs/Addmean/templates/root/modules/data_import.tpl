<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/{$module->table_name}/list">{$module->name}</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            Verileri İçeri Aktar - {$module->name}
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">
        </div>
        <form id="module-create-data" action="root/{$module->table_name}/data_import" method="post" forward="/root/{$module->table_name}/list">
            {'VERİLER'|section:'textarea#query.full|query__required__none[Daha önce alınan yedek|Json formatında olmalıdır]'}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">EKLE</a>
            </div>

        </form>
    </div>
</div>
