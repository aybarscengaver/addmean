<div class="widget">
    <div class="header"><span><span class="ico  gray spreadsheet"></span> Modüller </span>

        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal add" href="/root/{$module_name}/create">yeni ekle</a>{*
            <a class="uibutton icon next special" href="/root/{$module_name}/export">Dışarı Aktar</a>*}
        </div>
    </div>
    <!-- End header -->
    <div class="content">
        <form>
            <ul class="dataSet">
                <li>{$module_name}</li>
                <li>root/{$module_name}/delete</li>
            </ul>
            <div role="grid" class="dataTables_wrapper" id="static_wrapper"></div>
            <table id="static" class="display   ">
                <thead>
                <tr role="row">
                    <th class="sorting_disabled">İsim</th>
                    <th class="sorting_disabled">Tür</th>
                    <th class="sorting_disabled">Durum</th>
                    <th class="sorting_disabled">Tablo Adı</th>
                    <th class="sorting_disabled">İşlem</th>
                </tr>
                </thead>

                <tbody role="alert" aria-live="polite" aria-relevant="all">
                {foreach $data as $key=>$value}
                <tr class="{cycle values='odd,even'}">
                    <td>{$value->name}</td>
                    <td>{$value->type|statics:'module_types'}</td>
                    <td>{$value->status|statics:'status'}</td>
                    <td>{$value->table_name}</td>
                    <td align="left">
                        <span class="tip">
                          <a title="Düzenle" href="/root/{$module_name}/edit/{$value->id}">
                              <img src="/assets/root/images/icon/icon_edit.png">
                          </a>
                        </span>
                        <span class="tip">
                          <a title="Sil" name="{$value->name}" class="Delete" id="{$value->id}">
                              <img src="/assets/root/images/icon/icon_delete.png">
                          </a>
                        </span>
                        <span class="tip">
                            <a href="/root/{$module_name}/fields/{$value->id}" title="Özellikler" name="Özellikler"
                               class="fields">
                                <img src="/assets/root/images/icon/color_18/attachment.png" alt="">
                            </a>
                        </span>
                        <span class="tip">
                            <a class="relations" href="/root/module_relations/list/{$value->id}" title="İlişkiler"
                               name="İlişkiler">
                                <img src="/assets/root/images/icon/color_18/connect.png" alt="">
                            </a>
                        </span>
                        <span class="tip">
                            <a class="export" href="/root/modules/export/{$value->id}/" target="_export" title="Dışarı Aktar" name="Dışarı Aktar">
                                <img src="/assets/root/images/icon/color_18/download2.png" alt="">
                            </a>
                        </span>
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </form>


    </div>
</div>