<div class="widget">
    <div class="header"><span><span class="ico  gray spreadsheet"></span> {$module->name} </span>

        <div class="searchBox" style="margin-top:-26px">
            <span class="tip" style="padding:0;"><a class="uibutton icon normal add" href="/root/{$module->table_name}/create">yeni ekle</a></span>
            <span class="tip" style="padding:0;"><a class="uibutton icon normal prev" href="/root/{$module->table_name}/data_import">veriyi içeri aktar</a></span>
            <span class="tip" style="padding:0;"><a class="uibutton icon normal next" target="_blank" href="/root/{$module->table_name}/data_export" title="Farklı kaydedebilirsiniz">veriyi dışarı aktar</a></span>
        </div>
    </div>
    <!-- End header -->
    <div class="content">
        <form>
            <ul class="dataSet">
                <li>{$module->table_name}</li>
                <li>root/{$module->table_name}/delete/{if $language_key}{$language_key}{/if}</li>
            </ul>
            <div role="grid" class="dataTables_wrapper" id="static_wrapper"></div>
            <table id="static" class="display   ">
                <thead>
                <tr role="row">
                    {foreach $module_columns as $key=>$value}
                    {if $value->list==1}
                        <th class="sorting_disabled">{$value->label}</th>
                    {/if}
                    {/foreach}
                        <th class="sorting_disabled">Seo</th>
                    <th class="sorting_disabled">İşlem</th>
                </tr>
                </thead>

                <tbody role="alert" aria-live="polite" aria-relevant="all" data-sort-url="root/modules/sort_module_datas/{$module->id}" {if $module->order==1}class="sortable"{/if}>
                {foreach $data as $key=>$value}
                <tr class="{cycle values='odd,even'} module_data " data-id="{$value->id}"  id="id_{$value->id}">
                    {foreach $module_columns as $_key=>$_value}
                       {if $_value->list}
                           <td>{$_value|section:$value:true}</td>
                       {/if}
                    {/foreach}
                    <td>{$value->seo}</td>

                    <td align="left">

                                      <span class="tip">
                                          <a href="/root/{$module->table_name}/edit/{$value->id}" title="Düzenle">
                                              <img src="/assets/root/images/icon/icon_edit.png">
                                          </a>
                                      </span>

                                      <span class="tip">
                                          <a title="Sil" name="ID:{$value->id}" class="Delete" id="{$value->id}">
                                              <img src="/assets/root/images/icon/icon_delete.png">
                                          </a>
                                      </span>

                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>

            <div class="pages" data-target="/root/{$module->table_name}/list/" data-total-count="{$data_count}" data-limit="{$list_limit}" data-current-page="{$current_page}"></div>
        </form>


    </div>
</div>

{$special_buttons}