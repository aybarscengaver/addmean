<div class="widget">
    <div class="header">
        <span>
            <span class="ico gray add"></span>
            Özellik Ekle - {$module->name}
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">
            Modüle özellik ekleme işlemini kolaylıkla yapın.
        </div>
        <form id="module-create" action="root/modules/create_field/{$module->id}" method="post" reset="true" forward="/root/modules/create_field/{$module->id}">
        {'SÜTUN ADI'|section:'text#column_name.full|column_name__required__none[Tabloda oluşturulacak sütun için isim|]'}
        {'ETİKET'|section:'text#label.full|label__required__none[Özelliğin etiketi|]'}
        {'TÜR'|section:'select#type.full|type__required__none[Özelliğin hangi tür içeriğe sahip olacağı|][field_types]'}

        {'DEĞERLER'|section:'text#values.full|values__optional__none[Eğer select türünde bir özellik eklenecekse kullanılır, bir static ile ilişkilendirilmek istenirse sadece o metodun adının yazılması yeterli olur.| Pipe (|) işareti ile ayrılarak yazılmalıdır]'}

        {'DOSYA ANAHTARI'|section:'select#file_alias.full|file_alias__optional__none[Bir dosya yükleyici yada resim yükleyici seçildiyse bu alana hangi ayarlar ile yükleneceği yazılacak.|][file_settings.name&id]'}

        {'ÖNTANIMLI DEĞER'|section:'text#default_value.full|default_value__optional__none[Hiç bir değer girilmediğinde otomatik atanacak değer|]'}

        {'TABLO ADI'|section:'text#table_name.full|table_name__optional__none[Select türü için veriler tablodan alınacaksa tablo adı yazılmalıdır|]'}
        {'DEĞER SüTUNU'|section:'text#key_row.full|key_row__optional__none[Select türü için anahtar olarak tablodan çekilecek sütun adı|]'}
        {'ETİKET SüTUNU'|section:'text#value_row.full|value_row__optional__none[Select türü için değer olarak tablodan çekilecek sütun|]'}
        {'KOŞUL'|section:'text#where_clause.full|where_clause__optional__none[Tablodan veri çekilirken kullanılacak koşul|]'}

        {'İŞLE'|section:'select#modifier.full|modifier__optional__none[Girilen veri herhangi bir işlemden geçecek mi?|][modifiers]'}
        {'ZORUNLU ALAN'|section:'checkbox#required. |required__optional__none[Bu alanın doldurulması zorunlu mu?|]'}
        {'YARDIM METNİ'|section:'text#help_text.full|help_text__optional__none[|Bu yazıyı gördüğünüz yerde çıkacak metin]'}
        {'AÇIKLAMA'|section:'text#description.full|description__optional__none[Bu yazıyı gördüğünüz yerde çıkacak metin|]'}

        {'LİSTELEMEDE GÖZÜKSÜN'|section:'checkbox#list. |list__optional__none[Listeleme sayfasında bu sütun görüntülensin mi ?|]'}
        {*{'FİLTRE OLSUN'|section:'checkbox#filter. |filter__optional__none[Filtreleme işlemine dahil olsun mu?|]'}*}
        {'DÜZENLEMEDE GÖZÜKSÜN'|section:'checkbox#edit. |edit__optional__none[Ekleme,düzenleme ekranında bu alan görünsün mü?|][][1]'}
        {'ÖNYÜZ FORMDA GÖZÜKSÜN'|section:'checkbox#frontend_form. |frontend_form__optional__none[Site önyüzünde oluşturulacak formda bu alan görüntülensin mi ?|][][1]'}

            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">OLUŞTUR</a>
            </div>
        </form>
    </div>
</div>