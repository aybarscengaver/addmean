<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/{$module->table_name}/list">{$module->name}</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            İçerik Ekle - {$module->name}
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">
        </div>
        <form id="module-create-data" action="root/{$module->table_name}/create" method="post" forward="/root/{$module->table_name}/create">
            {if $language_key}
                <input type="hidden" name="l" value="{$language_key}"/>
            {/if}
        <!--Module Fields -->
        {foreach $module_fields as $key=>$value}
            {if $value->edit==1}
                {$value|section}
            {/if}
        {/foreach}
        <!-- / Module Fields -->

        {if isset($module_relations['many_to_many'])}
            <!-- Many To Many - Karmaşık İlişki -->
            {foreach $module_relations['many_to_many'] as $rel}
                {$rel|many_to_many}
            {/foreach}
            <!-- /Many To Many - Karmaşık İlişki -->
        {/if}



            {if isset($module_relations['one_to_many'])}
                <!--One To Many - Doğrusal İlişki -->
                {foreach $module_relations['one_to_many'] as $rel}
                    <div class="widget">
                        <div class="header">
        <span>
            <span class="ico gray target"></span>
           Bağlantılı İçerikler - {$rel.info->module_name}
        </span>
                        </div>
                        {for $i=1 to $rel.info->repeat_count }
                            <div class="content">
                            {foreach $rel.fields as $key=>$value}
                            	{$value|section:($rel.info->module_table_name):false:($i|cat:'_new')}
                        	{/foreach}
                            </div>
                        {/for}

                    </div>
                {/foreach}
                <!-- /One To Many - Doğrusal İlişki -->
            {/if}

            <div class="section">
            <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">EKLE</a>
        </div>

        </form>
    </div>
</div>



















