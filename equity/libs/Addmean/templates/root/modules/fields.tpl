<div class="widget">
    <div class="header"><span><span class="ico  gray spreadsheet"></span> {$module->name} -  Özellikler </span>

        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal add" href="/root/modules/create_field/{$module->id}{if $language_key}?l={$language_key}{/if}">yeni ekle</a>
        </div>
    </div>
    <!-- End header -->
    <div class="content">
        <form>
            <ul class="dataSet">
                <li>module_fields</li>
                <li>root/modules/delete_field{if $language_key}/{$language_key}{/if}</li>
            </ul>
            <div role="grid" class="dataTables_wrapper" id="static_wrapper"></div>
            <table id="static" class="display   ">
                <thead>
                <tr role="row">
                    <th class="sorting_disabled">Etiket</th>
                    <th class="sorting_disabled">Tür</th>
                    <th class="sorting_disabled">Öntanımlı Değer</th>
                    <th class="sorting_disabled">Sütun Adı</th>
                    <th class="sorting_disabled">İşlem</th>
                </tr>
                </thead>

                <tbody role="alert" aria-live="polite" aria-relevant="all" class="sortable" data-sort-url="root/modules/sort_fields/{$module->id}">
                {foreach $data as $key=>$value}
                <tr class="{cycle values='odd,even'}" id="id_{$value->id}">
                    <td>{$value->label}</td>
                    <td>{$value->type|statics:'field_types'}</td>
                    <td>{$value->values}</td>
                    <td>{$value->column_name}</td>
                    <td align="left">
                                      <span class="tip">
                                          <a title="Düzenle" href="/root/modules/edit_field/{$value->id}{if $language_key}?l={$language_key}{/if}">
                                              <img src="/assets/root/images/icon/icon_edit.png">
                                          </a>
                                      </span>
                                      <span class="tip">
                                          <a title="Sil" name="{$value->label}" class="Delete" id="{$value->id}">
                                              <img src="/assets/root/images/icon/icon_delete.png">
                                          </a>
                                      </span>
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </form>


    </div>
</div>