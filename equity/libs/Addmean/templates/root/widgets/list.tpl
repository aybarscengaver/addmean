<div class="widget">
    <div class="header"><span><span class="ico  gray sphere"></span> Widget Yönetimi </span>
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal add" href="/root/widgets/create">yeni ekle</a>
        </div>
    </div>
    <!-- End header -->
    <div class="content">
        <form>
            <ul class="dataSet">
                <li>widgets</li>
                <li>root/widgets/delete</li>
            </ul>
            <div role="grid" class="dataTables_wrapper" id="static_wrapper"></div>
            <table id="static" class="display   ">
                <thead>
                <tr role="row">
                    <th class="sorting_disabled">Tanım</th>
                    <th class="sorting_disabled">Ana Modül</th>
                    <th class="sorting_disabled">Durum</th>
                    <th class="sorting_disabled">İşlem</th>
                </tr>
                </thead>

                <tbody role="alert" aria-live="polite" aria-relevant="all">
                {foreach $data as $key=>$value}
                <tr class="{cycle values='odd,even'}">
                    <td>{$value->name}</td>
                    <td>{$value->main_module}</td>
                    <td>{$value->status|statics:'status'}</td>
                    <td align="left">
                                      <span class="tip">
                                          <a href="/root/widgets/edit/{$value->id}" title="Düzenle">
                                              <img src="/assets/root/images/icon/icon_edit.png">
                                          </a>
                                      </span>
                                      <span class="tip">
                                          <a title="Sil" name="ID:{$value->id}" class="Delete" id="{$value->id}">
                                              <img src="/assets/root/images/icon/icon_delete.png">
                                          </a>
                                      </span>

                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </form>


    </div>
</div>