<div class="widget">
    <div class="header">
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/widgets/list">Widget Yönetimi</a>
        </div>
        <span>
            <span class="ico gray add"></span>
            İçerik Ekle - Widget
        </span>
    </div>
    <div class="content">
        <div class="boxtitle">

        </div>
        <form id="module-create" action="root/widgets/create" method="post">
        {"TANIM"|section:"text#name.full|name__required__none[Widget'ı tanımlayıcı bir isim|]"}
        {"ANA MODUL"|section:"select#main_module.full|main_module__required__none[Widget'ın işlevini gerçekleştirdiği modül|][modules.name&table_name][]"}
        {"DOSYA YOLU"|section:"text#directory_name.full|directory_name__required__none[Widget dosyalarının bulunduğu dizin|]"}
        {"BÖLÜM"|section:"select#application.full|application__required__none[Widget'ın kullanılacağı kısım|][applications][2]"}
        {"DURUM"|section:"select#status.full|status__required__none[|][status][2]"}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">EKLE</a>
            </div>
        </form>
    </div>
</div>

