<div class="widget">
    <div class="header">
        <span>
            <span class="ico gray add"></span>
            İlişki Oluştur - {$module->name}
        </span>
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/{$module_name}/list/{$id}">ilişkiler</a>
        </div>
    </div>
    <div class="content">
        <div class="boxtitle">
            Modülleri birbirine bağlamak içindir.
        </div>
        <form id="module-relation-create" action="root/{$module_name}/create/{$id}" method="post">
        {*Daha sonra form oluşturan bir helper yazılacak @todo *}
            {'ÜST MODUL'|section:'select#master_module.full|master_module__required__none[Bağlantının sağlanacağı ana modül|][modules.name&table_name?]':$id}
            {*type#id.class|name.[validate]~Description^HelpText~*}
            {'ALT MODUL'|section:'select#slave_module.full|slave_module__required__none[Ana modüle bağlanacak modül|][modules.name&table_name?]'}
            {'Tekrar Sayısı'|section:'number#repeat_count.full|repeat_count__required__none[Alt modülün özelliklerinin maximum kaç tane olabileceği|]':1}
            {'TÜR'|section:'select#type.full|type__required__none[Modülün hangi kısım için kullanıldığı|][relation_types]'}
            {'Tablo / Sütun Adı'|section:'text#table_name.full|table_name__required__none[Ortak kullanılacak tablo/sütun adı| ][]'}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">OLUŞTUR</a>
            </div>
        </form>
    </div>
</div>