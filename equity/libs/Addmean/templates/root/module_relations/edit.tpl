<div class="widget">
    <div class="header">
        <span>
            <span class="ico gray add"></span>
            İlişki Düzenle - {$module->name}
        </span>
        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal prev" href="/root/{$module_name}/list/{$data->master_module}">ilişkiler</a>
        </div>
    </div>
    <div class="content">
        <div class="boxtitle">
            Modülleri birbirine bağlamak içindir.
        </div>
        <form id="module-relation-create" action="root/{$module_name}/edit/{$data->id}" method="post" reset="false">
        {*Daha sonra form oluşturan bir helper yazılacak @todo *}
            {'ÜST MODUL'|section:'select#master_module.full|master_module__required__none[Bağlantının sağlanacağı ana modül|][modules.name&table_name?]':$data->master_module}
            {*type#id.class|name.[validate]~Description^HelpText~*}
            {'ALT MODUL'|section:'select#slave_module.full|slave_module__required__none[Ana modüle bağlanacak modül|][modules.name&table_name?]':$data->slave_module}
            {'Tekrar Sayısı'|section:'number#repeat_count.full|repeat_count__required__none[Alt modülün özelliklerinin maximum kaç tane olabileceği|]':$data->repeat_count}
            {'TÜR'|section:'select#type.full|type__required__none[Modülün hangi kısım için kullanıldığı|][relation_types]':$data->type}
            {'Tablo Adı'|section:'text#table_name.full|table_name__required__none[Ortak kullanılacak tablo adı| ][]':$data->table_name}
            <div class="section">
                <a class="uibutton submit_form" href="javascript:void(0)" rel=1 title="Kayıt ediliyor.">GÜNCELLE</a>
            </div>
        </form>
    </div>
</div>