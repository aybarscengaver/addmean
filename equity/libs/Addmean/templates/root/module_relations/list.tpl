<div class="widget">
    <div class="header"><span><span class="ico  gray spreadsheet"></span> Modüller İlişkileri </span>

        <div class="searchBox" style="margin-top:-26px">
            <a class="uibutton icon normal add" href="/root/{$module_name}/create/{$id}">yeni ekle</a>
        </div>
    </div>
    <!-- End header -->
    <div class="content">
        <form>
            <ul class="dataSet">
                <li>{$module_name}</li>
                <li>root/{$module_name}/delete</li>
            </ul>
            <div role="grid" class="dataTables_wrapper" id="static_wrapper"></div>
            <table id="static" class="display   ">
                <thead>
                <tr role="row">
                    <th class="sorting_disabled">Üst Modül</th>
                    <th class="sorting_disabled">Alt Modül</th>
                    <th class="sorting_disabled">İlişki Türü</th>
                    <th class="sorting_disabled">Tablo Adı</th>
                    <th class="sorting_disabled">İşlem</th>
                </tr>
                </thead>

                <tbody role="alert" aria-live="polite" aria-relevant="all">
                {foreach $data as $key=>$value}
                <tr class="{cycle values='odd,even'}">
                    <td>{$value->master_module|get_value:'modules':'table_name':'name'}</td>
                    <td>{$value->slave_module|get_value:'modules':'table_name':'name'}</td>
                    <td>{$value->type|statics:'relation_types'}</td>
                    <td>{$value->table_name}</td>
                    <td align="left">
                                      <span class="tip">
                                          <a title="Düzenle" href="/root/{$module_name}/edit/{$value->id}">
                                              <img src="/assets/root/images/icon/icon_edit.png">
                                          </a>
                                      </span>
                                      <span class="tip">
                                          <a title="Sil" name="{$value->id}" class="Delete" id="{$value->id}">
                                              <img src="/assets/root/images/icon/icon_delete.png">
                                          </a>
                                      </span>
                        <span class="tip">
                            <a href="/root/{$module_name}/fields/{$value->id}" title="Özellikler" name="Özellikler" class="fields">
                                <img src="/assets/root/images/icon/color_18/attachment.png" alt="">
                            </a>
                        </span>
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </form>


    </div>
</div>