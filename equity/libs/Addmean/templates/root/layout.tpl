<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>{$pageTitle}</title>
    <meta name="keywords" content="{$pageKeywords}">
    <meta name="description" content="{$pageDescription}">
    <meta http-equiv="Content-Type" content="text/html;charset={$config->system['charset']}">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/assets/resources/javascripts/head.js"></script>
    {plugins layout="root"}
    {scripts layout="root"}
    {styles layout="root"}


</head>
<body>
{'header'|widget:'root'}

{'left_menu'|widget:'root'}

<div id="content">
    <div class="inner">
        {'top_column'|widget:'root'}
        <div class="clear"></div>
        {$page}

        <!-- clear fix -->
        <div class="clear"></div>

        <div id="footer"> &copy; 2013 <span class="tip"><a href="http://www.Addmean.com"
                                                           title="ACMP">Addmean</a> </span>
        </div>

    </div>
    <!--// End inner -->
</div>
<!--// End content -->
<div class="clear"></div>
<div id="versionBar">
    <div class="copyright">{t} &copy; Tüm hakları saklıdır. {/t}  <span class="tip"><a href="http://www.addmean.com" title="ACMP">Addmean</a> </span></div>
    <!-- // copyright-->
</div>
</body>
</html>
