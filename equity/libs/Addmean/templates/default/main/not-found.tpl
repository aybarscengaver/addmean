<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title> {t}Aradığınız içerik bulunamadı.{/t}</title>
    <style type="text/css">
        body {
            font-family:arial;
        }
    </style>
</head>
<body>
<div style="height:1px;clear:both;"></div>
<div style="width:100%;text-align: center;padding:50px 0;">
    <strong style="color:red;padding-top:300px; font-size:75px;">404</strong><br>

    <p style="color:#aaa;line-height: 100px;font-size:22px;">
        {t}Aradığınız içerik bulunamadı.{/t}
        <br>
        <a href="/" style="color:blue;text-decoration:none">{t}Geri dön{/t}</a>
    </p>
</div>
</body>
</html>