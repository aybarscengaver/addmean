<div class="content">
    <h1>{t}Congratulations!{/t}</h1>
    <div class="logo"></div>

    <div>
        {t}        Addmean installed successfully.{/t}
    </div>
    <div>
        {t}If you wanna go to:{/t}
        <ul>
            <li><a href="http://docs.addmean.com">{t}Documentations{/t}</a></li>
            <li><a href="/root/">{t}Management panel{/t}</a></li>
            <li><a href="http://addmean.com">{t}Official Site{/t}</a></li>
        </ul>

        <br>
        <br>
        {t}Other languages{/t}
        <ul>
            <li><a href="/language/change/tr">{t}Turkish{/t}</a></li>
            <li><a href="/language/change/en">{t}English{/t}</a></li>
        </ul>
    </div>
    <h1>{t}Thanks for choice.{/t}</h1>
</div>