<!DOCTYPE html>
<html lang="en">
<head>
    <title>{$pageTitle}</title>
    <meta name="keywords" content="{$pageKeywords}">
    <meta name="description" content="{$pageDescription}">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/assets/resources/javascripts/head.js"></script>
    {plugins layout="default"}
    {scripts layout="default"}
    {styles layout="default"}

</head>
<body>
<div class="container">
    {$page}
</div>

<script type="text/javascript">
    var c = document.createElement('canvas'),
            ctx = c.getContext('2d'),
            cw = c.width = 200,
            ch = c.height = 200;

    for( var x = 0; x < cw; x++ ){
        for( var y = 0; y < ch; y++ ){
            ctx.fillStyle = 'hsl(0, 0%, ' + ( 100 - ( Math.random() * 15 ) ) + '%)';
            ctx.fillRect(x, y, 1, 1);
        }
    }

    document.body.style.background = 'url(' + c.toDataURL() + ')';
</script>

</body>
</html>