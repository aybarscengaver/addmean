<?php
/**
 * User: aybarscengaver
 * Date: 03.04.2013
 * Time: 23:41
 * File: index.php
 * Package: AddMean
 */
ob_start();
if(!isset($_SESSION)){
    session_start();
}

spl_autoload_register('Core::autoLoader');
include_once dirname(__FILE__).'/libraries/system/catcher.php';
include_once dirname(__FILE__).'/libraries/system/ajaxcatcher.php';
