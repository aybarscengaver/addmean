<?php
/**
 * User: delirehberi
 * Date: 06.01.2013
 * Time: 21:18
 * Filename: hooks.php
 * Description:
 * Usage:
 */
class hooks
{
    public static $hooks = array();

    public static function addAction($where, $action, $arguments = array())
    {
        $tmp = array();
        $tmp['where'] = $where;
        $tmp['action'] = $action;
        $tmp['arguments'] = $arguments;
        self::$hooks[$where][] = $tmp;
    }

    public static function run($where, $object, $method, &$parameters)
    {
        $method = $where.'_'.$method;
        if(is_callable(array($object,$method))){
            call_user_func_array(array($object,$method),$parameters);
        }
    }
}