<?php

/**
 *
CREATE TABLE IF NOT EXISTS `email_layouts` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`key` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
`subject` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
`layout` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=3 ;

 */
include_once dirname(__FILE__).'/emailer.php';
use Email\emailer;
class email{
    public function send($body,$subject,$to=null){
        $emailer = new emailer();
        $emailer->send($body,$subject,$to);
    }
}