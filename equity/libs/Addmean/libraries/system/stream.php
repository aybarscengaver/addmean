<?php
/**
 * User: aybarscengaver
 * Date: 15.04.2013
 * Time: 03:15
 * File: stream.php
 * Package: AddMean
 * Description: Hareketleri bu sınıf içerisinden yöneteceğiz. Her yapılan hareket bu sınıf vasıtasıyla model dosyası
 * üstünden veritabanına yazılacak.
 */

class stream
{
    public $model;

    public function __construct()
    {
        $this->model = new m_stream();
    }

    public function init()
    {

    }
}