<?php
/**
 * User: delirehberi
 * Date: 30/01/13
 * Time: 13:42
 */
class search {
    public $db, $table, $indexes, $rows,$config=false;

    public function __construct() {
        $this->db = DB::singleton();
    }

    public function find( $str ) {
        $size = strlen( $str );
        if ( $size<4 ) {
            return $this->result( "Arama kelimeniz 4 karakterden kısa olmamalıdır!" );
        }
        if($this->config===true){
            $this->config();
        }
        $rows = $this->rows!='*'?join( ',', $this->rows ):$this->rows;
        $indexes = join( ',', $this->indexes );
        $query = "SELECT $rows FROM {$this->table} WHERE (MATCH($indexes) AGAINST('$str' IN BOOLEAN MODE))";
        echo $query;
        return $this->result( $query );
    }

    private function result( $query ) {
        $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        $result = $this->db->query( $query ,PDO::FETCH_OBJ);
        $data = array();
        foreach($result as $va ){
            $data[] = $va->id; //@todo burası geçiştirmek için böyle yapıldı. düzeltilecek.
        }
        return $data;
    }

    private function config() {
        $regex = "~([^\:]+)\:\/\/([^\:]+)\:(.*)~";
        preg_match_all( $regex, search_engine_string, $tmp );
        $this->table = $tmp[1][0];
        $this->rows = $tmp[2][0];
        if ( $this->rows!='*' ) {
            $this->rows = explode( ',', $this->rows );
        }
        $this->indexes = explode( ',', $tmp[3][0] );
        return true;
    }
}
