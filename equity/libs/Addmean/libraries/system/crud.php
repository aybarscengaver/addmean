<?php
/**
 * User: wyrus
 * Date: 8/5/12
 * Time: 1:26 PM
 * Company: Deli
 */
class crud
{
    public $post;
    public $get;
    public $cookie;
    public $old;
    private static $regex = "~([^\__]+)\_\_([^\__]+)\_\_(.*)~";
    private static $instance;

    public static function singleton()
    {
        if (self::$instance) {
            return self::$instance;
        }
        self::$instance = new crud();
        return self::$instance;
    }

    public function __construct()
    {
        $this->get = $this->filter($_GET);
        $this->post = $this->filter($_POST);
        $this->cookie = $this->filter($_COOKIE);
    }

    public function get($data)
    {
        return $this->post[$data];
    }

    public function set($data, $value, $type = 'post')
    {
        switch ($type) {
            case 'post';
                $this->post[$data] = $value;
                break;
            case 'get';
                $this->get[$data] = $value;
                break;
            case 'cookie';
                /**
                 * @todo burada cookie değeri de değiştirilecek daha sonra.
                 */
                $this->cookie[$data] = $value;
                break;
            default:
                break;
        }
    }




    public function remove($data, $type = 'post')
    {
        switch ($type) {
            case 'post';
                unset($this->post[$data]);
                break;
            case 'get';
                unset($this->get[$data]);
                break;
            case 'cookie';
                /**
                 * @todo burada cookie değeri de değiştirilecek daha sonra.
                 */
                unset($this->cookie[$data]);
                break;
            default:
                break;
        }
    }

    public function set_old_data($data, $table, $id)
    {
        $this->old[$table][$id] = $data;
    }

    public function post($data)
    {
        if (!isset($this->post[$data])) {
            return null;
        }
        return $this->post[$data];
    }

    public function filter($data)
    {

        if (is_array($data)) {
            $data = array_map("crud::filter_map", $data);
        } else {
            $data = utils::filter($data);
        }
        $data = $this->check($data);
        return $data;
    }

    private function check($data)
    {
        $_data = array();
        foreach ($data as $k => $value) {
            $keys = explode('__', $k);

            if(is_array($value)){
                $_data[$keys[0]] = $this->check($value);
            }else{
                $_data[$keys[0]] = $value;
            }
            //check for relation
            if (isset($keys[1]) && $keys[1] == 'required' && $value == '') {
                throw new AjaxCatcher("{$keys[0]} alanını doldurmanız gerekmektedir.");
            }
            if (isset($keys[2]) && $keys[2] != 'none') {
                $modifier = $keys[2];
                    $_data[$keys[0]] = modifiers::$modifier($value);
            }
        }
        return $_data;
    }

    public function  __get($name)
    {
        return $this->$name;
    }

    /**
     * Geliştirilecek
     * @static
     *
     * @param $data
     *
     * @return mixed
     */
    public static function filter_map($data)
    {
        return utils::filter($data);
    }


}
