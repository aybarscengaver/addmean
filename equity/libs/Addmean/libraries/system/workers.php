<?php
/**
 * User: delirehberi
 * Date: 11.03.2013
 * Time: 07:47
 */
namespace workers;
use Core\Config;

class workers {
    protected $db;
    protected $_table_name = "workers";
    protected static $instance, $config;
    protected $works;

    public function __construct() {
        self::$config = Config::init();
        $this->db = \DB::singleton();
        $this->works = $this->getWorks();
        $this->init();
    }

    public static function run() {
        if (!isset(self::$instance)) {
            self::$instance = new workers();
        }
        //$cron = \Cron\CronExpression::factory("* * * * *")
    }


    private function init() {
        foreach ($this->works as $work) {
            $cron = \Cron\CronExpression::factory($work->interval);
            $previous_run_time = $cron->getPreviousRunDate()->format("d-m-Y H:i:s");
            if ($work->last_work == $previous_run_time) {
                //devam
                continue;
            }
            //çalıştır
            include self::$config->folders['cms']['addmean'] . '/workers/' . $work->worker_file;
            $tmp = explode('.', $work->worker_file);
            $className = $tmp[0];
            $class = new $className();
            $class->run();
        }
    }

    public function getWorks() {
        $this->db->table = $this->_table_name;
        $this->db->where = "status='2'";
        $works = $this->db->read();
        return $works;
    }
}