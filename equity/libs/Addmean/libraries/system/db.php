<?php
/**
 * @author Emre YILMAZ
 * @class  DB
 * @description PDO sınıfından türetilmiş olan veritabanı sınıfı singleton yapısı üzerine kuruludur. Tüm ayarlar nesne
 *              özelliği olarak ayarlanmaktadır. Ayarlar yapıldıktan sonra gerekli metod çalıştırılıp sonuç alınabilir.
 *
 * @methods
 *              reset,singleton,select,read,update,delete,drop_table,rename_table,insert,connection_string
 */
use Core\Config;
class DB extends \PDO {

    /**
     *
     * @var type
     */
    private static $instance, $is_connected = false, $connection_data, $link;

    /**
     * @var string
     */
    public $table, $where, $join, $orderBy, $groupBy,$rows,$page_no,$die,$batchData;
    public $count = false;
    public $limit = null;
    public $config;
    /**
     * @description Tüm veritabanı sınıfı değişkenlerini sıfırlıyor.
     */
    public function reset(){
        $this->where=$this->join=$this->orderBy=$this->groupBy=$this->rows=$this->page_no=$this->count=$this->limit=$this->die=false;
    }

    /**
     * @description Singleton yapısı.
     * @return DB
     */
    public static function singleton() {
        if ( !self::$instance ) {
            self::$instance = new DB();
        }
            return self::$instance;

    }

    /**
     * @description Veritabanı bağlantısını yapıp kendisini döndürür.
     * @return PDO
     */
    public function __construct() {
        $this->config = Config::init();
        $data = $this->config->database;
        return parent::__construct( "mysql:host={$data['host']};dbname={$data['database']}", $data['username'], $data['password'] );
    }

    /**
     * @description read metodunun yansımasıdır.
     * @return array
     * @throws Catcher
     */
    public function select(){
       return $this->read();
    }

    /**
     * @description Veritabanına veri girer.
     * @return string
     * @throws Catcher
     */
    public function insert($params=null,$update=false){
        try{
            if(isset($params)){
                $table_name = isset($params['table_name'])?$params['table_name']:$this->table;
                $rows = $params['rows'];
            }else{
                $table_name = $this->table;
                $rows = $this->rows;
            }
            $this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $keys = array_keys($rows);
            $keys = array_map("db::addMagicQuotes",$keys);
            $keys = join(',',$keys);
            $values = ($rows);
            $values = array_map("db::addQuotes",$values);
            $values = join(',',$values);
            $table_name = $this->addMagicQuotes($table_name);
            if(!$update){
                $query = "INSERT INTO {$table_name} ({$keys}) VALUES ($values)";
            }else{
                //onduplicate
                $update_rows = array();
                $keys = array_keys($rows);
                foreach($keys as $update_key=>$update_v){
                    if($update_key!='id'){
                        $update_rows[] ="`{$update_v}`='{$rows[$update_v]}'";
                    }
                }
                $update_query = join(',',$update_rows);
                //\onduplicate

                $keys = array_map("db::addMagicQuotes",$keys);
                $keys = join(',',$keys);
                $query = "INSERT INTO {$table_name} ({$keys}) VALUES ($values) ON DUPLICATE KEY UPDATE {$update_query}";
            }
            $this->query($query);
            $this->reset();
            return $this->lastInsertId();
        }catch(PDOException $e){
            throw new Catcher($e->getMessage(),$e->getCode());
        }
    }

    public function table_exists($table_name){
        $query = "SHOW TABLES";
        $data = $this->query($query)->fetchAll();
        foreach($data as $key=>$value){
            if($value[0]==$table_name){
                return true;
            }
        }
        return false;
    }

    public function batch_insert(){
        try{
            $ids = array();
            if(isset($params)){
                $table_name = isset($params['table_name'])?$params['table_name']:$this->table;
            }else{
                $table_name = $this->table;
            }
            $this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $table_name = $this->addMagicQuotes($table_name);

            foreach($this->batchData as $value){
                $this->rows=$value;
                $this->insert(null,true);
                $ids[]= $this->lastInsertId();
            }
            return $ids;
        }catch(PDOException $e){
            throw new Catcher($e->getMessage(),$e->getCode());
        }
    }
    /**
     * @description Dizideki her elemanı ' işareti içine alır. Array_map için kullanılıyor.
     * @param $data
     * @return string
     */
    public static function addQuotes($data){
        if(is_array($data)){
            return "'".json_encode($data)."'";
        }
        return "'{$data}'";
    }

    /**
     * @description Dizideki her elemanı ` işaretleri içine alır. Array_map için kullanılıyor.
     * @param $key
     * @return string
     */
    public static function addMagicQuotes($key){
        $tmp = explode(' ',$key);
        if(count($tmp)>1){
            //tablodur ve alias atanmıştır.
            return "`{$tmp[0]}` {$tmp[1]}";
        }
        return "`{$key}`";
    }
    /**
     * @description Veritabanına SELECT sorgusu çeker.
     * @return array
     * @throws Catcher
     */
    public function read($params =array()){
        try{
            $this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $orderBy='';
            $groupBy='';
            $limit = '';
            $where = '';
            $start = 0;

            if(!$this->rows){
                $this->rows='*';
            }
            if($this->count){
                $this->rows='COUNT(*) as count';
            }
            if($this->where){
                $where = "WHERE {$this->where}";
            }else{
            }
            if($this->orderBy){
                $orderBy = "ORDER BY {$this->orderBy}";
            }
            if($this->groupBy){
                $groupBy = "GROUP BY {$this->groupBy}";
            }
            if($this->page_no){
                $start = $this->page_no*$this->limit;
            }
            if($this->limit){
                $limit = "LIMIT {$start},{$this->limit}";
            }

            $table = $this->addMagicQuotes($this->table);

            $query = "SELECT {$this->rows} FROM {$table} {$this->join} {$where} {$orderBy} {$groupBy} {$limit}";
            if($this->die===true){
                die($query);
            }
            $query = $this->query($query,PDO::FETCH_OBJ);

            $result = array();
            foreach($query as $r){
                $result[]=$r;
            }
            if($this->limit==1 || $this->count){
                $this->reset();
                return isset($result[0])?$result[0]:null;
            }
            $this->reset();

            return $result;
        } catch ( PDOException $e ) {
            throw new Catcher( $e->getMessage() );
        }
    }

    public function show_columns($table_name){
        $table_name = $this->addMagicQuotes($table_name);
        $query = $this->query("SHOW COLUMNS FROM {$table_name}",PDO::FETCH_OBJ);
        return $query->fetchAll();
    }

    /**
     * @description Veritabanındaki herhangi bir veriyi günceller.
     * @throws Catcher
     */
    public function update($archive=false){
        try{
            $this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $keys = array_keys($this->rows);
            $values = ($this->rows);
            $values = array_map("db::addQuotes",$values);
            $table_name = $this->addMagicQuotes($this->table);

            $query = "UPDATE {$table_name} SET ";
            $rows = array();
            foreach($keys as $key=>$v){
                $rows[] ="`{$v}`={$values[$v]}";
            }
            $query.=join(",",$rows);

            $query.=" WHERE {$this->where}";
            if(!$archive){
                self::store_old_data();
            }
            $this->query($query);
            return 1;
        }catch (PDOException $e){
            throw new Catcher($e->getMessage());
        }
    }

    /**
     * @description Crud içerisine tablo adına göre update'den önceki verileri atıyorum burada.
     *              $data['tablo_adi']['id'] şeklinde
     */
    private function store_old_data(){
        /**
         * Save old data in crud
         */
        $crud = crud::singleton();
        $this->rows = $this->count=false;
        $this->limit = 1;
        $data = self::read();
        if($data){
            $crud->set_old_data($data,$this->table,$data->id);
        }
    }

    /**
     * @description Veritabanında herhangi bir tablodan herhangi bir veriyi silmeye yarar.
     */
    public function delete(){
        $table_name = $this->addMagicQuotes($this->table);
        $query = "DELETE FROM {$table_name} WHERE ".$this->where;
        $this->query($query);
        $this->reset();
    }



    /**
     * @description Veritabanından bir tabloyu silmeyi sağlar.
     */
    public function drop_table(){
        $table = $this->addMagicQuotes($this->table);
        $query = "DROP TABLE {$table}";
    }

    /**
     * @desc Tablodan bir sütunu sil.
     * @param $column_name
     */
    public function drop_column($column_name){
        $sql = "ALTER TABLE `{$this->table}` DROP COLUMN `{$column_name}`";
        $this->query($sql);
    }

    /**
     * @desc Bir tabloya sütun ekler
     * @param $column_name [int,varchar,date]
     * @param string $type
     * @param int $length
     */
    public function add_column($column_name,$type="int",$length=10){
        $length = !empty($length)?"({$length})":'';
        $sql =<<<SQL
ALTER TABLE `{$this->table}` ADD COLUMN `{$column_name}` {$type}{$length} NULL AFTER `id`
SQL;
        $this->query($sql);
    }

    /**
     * @description Veritabanından bir tablonun adını değiştirmeye yarar.
     * @param $new_name
     */
    public function rename_table($new_name){
        $table = $this->addMagicQuotes($this->table);
        $new_name = $this->addMagicQuotes($new_name);
        $query = "RENAME TABLE {$table} TO {$new_name}";
        $this->query($query);
    }

    /**
     * @return int
     * @TODO ÖNCE VERİYİ EKLEYİP SONRA SEO KOLONUNU GÜNCELLE
     */
    public function get_next_id(){
        $table = $this->addMagicQuotes($this->table);
        $sql = "SELECT MAX(id) as max_id FROM {$table}";
        $result = $this->query($sql,PDO::FETCH_OBJ)->fetchObject();
        if(!$result){
            return 1;
        }
        $last_id  =$result->max_id;
        $next_id = $last_id+1;
        return $next_id;
    }


    public function check_column($column_name,$table){
        $table = $this->addMagicQuotes($table);
        $sql = "SHOW COLUMNS FROM {$table}";
        $result = $this->query($sql,PDO::FETCH_OBJ)->fetchAll();
        foreach($result as $field){
            if($field->Field==$column_name){
                return true;
                break;
            }
        }
        return false;

    }
}