<?php

/**
 * @name Catcher
 * @desc    Hata yakalama sınıfı.
 */
class Catcher extends Exception
{
    /**
     * @name show
     * @desc Yazılım geliştirelere gösterilecek hata mesajı formatı.
     * @return string
     */
    public function show()
    {
        $content = "
<html>
<head>
<title>Hay aksi!</title>
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow&subset=latin-ext' rel='stylesheet' type='text/css'>

<meta http-equiv='content-type' content='text/html;charset=" . charset . "'/>
    <style>
    body{font-family: 'Archivo Narrow', sans-serif;

 font-size:16px;
 background-color:#fff;
 color:#111;

}
* {
  margin: 0;
  padding: 0;
  min-height: 100%;
  width: 100%;
}

canvas{
  height: 100%;
  width: 100%;
}
</style>
</head>
<body>
<canvas id='canvas'></canvas>
";
        $content .= "
            <div style='position: absolute;
top: 20px;
left: 30px;
width: 600px;
margin: 0 auto;
'>
<h1>Burada birşeyler ters gidiyor!</h1> <br/>

    <b>Hata Kodu</b>: " . parent::getCode() . "
        <br/>

    <br/><b>Hata Mesajı</b>: " . parent::getMessage() . "
    <br/>
    <br/>
    <b>Dosya</b>: " . parent::getFile() . "
    <br/>    <br/>

    <b>Satır</b>: " . parent::getLine() . "
    <br/>

    </div>
";
        $content.=<<<SCRIPT
<script type="text/javascript">
var livePatern = {
  canvas: null,
  context: null,
  cols: 0,
  rows: 0,
  colors: [252, 251, 249, 248, 241, 240],
  triangleColors: [],
  destColors: [],

  init: function(){
    this.canvas = document.getElementById('canvas');
    this.context = this.canvas.getContext('2d');
    this.cols = Math.floor(document.body.clientWidth / 24);
    this.rows = Math.floor(document.body.clientHeight / 24) + 1;

    this.canvas.width = document.body.clientWidth;
    this.canvas.height = document.body.clientHeight;

    this.drawBackground();
    this.animate();
  },

  drawTriangle: function(x, y, color, inverted){
    inverted = inverted == undefined ? false : inverted;

    this.context.beginPath();
    this.context.moveTo(x, y);
    this.context.lineTo(inverted ? x - 22 : x + 22, y + 11);
    this.context.lineTo(x, y + 22);
    this.context.fillStyle = "rgb("+color+","+color+","+color+")";
    this.context.fill();
    this.context.closePath();
  },

  getColor: function(){
    return this.colors[(Math.floor(Math.random() * 6))];
  },

  drawBackground: function(){
    var eq = null;
    var x = this.cols;
    var destY = 0;
    var color, y;

    while(x--){
      eq = x % 2;
      y = this.rows;

      while(y--){
        destY = Math.round((y-0.5) * 24);

        this.drawTriangle(x * 24 + 2, eq == 1 ? destY : y * 24, this.getColor());
        this.drawTriangle(x * 24, eq == 1 ? destY  : y * 24, this.getColor(), true);
      }
    }
  },

  animate: function(){
    var me = this;

    var x = Math.floor(Math.random() * this.cols);
    var y = Math.floor(Math.random() * this.rows);
    var eq = x % 2;

    if (eq == 1) {
      me.drawTriangle(x * 24, Math.round((y-0.5) * 24) , this.getColor(), true);
    } else {
      me.drawTriangle(x * 24 + 2, y * 24, this.getColor());
    }

    setTimeout(function(){
      me.animate.call(me);
    }, 10);
  },
};

!function(){livePatern.init();}()
</script>
SCRIPT;

        $content .= "</body>
</html>
";
        if (utils::is_ajax()) {
            $data = array();
            $data['type'] = "failed";
            $data['msg'] = parent::getMessage();
            $data['code'] = parent::getCode();
            $data['line'] = parent::getLine();
            $data['file'] = parent::getFile();
            $data['title'] = 'Hata!';
            $data = json_encode($data);
            return $data;
        }
        return $content;
    }

    /**
     * @name lite
     * @desc Son kullanıcıya gösterilecek hata mesajı formatı
     * @return string
     */
    public function lite()
    {
        if (utils::is_ajax()) {
            $data = array();
            $data['type'] = 'failed';
            $data['msg'] = "Beklenmedik bir hata oluştu.";
            $data['title'] = 'Hata';
            return json_encode($data);
        }
        return 'Beklenmedik bir hata oluştu. Hata kayıt altına alındı. En kısa zamanda çözümlenecektir.';
    }
}