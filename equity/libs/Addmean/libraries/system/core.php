<?php
/**
 * @name Core
 * @desc Sistemin çekirdek yapısı burada oluşturulmaktadır.
 */
use Core\Config;
class Core
{
    /**
     * @var $controller string Çalıştırılacak controller bu değişkende saklanır.
     * @var $action     string Controllerın yapacağı işlem bu değişkende saklanır.
     * @var $params     array Controllerda bulunan action a gönderilecek veriler bu dizide saklanır.
     */
    public static $controller, $action, $params, $config;
    /**
     * @var $instance object Çekirdek nesnenin singleton için tutulduğu değişken.
     */
    public static $instance;
    /**
     * @var theme Smarty nesnesi burada tutulacak.
     */
    public static $theme;
    /**
     * @var application string Hangi programın çalışacağı ayarlanacak. (root ? default)
     */
    public static $application = 'default';



    public function __construct($application)
    {
        self::$config = Config::init();

        // Print a test message
        self::$application = $application;
        //Input Output controller
        include_once self::$config->folders['cms']['libraries'].'system/io.php';
        //Controller
        include_once self::$config->folders['cms']['libraries'].'system/controller.php';
        //Smarty
        include_once self::$config->folders['cms']['libraries'].'system/theme.php';

        include_once self::$config->folders['smarty']['main'];

        self::$theme = new theme(self::$application);
        //Database
        include_once self::$config->folders['cms']['libraries'] . 'system/db.php';
        self::$theme->setup();
        /**
         * Autorization
         */
        $tmp = IO::getUri();
        include_once self::$config->folders['cms']['libraries'] . 'system/authorize.php';
        $authorize = new authorize($application);
        $authorize->controller = $tmp['controller'];
        $authorize->action = $tmp['action'];
        if ($application == 'root') {

            $authorize->exceptions = array('user' => 'login');
            $authorize->login_url = '/root/user/login';
            $authorize->level = 2;

        } elseif ($application == "default") {
            //normal kullanıcı giriş kontrolü burada da şifresiz girilenmeyecek alanlar include edilcek @todo
            $authorize->reverse_exceptions = array(
                'members' => array(
                    'profile',
                    'messages'
                ),
                'publications' => array(
                    'submit',
                    'submit_step_2'
                )
            );
            $authorize->login_url = '/members/login';
            $authorize->level = 1;
        }
        $authorize->checkIn();
        self::getController();
    }

    /**
     * Burada var bi bokluk ama çözemedim gitti.
     */



    public static function singleton($application)
    {
        if (!self::$instance) {
            return self::$instance = new Core($application);
        } else {
            return self::$instance;
        }
    }


    /**
     * @static
     * @throws Catcher
     */
    public static function getController($uri = null,$tmp=null)
    {
        $tmp = isset($tmp)?$tmp:IO::getUri($uri);
        if ($tmp) {
            $controller = self::$config->folders['cms']['addmean'] . 'modules/' . self::$application . '/' . $tmp['controller'] . '/c/' . $tmp['controller'] . '.php';
            if (!is_file($controller)) {
                if ($uri == null && isset($_SERVER['REDIRECT_QUERY_STRING']) && !empty($_SERVER['REDIRECT_QUERY_STRING'])) {
                    //Bazı sunucularda böyle (deb)
                    return self::getController($_SERVER['REDIRECT_QUERY_STRING']);
                } elseif ($uri == null && isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])) {
                    //Bazı sunucularda böyle (red)
                    return self::getController($_SERVER['QUERY_STRING']);
                } else {
                    throw new Catcher("{$tmp['controller']} adında bir modül mevcut değil.", -1);
                }
            }
            include(self::$config->folders['cms']['addmean'] . 'modules/' . self::$application . '/' . $tmp['controller'] . '/c/' . $tmp['controller'] . '.php');
            $class = new $tmp['controller']();
            if($tmp['action']=='init'){
                $tmp['action']='index';
            }
            $action = $tmp['action'] . 'Action';
            if (is_callable(array($class, $action))) {
                call_user_func_array(array($class, $action), $tmp['params']);
            } else {
                throw new Catcher("İstediğiniz sayfa mevcut değil!");
            }
        } else {
            $tmp = array(
                'controller'=>self::$config->system['default_module'],
                'action'=>'index',
                'params'=>array()
            );
            return self::getController(null,$tmp);

        }
    }

    public static function getModel()
    {
        $tmp = IO::getUri();
        if ($tmp) {
            include(self::$config->folders['cms']['addmean'] . 'modules/' . self::$application . '/' . $tmp['controller'] . '/c/' . $tmp['controller'] . '.php');
            call_user_func_array(array($tmp['controller'] . "Model", $tmp['action']), $tmp['params']);
        } else {
            include(self::$config->folders['cms']['addmean'] . 'modules/' . self::$application . '/' . self::$config->system['default_module'] . '/c/' . self::$config->system['default_module'] . '.php');
            call_user_func(array(self::$config->system['default_module'] . 'Model', 'init'));
        }
    }

    public static function autoLoader($name)
    {

        if(!isset(self::$config)){
            self::$config=Config::init();
        }
        /**
         * Kütüphaneler
         */
        $library = strtolower($name);

        preg_match_all('~^m_(.*)$~', $name, $model);

        preg_match("~lib\_(.*)$~", $name, $thirdparty);

        if (is_file(self::$config->folders['cms']['libraries'] . 'default/' . $library . '.php')) {
            //Kullanıcı sınıfları öncelikli.
            include self::$config->folders['cms']['libraries'] . 'default/' . $library . '.php';
        } elseif (is_file(self::$config->folders['cms']['libraries'] . 'root/' . $library . '.php')) {
            //Yönetim paneli sınıfları
            include self::$config->folders['cms']['libraries'] . 'root/' . $library . '.php';
        } elseif (is_file(self::$config->folders['cms']['libraries'] . 'system/' . $library . '.php')) {
            //Sistem sınıfları
            include self::$config->folders['cms']['libraries'] . 'system/' . $library . '.php';
        } else if (isset($model[1]) && isset($model[1][0])) {
            if (is_file(self::$config->folders['cms']['addmean'] . 'models/' . $model[1][0] . '.php')) {
                include(self::$config->folders['cms']['addmean'] . 'models/' . $model[1][0] . '.php');
            } else if (is_file(self::$config->folders['cms']['addmean'] . 'modules/' . self::$application . '/' . $model[1][0] . '/m/' . $model[1][0] . '.php')) {
                //modül modeli
                include(self::$config->folders['cms']['addmean'] . 'modules/' . self::$application . '/' . $model[1][0] . '/m/' . $model[1][0] . '.php');
            } else if (is_file(self::$config->folders['cms']['addmean'] . 'modules/root/' . $model[1][0] . '/m/' . $model[1][0] . '.php')) {
                include(self::$config->folders['cms']['addmean'] . 'modules/root/' . $model[1][0] . '/m/' . $model[1][0] . '.php');
            }
        } else if (isset($thirdparty[1])) {
            if (is_dir(self::$config->folders['cms']['libraries']. 'thirdparty/' . $thirdparty[1]) && $thirdparty[1]) {

                $loader_file = self::$config->folders['cms']['libraries'] . 'thirdparty/' . $thirdparty[1] . "/loader.php";
                if (is_file($loader_file)) {
                    require_once($loader_file);
                }
            }
        } else if (strpos($name, '\\')) {
            //namespace ,maybe :)
            $namespace_file = self::$config->folders['cms']['libraries'] . '/thirdparty/' . str_replace('\\', '/', $name) . '.php';
            if (is_file($namespace_file)) {
                require_once($namespace_file);
            }
        }
    }

    /**
     * @static
     *
     * @param $application
     */
    public static function setApplication($application)
    {
        if (is_dir(self::$config->folders['cms']['addmean'] . 'modules/' . $application)) {
            self::$application = $application;
        } else {
            throw new Catcher('Sistem belirtilen programda çalıştırılamadı. Lütfen program tanımını doğru yaptığınızdan
            emin olun!', '666');
        }
    }

    /**
     *
     * Ajax işlemlerinde, ajax sorgularını yapacak kısmın başlangıç noktası.
     *
     * @static
     *
     * @param $data
     *
     * @throws AjaxCatcher
     */
    public static function ajax($get)
    {
        include_once(self::$config->folders['cms']['libraries'] . 'system/ajax.php');
        $x = "";
        foreach($get as $key=>$value) {
            if($key!='c'){
                $x=$key; //Burada bir bug oluşabilir @research @todo
                break;
            }
        }
        $app = explode('/', $x);
        $application = $app[0];
        self::setApplication($application);
        //Input Output controller
        include_once self::$config->folders['cms']['libraries'] . 'system/io.php';
        //Database
        include_once self::$config->folders['cms']['libraries'] . 'system/db.php';

        $class = $app[1];
        $method = $app[2] . 'Ajax';
        unset($app[0], $app[1], $app[2]);
        $parameters = $app ? $app : array();
        $classFile = self::$config->folders['cms']['addmean'] . 'modules/' . $application . '/' . $class . '/a/' . $class . '.php';
        if (!is_file($classFile)) {
            throw new AjaxCatcher("Geçersiz istek.");
        }
        include_once($classFile);
        if (!class_exists($class)) {
            throw new AjaxCatcher("Geçersiz sınıf çağırdınız.");
        }
        $object = new $class();
        if (!is_callable(array($object, $method), false, $callableName)) {
            throw new AjaxCatcher("Geçersiz bir metod çağırdınız.");
        }
        hooks::run('before', $object, $method, $parameters);
        call_user_func_array(array($object, $method), $parameters);
        hooks::run('after', $object, $method, $parameters);
    }
}