<?php

use Core\Config;
class widget {
    public $view,$DATA,$theme,$db;
    static $instance,$config;

    public function __construct(){
        $this->theme = new theme('root');
        $this->db = db::singleton();
        self::$config = Config::init();
    }

    public static function singleton(){
        if(empty(self::$instance)){
            self::$instance = new widget();
        }
        return self::$instance;
    }

    public static function load($widget,$layout='default',$param=null){
        if(!isset(self::$config)){
            self::$config = Config::init();
        }
        $dir = self::$config->folders['widgets'] . $layout.'/'.$widget;
        $class = $dir.'/'.$widget.'.class.php';
        $view = $dir.'/'.$widget.'.tpl';
        include_once($class);
        $widget = new $widget();
        $widget->view = $view;
        $widget->beforeInit($param);
        $return = $widget->init($param);
        $widget->afterInit($param);
        return $return;
    }

    /**
     * @description Modüle ilişik olan modüllere ait widgetları getirir.
     * @param $data
     * @param $application
     * @return array
     */
    public function loadRelational($data,$application){
        switch($application){
            case "default";
                $application=2;
                break;

            case "root";
            default:
                $application=1;
                break;
        }
        $this->db->table='widgets';
        $this->db->where="application='{$application}' AND status='2' AND main_module='{$data['info']->module_table_name}'";
        $widgets = $this->db->read();
        return $widgets;
    }

    public function init(&$param=null){
        $this->theme->assign('params',$param);
        $data = $this->theme->fetch($this->view);
        return $data;
    }

    public function assign($key,$value){
        $this->theme->assign($key,$value);
    }

    public function beforeInit(&$params){
        $this->theme->assign('params',$params);

    }
    public function afterInit(&$params){
        $this->theme->assign('params',$params);

    }
}
