<?php
/**
 * User: delirehberi
 * Date: 11.01.2013
 * Time: 11:48
 * Filename: url.php
 * Description:
 * Usage:
 */
use Core\Config;
class url
{
    private $_modules_table_name = "modules";
    protected $db;
    static $config;

    public function __construct()
    {
        self::$config = Config::init();
        $this->db = DB::singleton();
    }

    public function renderHtaccess()
    {
        //get links from modules table
        $modules = $this->getModules();
        $rewrite_rules = array();
        foreach ((array)$modules as $key => $value) {
            if (isset($value->seo) && !empty($value->seo)) {

                $regex = preg_replace("(\{[^\}]+\})", "(.*)", $value->seo);
                $module = $value->table_name;
                $actions = $this->getModuleActions($module);
                $d = 'RewriteRule ^(' . $regex . ')\$ /index.php?c=' . $module . '/index&seo=\$1 [L]';
                $rewrite_rules[] = $d;

            }
        }
        $this->writeHtaccess($rewrite_rules);
    }

    /**
     * @todo Burada çok güzel bir bug var, şöyleki automated satırları dolu iken tanımıyor
     * @param $rules
     */
    public function writeHtaccess($rules)
    {
        $htaccess = file_get_contents(self::$config->files['htaccess']);
        $content = "############AUTOMATED############\n";

        $content .= join("\n", $rules) . "\n";
        $content .= "############AUTOMATED############";
        $content = preg_replace("~\#\#\#\#\#\#\#\#\#\#\#\#AUTOMATED\#\#\#\#\#\#\#\#\#\#\#\#([^\#]+)\#\#\#\#\#\#\#\#\#\#\#\#AUTOMATED\#\#\#\#\#\#\#\#\#\#\#\#~", $content, $htaccess);
        $file = fopen(self::$config->files['htaccess'], "w+");
        fwrite($file, $content);
        fclose($file);
    }

    private function getModules()
    {
        $this->db->table = $this->_modules_table_name;
        $this->db->where = "seo IS NOT NULL";
        $modules = $this->db->read();
        return $modules;
    }

    private function getModuleActions($module)
    {
        $black_list = array("__construct", "editAction", "listAction", "deleteAction", "createAction", "viewContent");
        $file = self::$config->folders['cms']['addmean'] . "modules/default/" . $module . "/c/" . $module . ".php";
        require $file;
        $methods = get_class_methods($module);
        foreach ($methods as $key => $value) {
            if (in_array($value, $black_list)) {
                unset($methods[$key]);
            }
        }
        return $methods;
    }
}