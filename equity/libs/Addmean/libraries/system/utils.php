<?php
/**
 * User: wyrus
 * Date: 8/5/12
 * Time: 2:22 PM
 * Company: Deli
 */
class utils
{

    public static function is_ajax(){
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])&&strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest';
    }

    public static function filter($data)
    {
        $dirty = array("'",'"');
        $clean = array("`","&quot;");
        return str_replace($dirty, $clean, $data);
    }

    /**
     * @description Dosya ayar bilgisini veritabanınında çek getir yapıyor. ID ye yada verilen başka değere göre
     * @param $data
     * @param $where
     */
    public static function file_alias($data,$where='key'){
        $db = DB::singleton();
        $db->table="file_settings";
        $db->where="`{$where}`='{$data}'";
        $db->limit=1;
        $data = $db->read();
        $db->reset();
        if(!$data){
            return null;
        }
        if($data->image_height==0){
            $data->image_height=null;
        }

        if($data->image_width==0){
            $data->image_width=null;
        }

        if(!empty($data->sub_settings)){
            $sub_settings = json_decode($data->sub_settings);
//            $sub_settings = explode(',',$data->sub_settings);
            $data->sub_settings = array();
            foreach($sub_settings as $key=>$value){
                $db->table="file_settings";
                $db->where="`id`='{$value}'";
                $db->limit=1;
                $ds = $db->read();
                if($ds){
                    $data->sub_settings[$key] = utils::file_alias($ds->key);
                }
            }
        }
        return $data;
    }

    /**
     * @param $str
     * @return string
     * @link http://php.net/manual/en/function.preg-replace.php#96586
     */
    public static function seo($str)
    {
        return strtolower(
            preg_replace(
                array('/[^a-zA-Z0-9 - _A-Яа-я-]/', '/[ -]+/', '/^-|-$/'),
                array('', '-', ''),
                    self::remove_accent($str)
                )
        );
    }

    /**
     * @description Modül ayarlarındaki formatta seo kolonunu dolduracak veri bu kısımda otomatik olarak oluşturulur
     * @param $data post üzerinden gelen dizi halindeki temiz veri
     * @param $module modülün tüm bilgilerini içeren nesne halindeki temiz veri
     * @param int $offset eğer oluşturulan seo veritabanında mevcutsa sonuna offset eklenir.
     * @param int $id eğer güncelleme yapılacaksa veritabanına yapılan sorgu sırasında kendisi dahil edilmez sorguya.
     * @return string seo kolonunu dolduracak veri.
     */
    public static function made_seo($data, $module, $offset = 0,$id=0)
    {
        $pattern = $module->seo;
        //data içerisinde oluşturulan modülün sütun değerlerisi var.
        //pattern içinde tam regex şeysi var.
        //test/{baslik}.html
        $regex = "~\{([^\}]+)}~";
        preg_match_all($regex, $pattern, $tmp);
        $seo = "";
        $db = DB::singleton();

        if (isset($tmp[1])) {
            foreach ($tmp[1] as $key => $value) {
                if (isset($tmp[1])) {
                    foreach ($tmp[1] as $key => $value) {
                        if($value=='id'){
                            //id alanı cruddan gelmez. Veritabanından çekilecek.
                            $db->table=$module->table_name;
                            $data[$value] = $db->get_next_id();
                        }
                        if(isset($data[$value])){
                            $x = utils::seo($data[$value]);
                            $seo = str_replace($tmp[0][$key], $x, $pattern);
                        } else{
                            throw new AjaxCatcher("{$value} alanı modül özelliğinde bulunmadığı için seo oluşturulamadı!");
                        }
                    }
                }
            }
        }
        if ($offset > 0) {
            if (strpos($seo, '.')) {
                $seo = explode('.',$seo);
                $seo = $seo[0].'_'.$offset.'.'.$seo[1];
            } else {
                $seo = $seo.$offset;
            }
        }
        if($id>0){
            $db->where = "seo='{$seo}' AND id NOT IN ({$id})";
        }else{
            $db->where = "seo='{$seo}'";
        }
        $db->table = $module->table_name;


        $db->count = true;
        $check = $db->read();
        if($check->count>0){
            return utils::made_seo($data,$module,$offset+1,$id);
        }
        $db->reset();
        return $seo;
    }
    public static function check_all_seo($module){
        $crud = crud::singleton();
        $old_seo_column = $module->seo;
        $new_seo_column = $crud->post('seo');
        if($old_seo_column!=$new_seo_column){
            /**
                * Bu modülün tablosundan tüm verilerin seo sütunlarındaki değerleri tekrar hesaplayacaz
                */
            $module->seo = $new_seo_column;
            $db = DB::singleton();
            $db->reset();
            $db->table = $module->table_name;
            $db->where = false;
            $all_data = $db->read();
            foreach($all_data as $key=>$data){

                //Convert all object data to array data;
                $_data = array();
                foreach($data as $k=>$v){
                    $_data[$k] = $v;
                }
                //Reset seo column
                $seo = utils::made_seo($_data,$module,0,$_data['id']);
                $db->where = "id='{$_data['id']}'";
                $db->rows = array('seo'=>$seo);
                $db->update();
            }
        }

        return;
    }

    /**
     * @param $str
     * @return mixed
     * @link http://php.net/manual/en/function.preg-replace.php#96586
     */
    public static function remove_accent($str)
    {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        return str_replace($a, $b, $str);
    }

    public static function write_hierarchic($params,$tag='option',$offset=1,$selected=0,$key_row=null,$value_row=null){
        $lines ='';
        for($i=1;$i<=$offset;$i++){
            $lines.='&nbsp;&nbsp;';
        }
        if($key_row==null){
            $key_row = $params['key_row'];
        }
        if($value_row==null){
            $value_row = $params['value_row'];
        }
        if(!isset($params['value'])){
            $datas = $params;
        }else{
            $datas = $params['value'];
        }
        $lines=$lines.'->';
        foreach((array)$datas as $key=>$value){

            switch($tag){
                case 'option';
                    echo "<option value='{$value->$key_row}'";
                    if(isset($value->$key_row) && $selected===$value->$key_row){
                        echo " selected='selected' ";
                    }
                    echo ">{$lines} {$value->$value_row}</option>";
                break;
                case 'text';
                    echo "{$lines}{$value->$value_row}";
                    break;
                default:
                    break;
            }

            if(isset($value->childrens)){
                self::write_hierarchic($value->childrens,$tag,$offset+1,$selected,$key_row,$value_row);
            }
        }
    }
    public static function getParentWay($table_name,$parent_id=0){
        $way = array();
        if(!empty($parent_id)){
            $db = DB::singleton();
            $db->where="id='{$parent_id}'";
            $db->table=$table_name;
            $db->limit = 1;
            $data = $db->read();
            $way[] = $data;
            $way = array_merge($way,self::getParentWay($table_name,$data->parent_id));
        }
        return $way;
    }

    public static function password($str){
        return md5($str);
    }

    public static function redirect($url,$code=301){
        header("Location: {$url}",null,$code);
        exit;
    }


    /**
     * @description Clear space and unwanted characters in url string.
     * @param $url
     * @return bool|string
     */
    public static function urlCheck($url){
        $_parts = parse_url($url);
        $url = "";
        if(isset($_parts['scheme'])){
            $url.=$_parts['scheme'].'://';
        }else{
            $url.="http://";
        }

        if(isset($_parts['user'])){
            $url.=$_parts['user'].':';
            if(isset($_parts['pass'])){
                $url.=$_parts['pass'].'@';
            }else{
                $url.='@';
            }
        }

        if(isset($_parts['host'])){
            $url.=$_parts['host'];
        }else{
            return false;
        }

        if(isset($_parts['port'])){
            $url.=':'.$_parts['port'];
        }

        if(isset($_parts['path'])){
            $__path = str_replace(
                '--slash--',
                '/',rawurlencode(
                    str_replace(
                        '/',
                        '--slash--',$_parts['path']
                    )
                )
            );
            $url.=$__path;
        }else{
            $url.='/';
        }

        if(isset($_parts['query'])){
            $url.='?';
            $__query = $_parts['query'];
            $_q_vars = explode('&',$__query);
            foreach($_q_vars as $key=> $q_v){
                $tmp_qv = explode('=',$q_v);
                $tmp_qv = $tmp_qv[0].'='.urlencode($tmp_qv[1]);
                $_q_vars[$key]=$tmp_qv;
            }
            $query_variables = join('&',$_q_vars);
            $url.=$query_variables;
        }

        if(isset($_parts['fragment'])){
            $url.="#".$_parts['fragment'];
        }
        return $url;
    }
}
