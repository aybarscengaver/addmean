<?php
/**
 * User: emre
 * Date: 26.06.2012
 * Time: 22:45
 * Company: Deli
 */
use Core\Languages;
class ajax
{
    public $db;
    public $crud;
    public $model;
    private $id,$language;
    protected $session;
    public static $config;
    public static $origin_table_name;


    public function __construct()
    {
        self::$config = core::$config;
        $this->db = DB::singleton(); //todo buna gerek bile yok aslında burada.
        $this->crud = crud::singleton();
        $called_class = 'm_' . get_called_class();
        $this->model = new $called_class();
        self::$origin_table_name = $this->model->get_module_name();
        $this->archive = new archive($this->model->get_module_name());
        $this->session = m_session::singleton();
        $this->language = new Languages($this->model);
    }

    /**
     * @description Modüle otomatik ekleme yaparken, crud->set kullanılırsa, crud işleminde fieldcheck çalışır ve modüle özellik olarak
     * eklenmemiş olan tüm fazlalık alanları temizler. Bunun önüne geçmek için, crud->set işleminden sonra, bu metod çağrılıp, fake bir
     * modül özelliği var edilebilir.
     * @param $name
     * @param string $type
     */
    public function addField($name,$type='number'){
        $field = new \stdClass();
        $field->type=m_statics::field_types($type);
        $field->column_name=$name;
        $this->model->extra_fields[]=$field;
    }

    /**
     * Ajax sorgusu sonrası dönüş formatına uygun veri basar ekrana.
     *
     * @param string $msg
     * @param bool $status
     * @param string $title
     * @param array $extra
     */
    public function result($msg = '', $status = false, $title = '', $extra = array())
    {
        $data = array();
        $data['msg'] = $msg;
        $data['type'] = $status ? 'success' : 'failed';
        $data['title'] = $title ? $title : ($status ? 'Başarılı' : 'Hata');
        $data['extra'] = $extra;
        $data = json_encode($data);
        echo $data;
    }


    /**
     * @description Veri oluşturma işlemleri
     */
    public function createAjax($msg = 'Başarıyla eklendi.')
    {
        try {
            $data = $this->crud->post;
            $id = $this->model->insert($data);
            $this->id = $id;
            $data['id'] = $id;
            $this->archive->insert($data);
            $this->result($msg, true, 'Başarılı');
        } catch (AjaxCatcher $e) {
            $this->result($e->getMessage(), false, 'Hata');
        }
    }

    public function data_importAjax($msg = "Başarıyla eklendi")
    {

        try {
            $data = IO::post('query__required__none', true);

            $data = json_decode($data, true);
            $ids = $this->model->batch_insert($data);
            $this->archive->batch_insert($data, $ids);
            $this->result($msg, true, "Başarılı");
        } catch (AjaxCatcher $e) {
            $this->result($e->getMessage(), false, 'Hata');
        }
    }

    /**
     * @description Veri düzenleme işlemleri
     * @param $id
     * @throws AjaxCatcher
     */
    public function editAjax($id)
    {
        if (!$id) {
            throw new AjaxCatcher("Content ID not found!");
        }
        $data = $this->crud->post;
        $this->model->edit($data, $id);
        $this->archive->update($id, $data);
        $this->result("İçerik başarıyla güncellendi.", true);


    }

    /**
     * @description Veri silme işlemleri
     *
     */
    public function deleteAjax($l=null)
    {

        $data = IO::post('data');
        $data = explode('=', $data);
        $id = $data[1];
        $this->model->delete($id);
        $this->result("İçerik başarıyla silindi.", true);
    }

    /**
     * @description Her veri oluşturma işleminden önce çalıştırılacak işlemler
     */
    public function before_createAjax()
    {
        if ($this->model->is_module() && $this->model->has_seo()) {
            $module = $this->model->get_module_info();
            $seo = utils::made_seo($this->crud->post, $module);
            $this->crud->set('seo', $seo);
        }

    }

    public function after_createAjax()
    {
        $this->relationInsert($this->id);
    }

    public function relationInsert($id)
    {
        $module = $this->model->get_module_info(self::$origin_table_name);
        if($module){
            $this->db->table = 'module_relations';
            $this->db->where = "master_module='{$module->table_name}'";
            $relations = $this->db->read();
            $inputs = array();
            foreach ($relations as $r) {
                if ($r->type == 1) {
                    $data = $this->crud->post($r->slave_module.'__optional__none');
                    $this->crud->remove($r->slave_module);
                    //mtm
                    foreach($data as $d){
                        $this->many_to_many_relation($d,$id,$r);
                    }
                } elseif ($r->type == 2) {
                    $data = $this->crud->post($r->slave_module);
                    $this->crud->remove($r->slave_module);
                    $t=array();
                    foreach($data as $key=>$val){
                        foreach($val as $k=>$v){
                            $t[$key][$k]=current($v);
                        }
                    }
                    $inputs = $this->crud->filter($t);
                    foreach($inputs as $key=>$value){
                            $this->one_to_many_relation($value,$r,$id);
                    }
                }
            }
        }

    }


    /**
     * @description Her veri düzenleme işleminden önce çalıştırılacak işlemler
     * @todo Burada eski veri da kontrol edilerek, herhangi bir değişilik yoksa işlem yapılmaması gerekiyor.
     * @param $id
     */
    public function before_editAjax($id)
    {
        if ($this->model->has_seo()) {
            $module = $this->model->get_module_info();
            $seo = utils::made_seo($this->crud->post, $module, 0, $id);
            $this->crud->set('seo', $seo);
        }
    }

    public function after_editAjax($id)
    {
        $this->relationInsert($id);
    }

    private function one_to_many_relation($data, $relation_info, $master_table_insert_id)
    {
        $insert = $update = false;
        $array_keys = array_keys($data);
        if(in_array('id',$array_keys)){
            $update=true;
        }else{
            foreach($data as $key=>$value){
               if(!empty($value)){
                    $insert=true;
               }
            }
        }

        $data[$relation_info->table_name] = $master_table_insert_id;

        /**
         * Slave table da insert id ye sahip olan içeriğin relation info ile gelen table_name column name olarak algılanıp
         * o sütuna master table insert id girilecek sadece .
         * Slave table relation_info dan gelen slave modül idsinden çekilecek
         */
        if($insert){
            $slave_module_info = $this->model->get_module_info($relation_info->slave_module);
            $this->db->rows = $data;
            $this->db->table = $slave_module_info->table_name;
            $this->db->insert();
        }
        if($update){
            $slave_module_info = $this->model->get_module_info($relation_info->slave_module);
            $id = $data['id'];
            unset($data['id']);
            $this->db->rows = $data;
            $this->db->table = $slave_module_info->table_name;
            $this->db->where = "id='{$id}'";
            $this->db->update();
        }
    }

    private function many_to_many_relation($slave_module_data_id, $master_module_data_id, $relation_info)
    {
        /**
         * relation info da bulunan table-name tablosuna master table ve slave table insert id girilecek.
         * master table column name i module info table name sonuna  _id eklenerek oluşturuluyor
         * slave table column name i relation info dan gelen slave table id ye göre çekilecek olan modülün tablo adının sonuna
         * _id eklenerek oluşturulacak.
         * sonra bu tablodaki bu kolonlara gelen veriler girilecek
         */
        //many to many
        $this->db->rows = array(
            'master_module_data_id' => $master_module_data_id,
            'slave_module_data_id' => $slave_module_data_id
        );
        $this->db->table = $relation_info->table_name;
        $this->db->insert();
    }


    public function searchAjax()
    {
        $params = unserialize(urldecode($_GET['params']));
        $term = $_GET['term'];
        $regx = "~([^\.]+)\.([^\&]+)\&([^\?]+)\?(.*)~";
        preg_match_all($regx, $params['value'], $temp);
        $table_name = $temp[1][0];
        $key_column = $temp[3][0];
        $value_column = $temp[2][0];
        if (isset($temp[4]) && isset($temp[4][0])) {
            $where_clause = $temp[4][0];
        }
        $result = $this->db->query("SELECT * FROM {$table_name} WHERE {$value_column} LIKE '%{$term}%'");
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
        $r = array();
        foreach ($result as $key => $value) {
            $r[] = array(
                'id' => $value[$key_column],
                'label' => $value[$value_column],
                'value' => $value[$value_column]
            );
        }
        echo json_encode($r);
    }
}
