<?php
/**
 * Dosya yükleme işleme falan fıstık.
 *
 */
use PHPImageWorkshop\ImageWorkshop;
use Core\Config;

class upload {
    /**
     * @var $_file array $_FILES superglobalinin tutulacağı değişken
     * @var $_original_file string Orjinal boyutlarda yüklenen dosyanın tutulacağı değişken
     * @var $extension string Geçerli dosyanın uzantısının tutulacağı değişken
     * @var $extensions array İzin verilen dosya uzantılarının tutulacağı değişken
     * @var $image ImageWorkshop Resmin obje olarak tutulacağı değişken
     */
    private $_file, $_original_file, $extension, $extensions, $image;
    /**
     * @var $_upload_dir string Dosyaların yükleneceği dizin
     */
    public $_upload_dir;
    /**
     * @var $_file_alias object Dosya kayıt bilgilerinin veritabanından çekilip aktarılacağı değişken
     */
    public $_file_alias;
    /**
     * @var $file_name string Dosya adının tutulacağı değişken
     */
    public $file_name;

    public $_from_url = false;

    public $config;

    /**
     * @description Upload için gelen dosyanın süperglobaldeki adı ile sınıfı oluşturulur.
     * @param $file
     */
    public function __construct($file, $from_url = false) {
        $this->chmodFix(0777);
        if ($from_url) {
            $this->_file = $this->getFromUrl($file);
            $this->_from_url = true;
        } else {
            $this->_file = $_FILES[$file];
        }
        $this->_date_archive = date('d-m') . '/';
        $this->config = Config::init();
    }

    public function getFromUrl($url) {
        $url = utils::urlCheck($url);
        $file = array();
        $file['type'] = "application/octet-stream";
        $file['tmp_name'] = '/tmp/' . uniqid();
        $file['name'] = end(explode('/', $url));
        $file['error'] = 0;
        $file['size'] = 0;
        $cu = curl_init($url);
        curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
        $binary = curl_exec($cu);
        $file['info'] = curl_getinfo($cu);
        curl_close($cu);

        $file['size'] = strlen($binary);
        $handle = fopen($file['tmp_name'], "w+");
        fwrite($handle, $binary);
        fclose($handle);
        return $file;
    }

    /**
     * @description Burada dosya ile alaklı tüm ayarlar yapılıp dosya alt türleri ve orjinaliyle birlikte kaydedilir.
     */
    public function save() {
        if (!is_object($this->_file_alias)) {
            $this->_file_alias = utils::file_alias($this->_file_alias);
        }
        //First step save original
        $this->setExtensions();
        if (!$this->_original_file && $this->_file_alias->type == m_statics::media_types('Resim')) {
            $this->saveOriginal();
        }
        if ($this->_file_alias->type == m_statics::media_types('Resim')) {
            //Send image manipulators
            $this->manipulateAndSave();
        } else {
            //Only save
            $this->_upload();
        }
        if ($this->_file_alias->sub_settings) {
            foreach ((array)$this->_file_alias->sub_settings as $key => $value) {
                $this->_file_alias = $value;
                $this->save();
            }
        }

    }

    /**
     * @description Bu kısımda resim türünde yüklenen dosyalar için gerekli modifikasyon yapılıp gerekli klasörlere kayıt edilir.
     */
    private function manipulateAndSave() {
        //get manipulate type
        $manipulate_type = m_statics::image_modification_types($this->_file_alias->modifier);
        $this->image = ImageWorkshop::initFromPath($this->_original_file);
        switch ($this->_file_alias->modifier) {
            case 0; //İşlem yapma
                break;
            case 1; //Yeniden boyutlandır
                $this->image->resizeInPixel($this->_file_alias->image_width, $this->_file_alias->image_height, true);
                break;
            case 2; //Kırp
                /**
                 * Resim geniş ise yüksekliği eşitle.
                 * Resim uzun ise genişliği eşitle.
                 */
                if(($this->image->getHeight()/$this->image->getWidth())>($this->_file_alias->image_height/$this->_file_alias->image_width)){
                    $this->image->resizeInPixel($this->_file_alias->image_width,null,true);
                }else{
                    $this->image->resizeInPixel(null,$this->_file_alias->image_height,true);
                }

                $this->image->crop("pixel", $this->_file_alias->image_width, $this->_file_alias->image_height, 0, 0, 'MM');
                break;
            default:
                break;
        }
        //save image
        $dir = $this->config->folders['upload_dir']['backend'] . trim($this->_file_alias->folder, '/') . '/' . $this->_date_archive;
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
            $this->chmodFix(0777);
        }
        $this->image->save($dir, $this->file_name, true, null, 95);
        $this->chmodFixFile(0777, $dir . $this->file_name);

    }

    /**
     * @description Dosyanın yüklenebileceği tüm uzantıları metinden diziye çevirir.
     */
    private function setExtensions() {
        $this->extensions = explode(',', $this->_file_alias->extensions);
    }

    /**
     * @description Orjinal dosyayı gerekli yere kayıt edip $_original_file değişkenine dosyanın tam yolunu atar.
     * @throws Catcher
     */
    public function saveOriginal() {
        if (!$this->_file) {
            throw new Catcher("File not found!");
        }
        if (!$this->file_name) {
            $this->file_name = $this->createFileName();
        }
        $this->_upload($this->config->folders['upload_dir']['origins'] . trim($this->_file_alias->folder, '/') . '/');
        $this->_original_file = $this->config->folders['upload_dir']['origins'] . trim($this->_file_alias->folder, '/') . '/' . $this->_date_archive . $this->file_name;
    }

    /**
     * @description Tek dosya yükleme işlemleri sırasında dosyayı dizine yazmak için kullanılır. Parametresiz çalışırsa
     *              dosya aliasına göre su akar yolunu bulur.
     * @param null $dir
     */
    private function _upload($dir = null) {

        if (!$this->file_name) {
            $this->file_name = $this->createFileName();
        }

        if (!$dir) {
            $dir = $this->config->folders['upload_dir']['backend'] . trim($this->_file_alias->folder, '/') . '/';
        }

        $dir = $dir . '/' . $this->_date_archive;

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
            $this->chmodFix(0777);
        }
        if ($this->_from_url) {
            $file_type = $this->_file['info']['content_type'];
            copy($this->_file['tmp_name'], $dir . $this->file_name);
            unlink($this->_file['tmp_name']);
        } else {
            $destination = str_replace('//', '/', $dir . $this->file_name);

            move_uploaded_file($this->_file['tmp_name'], $destination);
        }
        $this->chmodFixFile(0777, $dir . $this->file_name);
    }

    /**
     * @description Dosya kayıt işleminden önce dosya adını ayarlar. Eğer ayarlarında yeniden adlandırma mevcutsa rastgele
     *              bir isim oluşturur. Yok değilse orjinal adını döndürür.
     * @return string Dosya adı
     */
    private function createFileName() {
        $old_name = $this->_file['name'];
        $this->splitExtension();
        if ($this->_file_alias->rename == '1') {
            //yeniden isimlendirilecek
            return $this->generateNewName() . '.' . $this->extension;
        } else {
            return $old_name;
        }
    }

    /**
     * @description Dosya adından dosya uzantısına ulaşır.
     * @param $filename
     * @throws AjaxCatcher
     */
    private function splitExtension() {
        $t = explode('.', $this->_file['name']);
        $this->extension = $t[count($t) - 1];
        if (!in_array(trim(strtolower($this->extension), '.'), $this->extensions)) {
            throw new AjaxCatcher("İzin verilen dosya uzantıları : " . join(',', $this->extensions));
        }
    }

    /**
     * @description Rastgele uzantısız bir dosya adı oluşturur.
     * @return string
     */
    private function generateNewName() {
        $time = time();
        $uniqu = uniqid();
        $f = str_shuffle($time . $uniqu);
        return $f;
    }

    /**
     * @description Mkdir işlemlerinden sonra dizinlerin yazma iznini fixlemek için kullanıyorum. Dizinleri tarayıp
     *              alt klasörlerle birlikte aynı ayara getiriyor
     * @param $mode
     * @param null $dir
     */
    private function chmodFix($mode, $dir = null) {
        if (!$dir) {
            if (!$this->config) {
                $this->config = Config::init();
            }
            $dir = $this->config->folders['upload_dir']['backend'];
        }
        $dirs = scandir($dir);

        foreach ((array)$dirs as $key => $value) {

            if ($value != '.' && $value != '..' && !empty($value)) {
                if (is_dir($dir . $value)) {
                    $d = umask(0);
                    if (!chmod($dir . $value, $mode)) {
                        throw new Exception('Klasör yetkilerini kontrol ediniz! Çözüm için : https://github.com/theylmz/addmean/wiki/Olas%C4%B1-Hatalar#yazma-zni-sorunu');
                    }
                    umask($d);
                    $this->chmodFix($mode, $dir . $value . '/');
                }
            }
        }
    }

    /**
     * @description Herhangi bir dosyayı yükledikten sonra chmod ayarını fixlemek için.
     * @param $mode
     * @param $file
     */
    private function chmodFixFile($mode, $file) {
        if (is_file($file)) {
            $d = umask(0);
            chmod($file, $mode);
            umask($d);
        }
    }
}
