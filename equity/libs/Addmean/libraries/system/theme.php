<?php
use Core\Config;

class theme {
    /**
     * @var Smarty
     */
    private static $ini;
    public static $smarty;
    private static $layout;
    public static $config;
    /**
     * @param $theme
     */
    public function __construct( $theme ) {

        self::$config = Config::init();
        if ( !self::$smarty ) {
            if(!class_exists('Smarty')){
                include libraries.'thirdparty/smarty/Smarty.class.php';
            }
            self::$smarty = new Smarty();
            self::$smarty->setCacheDir( self::$config->folders['smarty']['cache'] );
            self::$smarty->setCompileDir( self::$config->folders['smarty']['compile'] );
            self::$smarty->addPluginsDir( self::$config->folders['smarty']['plugins']);
            self::$smarty->setTemplateDir( self::$config->folders['smarty']['themes'] . $theme );
        }
        self::$layout = $theme;
        self::assign('config',self::$config);

    }

    public function application(){
        return self::$layout;
    }

    public function setup(){
        if ( !self::$ini ) {
            self::setIniFile(self::$layout);
        }
        $settings = self::$ini['app_settings'];
        $this->assign('pageTitle',$settings['title']);
        $this->assign('pageKeywords',$settings['keywords']);
        $this->assign('pageDescription',$settings['description']);
    }

    /**
     * @param $key
     * @param $value
     *
     * @return Smarty_Internal_Data
     */
    public function assign( $key, $value ) {
        return self::$smarty->assign( $key, $value );
    }

    /**
     * @param $tpl
     *
     * @return string
     */
    public function fetch($tpl){
        return self::$smarty->fetch($tpl);
    }
    /**
     * Layout içine sayfa dahil etmek için.
     * @param string $file
     */
    public function display( $file = '' ) {
        $page = self::$smarty->fetch( $file );
        self::$smarty->assign( 'page', $page );
        return self::$smarty->display( 'layout.tpl' );
    }

    /**
     * Layout olmadan render alabilmek için
     * @param string $file
     */

    public function viewContent($file=''){
        return self::$smarty->display($file);
    }

    /**
     * Esas plan şu ki ;
     * Buradan html kısmı render edecez, sonrasında body kısmını doldurup
     * öyle ekrana basacaz.
     */
    public static function getScripts( $layout = 'default' ) {
        //parse ini files
        if ( !self::$ini ) {
            self::setIniFile( $layout );
        }

        $tmp = array();
        $tmp['defaults'] = self::$ini['defaults']['script'];
        $tmp['extends'] = self::$ini['extends']['script'];
        return $tmp;

    }

    /**
     * Stil dosyalarını ini dosyasından (yahut veritabanından) alıp dahil eder.
     * @static
     *
     * @param string $layout
     *
     * @return array
     */
    public static function getStyles( $layout = 'default' ) {
        if ( !self::$ini ) {
            self::setIniFile( $layout );
        }
        $tmp = array();
        $tmp['defaults'] = isset(self::$ini['defaults']['style'])?self::$ini['defaults']['style']:array();
        $tmp['extends'] = isset(self::$ini['extends']['style'])?self::$ini['extends']['style']:array();
        return $tmp;
    }

    /**
     * Inı dosyalarını parse eder..
     * @static
     *
     * @param string $layout
     */
    private static function setIniFile( $layout = 'default' ) {
        $file = self::$config->folders['cms']['addmean'] . '/configuration/' . $layout . '.ini';
        $ini = parse_ini_file( $file, true );
        self::$ini = $ini;
    }

    /**
     * JS Pluginlerini getirir.
     * @static
     *
     * @param string $layout
     *
     * @return array
     */
    public static function getPlugins( $layout = 'default' ) {
        if ( !self::$ini ) {
            self::setIniFile( $layout );
        }
        $tmp = array();
        if(isset(self::$ini['defaults'])&&isset(self::$ini['defaults']['plugin'])){
            $tmp['defaults'] = self::$ini['defaults']['plugin'];
        }
        if(isset(self::$ini['extends'])&&isset(self::$ini['extends']['plugin'])){
            $tmp['extends'] = self::$ini['extends']['plugin'];
        }
        return $tmp;
    }

    /**
     * Pluginlerin içerisindeki structure.ini dosyalarını parse eder.
     *
     * @static
     *
     * @param string $plugin_dir
     * @param string $layout
     *
     * @return array
     */
    public static function setPluginIniFile( $plugin_dir = '', $layout = 'default' ) {
        $file = theme::$config->folders['document_root'].$plugin_dir . 'structure.ini';
        $ini = parse_ini_file( $file, 1 );
        return $ini;
    }

    /**
     * Pluginlerin içerisindeki dosyaları tam yoluyla döndürür.
     * @static
     *
     * @param string $plug
     * @param string $layout
     *
     * @return array
     */
    public static function getPluginFiles( $plug = '', $layout = 'default' ) {
        $plugindir = self::$config->folders['public_dir'].self::$config->folders['resource_assets'] . 'plugins/' . $plug . '/';
        $plug_ini = self::setPluginIniFile( $plugindir, $layout );
        $scripts = array();
        if(isset($plug_ini['script'])){
            foreach((array)$plug_ini['script'] as $p){
                $scripts[]=$plugindir.$p;
            }
        }

        $styles = array();
        if(isset($plug_ini['style'])){
            foreach((array)$plug_ini['style'] as $s){
                $styles[] = $plugindir.$s;
            }
        }

        return $files = array(
            'scripts'=>$scripts,
            'styles'=>$styles
        );
    }
}