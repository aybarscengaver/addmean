<?php
/**
 * User: aybarscengaver
 * Date: 07.04.2013
 * Time: 22:56
 * File: archive.php
 * Package: AddMean
 */

class archive {
    protected $_table_name,$db;
    public function __construct($table_name){
        $this->_table_name=$table_name;
        $this->db = DB::singleton();
    }

    public function update($content_id,$data){
        $this->db->where = "id='{$this->generateId($content_id)}'";
        $this->db->rows['name'] = isset($data['name'])?$data['name']:null;
        $this->db->rows['title'] = isset($data['title'])?$data['title']:null;
        $this->db->rows['content'] = isset($data['content'])?$data['content']:(isset($data['detail'])?$data['detail']:null);
        $this->db->rows['content_id'] = isset($data['id'])?$data['id']:0;
        $this->db->rows['data'] = isset($data)?json_encode($data):'{}';
        $this->db->rows['module_name'] = isset($this->_table_name)?$this->_table_name:'';
        $this->db->table='archive';
        $this->db->update($archive=true);
    }

    public function insert($data){
        $this->db->rows=Array();
        $this->db->rows['id'] = $this->generateID($data['id']);
        $this->db->rows['name'] = isset($data['name'])?$data['name']:null;
        $this->db->rows['title'] = isset($data['title'])?$data['title']:null;
        $this->db->rows['content'] = isset($data['content'])?$data['content']:(isset($data['detail'])?$data['detail']:null);
        $this->db->rows['content_id'] = isset($data['id'])?$data['id']:0;
        $this->db->rows['data'] = isset($data)?json_encode($data):'{}';
        $this->db->table='archive';
        $this->db->insert(null,true);
    }

    public function batch_insert($data,$ids){
        foreach($data as $key=>$value){
            $value['id'] = $ids[$key];
            $this->insert($value);
        }
    }

    private function generateID($content_id){
        return $this->_table_name.''.$content_id;
    }
}