<?php
/**
 * User: delirehberi
 * Date: 02.01.2013
 * Time: 15:34
 * Filename: doms.php
 * Description:
 * Usage:
 */
use Core\Config;

class doms {
    public static $instance = array();
    public static $theme, $module, $config; //smarty object
    public $model;

    public function __construct() {
        $this->model = model::singleton();
        self::$module = form::$module;
        self::$config = Config::init();
    }

    /**
     * @description Her eleman oluşturulmadan önce çalışıp parametreler için gereken düzenleme işlemini yapar.
     * @param $params
     * @return mixed
     */
    public function beforeInit(&$params) {
        return $params;
    }

    /**
     * @param null $theme Smarty Object
     * @return mixed
     */
    public static function singleton($theme = null) {
        self::$theme = $theme;
        $called_class = get_called_class();
        if (!isset(self::$instance[$called_class])) {
            self::$instance[$called_class] = new $called_class();
        }
        return self::$instance[$called_class];
    }

    /**
     *
     * @param $params
     * @param $theme
     */
    public function init($params, $theme) {
        $theme->assign('params', $params);
        $theme->assign('label', _($params['label']));
        $theme = $theme->fetch(self::$config->folders['dom_elements'] . $params['type'] . '/index.tpl');
        return $theme;
    }

    /**
     * @description  Listeleme sayfasında görüntülenen veri
     * @param $params
     * @param $value
     * @return mixed
     */
    public function getValue($params, $value) {
        $column_name = $params->column_name;
        return $value->$column_name;
    }

    /**
     * @description Her eleman çalışmasından sonra çağrılır.
     * @param $params
     */
    public function afterInit(&$params) {

    }

    protected function getTableData($params, $hierarchic = false) {
        //Is table data
        $db = DB::singleton();
        //table_name.value_row&key_row?where_clause
        preg_match_all("/([^\.]+)\.([^\&]+)\&([^\?]+)\??(.*)?/", $params['value'], $data);
        $params['table_name'] = $data[1][0];
        $params['value_row'] = $data[2][0];
        $params['key_row'] = $data[3][0];
        $params['where_clause'] = isset($data[4][0]) ? $data[4][0] : '';
        $__name = $params['value_row'];
        $__id = $params['key_row'];
        $this->model->set_table($params['table_name']);
        $language = new \Core\Languages($this->model);
        if ($hierarchic) {
            $data = $this->model->getHierarchic("{$params['value_row']},{$params['key_row']}", $params['where_clause']);
            $params['value'] = array();
            foreach ($data as $key => $value) {
                $k = $params['key_row'];
                $v = $params['value_row'];
                $params['value'][$value->$k] = new stdClass();
                $params['value'][$value->$k]->$__name = $value->$v;
                $params['value'][$value->$k]->$__id = $value->$k;
                if ($value->childrens) {
                    $params['value'][$value->$k]->childrens = $value->childrens;
                }
            }

        } else {
            $data = $this->model->get("{$params['value_row']},{$params['key_row']}", $params['where_clause']);
            $params['value'] = array();
            foreach ($data as $key => $value) {
                $k = $params['key_row'];
                $v = $params['value_row'];
                $params['value'][$value->$k] = $value->$v;
            }
        }
        return $params;
    }
}