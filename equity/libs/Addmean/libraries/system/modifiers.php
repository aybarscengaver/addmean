<?php
/**
 * User: wyrus
 * Date: 8/5/12
 * Time: 5:20 PM
 * Company: Deli
 */
class modifiers {

    public static function none_modifier($data=null){
        return $data;
    }

    /**
     * md5 şifreleme
     * @param null $data
     * @return string
     */
    public static function md5_modifier($data=null){
        if(!empty($data)){
           $data = md5($data);
        }
        return $data;
    }

    /**
     * Girilen veri boş ise şimdiki tarihe dönüştürür.
     * @param null $data
     * @return null|string
     */
    public static function now_date_modifier($data=null){
        if(empty($data)){
            $data = date("d-m-Y");
        }
        return $data;
    }

    /**
     * @description Bir checkbox türünde alan varsa, işaretli olmadığı zaman değerinin sıfır gitmesi gerekiyor.
     * @param null $data
     * @return int
     */
    public static function checkbox_modifier($data=null){
        //Gerek kalmadı buna
        return $data;
    }

    /**
     * @param null $data
     * @return null
     */
    public static function multiselect_modifier($data=null){
        return $data;
    }
}
