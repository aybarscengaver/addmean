<?php

/**
 *
CREATE TABLE IF NOT EXISTS `email_layouts` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`key` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
`subject` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
`layout` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=3 ;

 */
namespace Email;

require_once(dirname(__FILE__).'/../thirdparty/PHPMailer/class.phpmailer.php');
class emailer extends \PHPMailer{
    protected $smarty,$db;
    public function __construct($exceptions = false){
        /**
         * Kişisel ayarlamalar
         */
        $this->db=\DB::singleton();
        $this->smarty = \core::$theme;
        parent::__construct($exceptions);
    }

    public function renderEmail($file){}
    public function configure(){
        $config = \Core::$config;
        #regex smtp://email:password@server:port
        $regex = "~([^\:]+)\:\/\/([^\:]+)\:([^\@]+)\@([^\:]+)\:(\d+)~";
        preg_match_all($regex,$config->connection_string,$tmp);
        if($tmp[1][0] == 'smtp'){
            $this->IsSMTP();
            $this->Username = $tmp[2][0];
            $this->Password = $tmp[3][0];
            $this->Port = $tmp[5][0];
            $this->Setfrom($tmp[2][0],$config->pageTitle);
            //$this->SMTPDebug = true;
//            $this->SMTPSecure = "ssl";
            $this->SMTPAuth = true;
            $this->CharSet = 'UTF-8';
//            $this->SMTPDebug=true;
        }
        $this->Host=$tmp[4][0];
    }
    public function send($body,$subject,$to=null){
//        $this->SetFrom($from,$config->site_title);
        $config = \Core::$config;
        $admins = $config->admins_emails;
        $admins = explode(',',$admins);
        foreach($admins as $a){
            $this->AddAddress($a);
        }
        if($to){
            $this->AddAddress($to);
        }
        $this->Subject = $subject;
        $this->Body = $body;
        $this->IsHTML(true);
        $this->configure();
        try{
           parent::Send();
           //echo "{$from} kullanıcısından {$to} kullanıcısına eposta gönderildi.";
        }catch (\phpmailerException $e){
            print_r($e);
        }
    }


}