<?php

/**
 * @name Catcher
 * @desc    Ajax Hata yakalama sınıfı.
 */
class AjaxCatcher extends Exception {
    /**
     * @name show
     * @desc Yazılım geliştirelere gösterilecek hata mesajı formatı.
     * @return string
     */
    public function show() {

        $data = array();
        $data['type'] = "failed" ;
        $data['msg'] = parent::getMessage();
        $data['code'] = parent::getCode();
        $data['line'] = parent::getLine();
        $data['file'] = parent::getFile();
        $data['title'] = 'Hata!';
        $data = json_encode($data);
        return $data;
    }
    /**
     * @name lite
     * @desc Son kullanıcıya gösterilecek hata mesajı formatı
     * @return string
     */
    public function lite(){
        $data = array();
        $data['type'] = 'failed';
        $data['msg'] = "Beklenmedik bir hata oluştu.";
        $data['title'] = 'Hata';
        return json_encode($data);
    }
}