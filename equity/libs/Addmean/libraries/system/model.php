<?php
/**
 *Genel model dosyası henüz dahil edilmedi
 */
class Model
{
    public static $db;
    /**
     * @var _table_name Model tablo adı
     * @var _original_table Dil destekli m
     */
    protected $_table_name,$_original_table_name;
    private static $instance;
    protected $_fields_table_name = 'module_fields';
    protected $session;
    public $extra_fields=array();

    public function __construct()
    {
        self::$db = \DB::singleton();
        $this->session = m_session::singleton();
        $this->_original_table_name = $this->_table_name;
        $lang = new \Core\Languages($this);

    }
    public static function singleton()
    {
        $model_class = get_called_class();
        if (isset(self::$instance[$model_class]) && !empty(self::$instance[$model_class])) {
            return self::$instance[$model_class];
        }
        self::$instance[$model_class] = new $model_class();

        return self::$instance[$model_class];
    }
    /**
     * @param string $rows
     * @param null $where
     * @param null $page_no
     * @param null $limit
     * @param null $order_by
     * @param null $table_name
     * @return array
     */
    public function get($rows = "*", $where = null, $page_no = null, $limit = null, $order_by = null, $table_name = null, $join = null, $group_by = null)
    {
        self::$db->reset();
        if (!isset($table_name) || empty($table_name)) {
            self::$db->table = $this->_table_name;
        } else {
            self::$db->table = $table_name;
        }
        self::$db->rows = $rows;
        self::$db->limit = $limit;
        self::$db->where = $where;
        self::$db->page_no = $page_no;
        self::$db->orderBy = $order_by;
        self::$db->join = $join;
        self::$db->groupBy = $group_by;
        return self::$db->read();
    }

    public function get_count($where = null, $table_name = null, $join = null)
    {
        self::$db->reset();
        if (!isset($table_name) || empty($table_name)) {
            self::$db->table = $this->_table_name;
        } else {
            self::$db->table = $table_name;
        }
        self::$db->where = $where;
        self::$db->join = $join;
        self::$db->count = true;
        $count = self::$db->read();
        return $count->count;
    }

    public function getHierarchic($rows = "*", $where = null, $page_no = null, $limit = null, $order_by = null, $table_name = null, $parent_id = 0)
    {
        self::$db->reset();
        if (!isset($table_name) || empty($table_name)) {
            self::$db->table = $this->_table_name;
        } else {
            self::$db->table = $table_name;
        }
        self::$db->rows = $rows;
        self::$db->limit = $limit;
        $wheres = array();
        if ($where) {
            $wheres[] = $where;
        }
        if (!$parent_id) {
            $wheres[] = "(parent_id IS NULL OR parent_id='0')";
        } else {
            $wheres[] = "parent_id='{$parent_id}'";
        }
        self::$db->where = join(' AND ', $wheres);
        self::$db->page_no = $page_no;
        self::$db->orderBy = $order_by;
        $data = self::$db->read();
        if ($data) {
            foreach ((array)$data as $key => $value) {
                $data[$key]->childrens = $this->getHierarchic($rows, $where, $page_no, $limit, $order_by, $table_name, $value->id);
            }
        }
        return $data;
    }

    public function delete($id, $table = null)
    {
        self::$db->reset();
        if (!$table) {
            self::$db->table = $this->_table_name;
        } else {
            self::$db->table = $table;
        }
        self::$db->where = "id='{$id}'";
        self::$db->delete();
        return 1;
    }

    public function drop_table($table)
    {
        self::$db->reset();
        self::$db->table = $table;
        self::$db->drop_table();
        return 1;
    }

    public function drop_column($table, $column_name)
    {
        self::$db->query("ALTER TABLE {$table} DROP `{$column_name}` ");

    }

    public function get_module_name()
    {
        return $this->_original_table_name;
    }

    public function get_module_info($id = 0)
    {
        if (!empty($id) && strlen($id) < 3) { //Nedense bu kısımda int olarak görmüyordu id yi
            // ayrıca string int karıştı iyicene
            $id = (int)$id;
        }
        self::$db->reset();
        self::$db->table = "modules";
        if (!$id) {
            self::$db->where = "table_name='{$this->_original_table_name}'";
        } elseif (is_int($id)) {
            self::$db->where = "id='{$id}'";
        } else {
            self::$db->where = "table_name='{$id}'";
        }

        $module = self::$db->read();

        if ($module) {
            return $module[0];
        }
        return false;
    }

    public function get_module_frontend_fields($module_name=null)
    {
        if(!$module_name){
            $module_name=$this->_original_table_name;
        }
        $module = $this->get_module_info($module_name);
        $fields = $this->get('*', "module_id='{$module->id}}' AND frontend_form='1'", null, null, "order_no ASC", "module_fields");
        return $fields;
    }

    public function get_module_languages($module_name=null){
        if(!$module_name)
            $module_name=$this->_original_table_name;
        $module = $this->get_module_info($module_name);
        if(!$module){
            return array();
        }
        $langs = $module->multi_language;
        if(is_null($langs) || empty($langs)){
            return array();
        }
        $langs = json_decode($langs);
        return $langs;
    }


    public function set_table($table_name)
    {
        $this->_table_name = $table_name;
        $this->_original_table_name = $table_name;
        return $this;
    }

    public function set_table_suffix($suffix=''){
        $this->_table_name = $this->_original_table_name.$suffix;
        return $this;
    }

    public function insert($data)
    {
        $this->module_field_check($data);

        self::$db->reset();
        self::$db->rows = $data;
        self::$db->table = $this->_table_name;
        return self::$db->insert();
    }

    public function batch_insert($data){
        self::$db->reset();
        self::$db->batchData=$data;
        self::$db->table=$this->_table_name;
        return self::$db->batch_insert();
    }

    public function module_field_check(&$data)
    {
        if ($this->is_module(ajax::$origin_table_name)) {
            //fieldları kontrol etmeliyim (güvenlik meselesi)
            $module = $this->get_module_info($this->_original_table_name);

            $fields = $this->list_fields($module->id);

            $seo = new stdClass();
            $seo->column_name='seo';
            $seo->type=m_statics::field_types('text');
            array_push($fields,$seo);
            foreach($this->extra_fields as $f){
                array_push($fields,$f);
            }

            $array_keys = array_keys($data);
            $_data = array();
            foreach ($data as $key => $value) {
                reset($fields);
                foreach ($fields as $f_key => $f_value) {
                    if($f_value->column_name==$key){
                        $_data[$key]=$value;
//                        echo "\n\t{$f_value->column_name}=={$key} nBreak\n";
                    }elseif($f_value->type==m_statics::field_types('checkbox')&&$f_value->edit==1&&!in_array($f_value->column_name,$array_keys)){
                        $_data[$f_value->column_name]=0;
//                        echo "\n\t{$f_value->column_name} type==5 && edit=1 nBreak\n";
                    }
                }
            }
//            print_r($_data);
//            exit;
            $data = $_data; //artık temiz ;)
        }
    }

    public function edit($data, $id)
    {

        $this->module_field_check($data);
        self::$db->reset();
        self::$db->rows = $data;
        self::$db->table = $this->_table_name;
        self::$db->where = "id='{$id}'";
        return self::$db->update();
    }

    public function has_seo($table_name = null)
    {

        $module = $this->get_module_info($table_name);
        if (!empty($module->seo)) {
            return true;
        }
        return false;
    }

    /**
     * @description Modül tablo adı güncellendiğinde veritabanındaki orjinal tablonun adını da değiştiriyor.
     * @param $old_data object
     * @param $new_data array
     */
    public function check_table_name($old_data, $new_data)
    {
        if ($old_data->table_name != $new_data['table_name']) {
            //table name is changed, yellow alert!
            self::$db->table = $old_data->table_name;
            self::$db->rename_table($new_data['table_name']);

            //&change other lang tables
            $langs = m_statics::languages();
            foreach($langs as $key=>$value){
                self::$db->table = $old_data->table_name.'_'.$key;
                self::$db->rename_table($new_data['table_name'].'_'.$key);
            }
        }
    }

    public function check_order_column($old_data, $new_data)
    {
        if (isset($old_data->order) && $old_data->order == "1" && (!isset($new_data['order']) || $new_data['order'] == "0")) {
            //Önceden vardı ve kaldırıldı
            self::$db->table = $new_data['table_name']; // önceden check table yapılmıştı ismi değişmişse patlamasın.
            self::$db->drop_column('order_no');
            $langs = m_statics::languages();
            foreach($langs as $key=>$value){
                self::$db->table = $new_data['table_name'].'_'.$key; // önceden check table yapılmıştı ismi değişmişse patlamasın.
                self::$db->drop_column('order_no');
            }
        } elseif (($old_data->order == 0 || is_null($old_data->order) || empty($old_data->order)) && isset($new_data['order']) && $new_data['order'] == "1") {
            //önceden yoktu ve eklendi
            self::$db->table = $new_data['table_name'];
            self::$db->add_column('order_no', 'int', 10);
            $langs = m_statics::languages();
            foreach($langs as $key=>$value){
                self::$db->table = $new_data['table_name'].'_'.$key;
                self::$db->add_column('order_no', 'int', 10);
            }
        } else {
            /**
             * Değişiklik yok maalesef :\
             */
        }
    }

    public function is_module($table_name = null)
    {
        self::$db->table = "modules";
        if ($table_name) {
            self::$db->where = "table_name='{$table_name}'";
        } else {
            self::$db->where = "table_name='{$this->_original_table_name}'";
        }
        self::$db->count = true;
        $check = self::$db->read();
        self::$db->reset();
        if ($check->count > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_relation($relation_id)
    {
        self::$db->table = "module_relations";
        self::$db->where = "id='{$relation_id}'";
        self::$db->limit = 1;
        $data = self::$db->read();
        return $data;
    }

    public function __call($name, $params)
    {
        /**
         * Önce search için
         */
        $search_regexp = '/^likeBy(.+)$/';
        preg_match_all($search_regexp, $name, $tmp);
        if (isset($tmp[1]) && isset($tmp[1][0])) {
            $column_name = (strtolower($tmp[1][0]));
            self::$db->table = $this->_table_name;
            self::$db->where = "`{$column_name}` LIKE '%{$params[0]}%'";
            if (isset($params[1])) {
                self::$db->limit = $params[1];
            }
            $data = self::$db->read();
            if ($column_name == 'id') {
                return $data[0];
            }
            return $data;
        }
        /**
         * Sonra get için
         */
        $get_regexp = '/^getBy(.+)$/';
        preg_match_all($get_regexp, $name, $tmp);
        if (isset($tmp[1]) && isset($tmp[1][0])) {
            $column_name = (strtolower($tmp[1][0]));
            self::$db->table = isset($params[2]) ? $params[2] : $this->_table_name;
            self::$db->where = "`{$column_name}`='{$params[0]}'";
            if (isset($params[1])) {
                self::$db->rows = $params[1];
            }
            $data = self::$db->read();
            if (!$data) {
                return array();
            }
            if ($column_name == 'id' || $column_name=='seo') {
                return $data[0];
            }
            return $data;
        }


        $get_regexp = '/^getInBy(.+)$/';
        preg_match_all($get_regexp, $name, $tmp);
        if (isset($tmp[1]) && isset($tmp[1][0])) {
            $column_name = (strtolower($tmp[1][0]));
            self::$db->table = isset($params[2]) ? $params[2] : $this->_table_name;
            self::$db->where = "`{$column_name}` IN ({$params[0]})";
            if (isset($params[1])) {
                self::$db->rows = $params[1];
            }
            $data = self::$db->read();
            if (!$data) {
                return array();
            }
            return $data;
        }
    }

    /**
     * Aşağıdaki metodun burada olmaması gerektiğinin farkındayım fakat beni affedin, başka koyacak yer yoktu.
     * Aynı metoddan kesinlikle 2 tane oluşturmam.
     */
    /**
     * @description Modülle bağlı olan diğer modüllerin özellikleriyle birlikte döndürür.
     * @param $module_id
     * @return array
     */
    public function get_relations($module_name, $data_id = 0)
    {
        self::$db->reset();
        self::$db->table = "module_relations mr";
        self::$db->where = "mr.master_module='{$module_name}'";
        self::$db->join = "INNER JOIN modules m ON m.table_name=mr.slave_module";
        self::$db->rows = "m.id as module_id,
                            m.name as module_name,
                            m.table_name as module_table_name,
                            m.type as module_type,
                           mr.id,
                           mr.master_module,
                           mr.slave_module,
                           mr.repeat_count,
                           mr.type,
                           mr.table_name
        ";

        $relations = self::$db->read();
        $modules = array();
        $m = array();

        foreach ($relations as $r => $z) {
            //modülün özelliklerini çek
            $m[$r]['info'] = $z;
            if (!empty($data_id)) {
                if ($z->type == 1) {
                    //many to many
                    /*
                     * Aslına bakarsak burada yapılacak iş biraz daha farklı
                     * Burada düzenleme ekranı formu için gereken veriler çekilecek.
                     */
                    $datas = Array();
                    self::$db->table = $z->table_name;
                    self::$db->where = "`{$z->table_name}`.master_module_data_id='{$data_id}'";
                    self::$db->join = "INNER JOIN `{$z->slave_module}` ON `{$z->slave_module}`.id=`{$z->table_name}`.slave_module_data_id";
                    self::$db->rows = "`{$z->slave_module}`.*";
                    $datas = self::$db->read();
                } elseif ($z->type == 2) {
                    /**
                     * Fieldlara sadece onetomany ilişkide ihtiyaç var
                     * manytomany ilişkide veri ekleme olmayıp veri ilişkilendirme olduğundan dolayı
                     * fieldlar çekilmedi.
                     * Benden ötürü beya... [emre]
                     */
                    $fields = $this->list_fields($z->module_id);
                    $m[$r]['fields'] = $fields;
                    //one to many

                    /**
                     * Buradayken master tabloya girip tablo adı olarak gelen sütun adından veriyi çekeceksin.
                     */
                    self::$db->table = $z->module_table_name;
                    self::$db->where = "{$z->table_name}='{$data_id}'";
                    $datas = self::$db->read();
                }
                $m[$r]['datas'] = $datas;
            } else {
                if ($z->type == 1) {
                    //many to many
                    /*
                     * Aslına bakarsak burada yapılacak iş biraz daha farklı
                     * Burada düzenleme ekranı formu için gereken veriler çekilecek.
                     */
                    $datas = Array();

                } elseif ($z->type == 2) {
                    /**
                     * Fieldlara sadece onetomany ilişkide ihtiyaç var
                     * manytomany ilişkide veri ekleme olmayıp veri ilişkilendirme olduğundan dolayı
                     * fieldlar çekilmedi.
                     * Benden ötürü beya... [emre]
                     */
                    $fields = $this->list_fields($z->module_id);
                    $m[$r]['fields'] = $fields;
                    //one to many

                    /**
                     * Buradayken master tabloya girip tablo adı olarak gelen sütun adından veriyi çekeceksin.
                     */
                    self::$db->table = $z->module_table_name;
                    self::$db->where = "{$z->table_name}='{$data_id}'";
                    $datas = self::$db->read();

                }
                $m[$r]['datas'] = $datas;
            }
            /**
             * Burada
             * Mysql'den sadece alt tablonun id ve label verisini getirdiğimiz zaman sorun çözülecek.
             * Çünkü bu verileri multiselect olarak göstereceğiz ve seçilen içerik veritabanındaki gerekli
             * birleştirici tablo üzerinde yeni alanlar oluşturacak.
             * Zaten master tablomuz belli olduğu için sıkıntı yok ;)
             */


            if ($z->type == 1) {
                $modules['many_to_many'] = $m;
            } else {
                $modules['one_to_many'] = $m;
            }

        }
        return $modules;
    }

    /**
     * Bu da yukarıdakiyle aynı şartlara sahip olduğu için burada:
     */

    /**
     * @description Bir modülün tüm özelliklerini döndürür.
     * @param $module_id
     * @return object
     */
    public function list_fields($module_id)
    {
        $data = $this->get('*', "module_id='{$module_id}'", null, null, "order_no ASC", $this->_fields_table_name);
        return $data;
    }

}