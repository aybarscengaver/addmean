<?php

/**
 * Class Controller
 * Description: Main controller class.
 */
use Core\Languages;

class Controller {
    public $db;
    public $model;
    public $module_info;
    protected $_list_limit = 30;
    public $main_controller;
    public $session,$language;
    public static $config;

    public function __construct() {
        core::$theme->setup();
        self::$config = core::$config;
        $model_class_name = "m_" . get_called_class();
        if (is_callable(array($model_class_name, 'get'))) {
            $this->model = new $model_class_name();
        } else {
            $this->model = new model();
        }
        $this->assign('module_name', $this->model->get_module_name());
        $this->db = db::singleton();
        $main_controller = (dirname(__DIR__) . '/../modules/' . core::$theme->application()) . '/main.php';
        include_once $main_controller;
        $this->session = m_session::singleton();
        $this->language = new Languages($this->model);
        $this->main_controller = new mainController($this);
        $this->setTitle(self::$config->pageTitle);
        $this->setDescription(self::$config->pageDescription);
        $this->setKeywords(self::$config->pageKeywords);
    }


    public function soap() {
        $config = \Core\Config::init();
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient(null, array('uri' => $config->soap['uri'], 'location' => $config->soap['location'],));
        return $client;
    }

    /**
     * @description Variable assign to tpl files.
     * @param $key
     * @param $value
     */
    public function assign($key, $value) {
        core::$theme->assign($key, $value);
    }

    /**
     * @description Page rendering
     * @param string $file
     */
    protected static function display($file = null, $controller = null) {
        if (!$controller) {
            $controller = get_called_class();
        }
        if (!$file) {
            $file = debug_backtrace();
            $file = $file[1]['function'];
            $file = str_replace('Action', '', $file); //todo daha adam akıllı bişi olmalı burada
        }
        if ($file == 'init') {
            $file = 'index';
        }
        core::$theme->display($controller . '/' . $file . '.tpl');
    }

    /**
     * @description Page rendering without layout.
     * @param null $file
     * @param null $controller
     */
    public function viewContent($file = null, $controller = null) {
        if (!$controller) {
            $controller = get_called_class();
        }
        if (!$file) {
            $file = debug_backtrace();
            $file = $file[1]['function'];
        }
        if ($file == 'init') {
            $file = 'index';
        }

        core::$theme->viewContent($controller . '/' . $file . '.tpl');
    }

    /**
     * @description If special_buttons.tpl having in module template directory, get special buttons and render for list
     *              view
     * @return null|string
     */
    public function getSpecialButtons() {
        $controller = get_called_class();
        $file = 'special_buttons.tpl';
        $full = self::$config->folders['cms']['addmean'] . 'templates/' . core::$application . '/' . $controller . '/' . $file;
        if (is_file($full)) {
            return core::$theme->fetch($controller . '/' . $file);
        }
        return null;
    }

    /**
     *  Controller içinden yapılacak veritabanı bağlantısı için
     *  Veritabanı nesnesini oluşturur.
     * @return type
     */
    protected function db() {
        include_once self::$config->folders['cms']['libraries'] . 'system/db.php';
        return $this->db =& db::singleton();
    }

    /**
     * @description Check for admin required areas \ temp
     */
    protected function checkLogin() {
        $user = $_SESSION['admin_user'];
        if (!$user) {
            header('Location: /root/user/login');
        }
    }

    /**
     * @description Set page meta keywords in method
     * @param null $keywords
     */
    protected function setKeywords($keywords = null) {
        if (!$keywords) {
            return;
        }
        self::assign('pageKeywords', $keywords);
    }

    /**
     * @description Set page meta description in method
     * @param null $description
     */
    protected function setDescription($description = null) {
        if (!$description) {
            return;
        }
        self::assign('pageDescription', $description);
    }

    protected function setTitle($title = null) {
        if (!$title) {
            return;
        }
        self::assign('pageTitle', $title);
    }

    public function listAction($page_no = 0) {
        if (!$this->model->is_module()) {
            throw new Catcher("Bu kayıtlı bir modül değil!", 562);
            /**
             * Hata Kodu : 562
             * Açıklama : El ile oluşturulmuş olan bir modülün tablo ve sütun bilgileri elimizde olmadığı için
             *            bu modülü ön tanımlı metodlar ile çalıştıramayız.
             */
        }
        $module_name = $this->model->get_module_name();
        $module_model = new m_modules();

        /**moduleinfo*/
        $module = $module_model->get("*", "table_name='{$module_name}'");
        $module = $module[0];
        if ($module->order == 1) {
            $orders[] = 'order_no ASC';
        }
        $orders[]= "id DESC";
        $order = join(',',$orders);
        $module_columns = $module_model->list_fields($module->id);
        $data = $this->model->get("*", null, $page_no, $this->_list_limit, $order);
        $data_count = $this->model->get_count();

        $this->assign('data_count', $data_count);
        $this->assign('current_page', $page_no);
        $this->assign('data', $data);
        $this->assign('module_columns', $module_columns);
        $this->assign('module', $module);

        //special buttons
        $special_buttons = $this->getSpecialButtons();
        $this->assign('special_buttons', $special_buttons);
        $this->assign('list_limit', $this->_list_limit);
        $this->display('list_module_data', 'modules');
    }

    /**
     * @description Default createAction for every controllers
     * @throws Catcher
     */
    public function createAction() {
        if (!$this->model->is_module()) {
            throw new Catcher("Bu kayıtlı bir modül değil!");
        }
        $module_name = $this->model->get_module_name();
        $module_model = new m_modules();
        /**moduleinfo*/
        $module = $module_model->get("*", "table_name='{$module_name}'", 0, 1);
        $module_fields = $module_model->list_fields($module->id);
        $module_relations = $module_model->get_relations($module->table_name);

        $this->assign('module_relations', $module_relations);
        $this->assign('module_fields', $module_fields);
        $this->assign('module', $module);
        $this->display('create_module_data', 'modules');
    }

    /**
     * @description Default editAction method for every controller
     * @param $id
     * @throws Catcher
     */
    public function editAction($id) {
        if (!$this->model->is_module()) {
            throw new Catcher("Bu kayıtlı bir modül değil!");
        }
        $module_name = $this->model->get_module_name();
        $module_model = new m_modules();
        /**moduleinfo*/
        $module = $module_model->get("*", "table_name='{$module_name}'");
        $module = $module[0];
        $module_fields = $module_model->list_fields($module->id);
        $module_relations = $module_model->get_relations($module->table_name, $id);

        $this->assign('module_relations', $module_relations);



        $data = $this->model->get('*', "id='{$id}'");
        $this->assign('data', $data[0]);
        $this->assign('module_fields', $module_fields);
        $this->assign('module', $module);
        $this->display('edit_module_data', 'modules');
    }

    public function viewAction($seo) {
        $this->display();
    }

    public function data_importAction() {
        $module = $this->model->get_module_info();
        $this->assign('module', $module);
        $this->display('data_import', 'modules');
    }

    public function data_exportAction() {
        $data = $this->model->get();
        $json_data = json_encode($data);
        header("Content-Type:text/json;charset=utf-8");
        echo $json_data;
    }

    /**
     * @description If page not exist, throw new exception for now. But on future, this method, save error logs an
     *              manage 301 redirects for new urls.
     * @param null $page
     * @throws Catcher
     */
    public function not_found($page = null) {
        throw new Catcher('Page Not Found', 404);
    }

    /**
     * @description
     *             1 = If method/page not set on controller, default display if exist tpl file .
     *             2 = Return model singleton method if $name has m_ string
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments) {
        if (strstr($name, 'm_')) {
            return $name::singleton();
        } else {
            $this->assign('params', $arguments);
            $name = str_replace('Action', '', $name);
            $this->display($name);
        }
    }
}
