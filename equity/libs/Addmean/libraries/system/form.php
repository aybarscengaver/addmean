<?php
/**
 * User: wyrus
 * Date: 8/3/12
 * Time: 12:01 AM
 * Company: Deli
 */
use Core\Config;
class form
{
    public static $instance;
    public static $count,$module,$config;
    private $theme;

    public static  function singleton($module_info=array())
    {
        if (!self::$instance) {
            self::$instance = new form($module_info);
        }

        return self::$instance;
    }

    public function __construct($module_info=array())
    {
        self::$module = $module_info;

        if(isset($module_info['application'])){
            $this->theme = new theme($module_info['application']);
        }else{
            $this->theme = new theme('root');
        }
        self::$count = 1;
        self::$config = Config::init();
    }

    public function loadDOM($label, $params, $value = null,$related_data_id)
    {
        if (is_string($params)&&strpos($params,"|")===false) {

            $relation_table_name = $params;
        } else {
            $relation_table_name = false;
        }
        if (is_object($label)) {

            $params = self::convertDOMString($label, $value);
            $label = $label->label;
        }

        $params = self::parseParams($params); //Bu kısımda string halindeki eleman özellikleri diziye aktarılıyor.

        $params['id'] = $params['id'] . '_' . self::$count;
        self::$count++;

        if($relation_table_name){

            //her halükarda kontrol etmeyeceksin arkadaşım.
            $params['validate'] = 'optional';
        }
        //generate name is here
        $params['name'] = $this->generateDomName($params, $relation_table_name,$related_data_id);

        $params['label'] = $label;
        if(isset($params['selected']) && empty($params['selected'])){
            $params['selected'] = $value;
        }

        if (!$params['type']) {
            return;
        }


        if (is_file(self::$config->folders['dom_elements'] . $params['type'] . '/index.php')) {
            /**
             * Bu kısımda dom işlemesi ve döndürülmesi özel olarak yapılıyor
             */
            require_once self::$config->folders['dom_elements'] . $params['type'] . '/index.php';

            $class_name = $params['type'] . '_dom';
            $class = $class_name::singleton($this->theme);

            $class->beforeInit($params);

            $theme = $class->init($params, $this->theme);
            $class->afterInit($params);
        } else {
            /**
             * Bu kısımda dom'un herhangi bir özel işlemi olmadığı için genel işlem uygulanıyor.
             */
            $this->theme->assign('params', $params);
            $this->theme->assign('label', $label);
            $theme = $this->theme->fetch(self::$config->folders['dom_elements'] . $params['type'] . '/index.tpl');
        }
        return $theme;
    }

    private function generateDomName($params,$relation_table=false,$id=null){
        $validate =$params['validate']?$params['validate']:'optional';
        $modifier = $params['modifier']?$params['modifier']:'none';

        if($relation_table){
            $validate = 'optional';
            $name = "{$relation_table}[{$id}][{$params['name']}__{$validate}__{$modifier}][".self::$count."]";
        }else{
            $name = "{$params['name']}__{$validate}__{$modifier}";
        }
        return $name;
    }

    /**
     * Parametreleri string içerisinden çekip alır.
     * text#id.name|name__validate__modifier[description|helptext][values][selectedvalue]
     * @param $params
     */
    private function parseParams($params)
    {

        //tür#elemanid.stilsinifi|sütun_adı__zorunlulu__modifier[açıklama|yardımmetni][seçenekleryadatabloayarları][seçilideğer][dosyaayarı]
        $regexp = "~([^\#]+)#([^\.]+)\.([^\|]+)\|([^\[]+)\[\[?([^\|]+)?\|?([^\]]+)?\]?\[?([^\]]+)?\]?\[?([^\]]+)?\]?\[?([^\]]+)?\]?\[?([^\]]+)?\]?~";
        preg_match_all($regexp, $params, $tmp);


        return self::beautifyParams($tmp);
    }

    private function convertDOMString($params, $value)
    {
        /**
         * Bu kısımda sadece değişkenler varmı yokmu kontrolü
         */
        $params->modifier=isset($params->modifier)?$params->modifier:'';
        $params->file_alias = isset($params->file_alias)?$params->file_alias:'';
        $params->where_clause=isset($params->where_clause)?$params->where_clause:'';
        $params->column_name = isset($params->column_name)?$params->column_name:'';
        $params->description = isset($params->description)?$params->description:'';
        $params->help_text = isset($params->help_text)?$params->help_text:'';

        /**
         * Bu kısımda sadece değişkenler varmı yokmu kontrolü
         */

        $type = m_statics::field_types($params->type);
        //select#type.full|type__required__none[Özelliğin hangi tür içeriğe sahip olacağı|][]
        if (is_object($value)) {
            $column_name = $params->column_name;
            $value = $value->$column_name;
        }
        $modifier = isset($params->modifier)&&!empty($params->modifier) ? m_statics::modifiers($params->modifier) : "";
        $required = $params->required == '1' ? 'required' : 'optional';
        //if is file uploader /image uploader
        $file_alias = isset($params->file_alias)&&!empty($params->file_alias)?$params->file_alias:'';
        $file_alias = utils::file_alias($file_alias, 'id');
        if ($file_alias) {
            $file_alias = $file_alias->key;
        }

        //if table data
        if ($params->table_name) {
            $params->values = "{$params->table_name}.{$params->key_row}&{$params->value_row}?{$params->where_clause}";
        }
        //tür#elemanid.stilsinifi|sütun_adı__zorunlulu__modifier[açıklama|yardımmetni][seçenekleryadatabloayarları][seçilideğer][dosyaayarı]
        $string = "{$type}#{$params->column_name}_{$params->id}.full|{$params->column_name}__{$required}__{$modifier}[{$params->description}|{$params->help_text}][{$params->values}][{$value}][{$file_alias}]";
        return $string;
    }

    /**
     * Parametreleri diziye aktarıp döndürür.
     *
     * @param $params
     *
     * @return array
     */
    private function beautifyParams($params)
    {
        $keys = array('all', 'type', 'id', 'class', 'name', 'description', 'help_text', 'value', 'selected', 'file_alias','placeholder');
        $_params = array();
        foreach ($params as $key => $value) {
            $_params[$keys[$key]] = $value[0];
        }

        preg_match('~data\{([^\}]+)\}~', $_params['value'], $static_value);
        $name = explode('__', $_params['name']);
        $_params['name'] = isset($name[0])?$name[0]:'';
        $_params['validate'] = isset($name[1])?$name[1]:'';
        $_params['modifier'] = isset($name[2])?$name[2]:'';

        return $_params;
    }

    public function loadValue($params, $value)
    {
        $type = m_statics::field_types($params->type);
        $column_name = $params->column_name;

        if (is_file(self::$config->folders['dom_elements'] . $type . '/list.tpl')) {
            $this->theme->assign('params', $params);

            if (is_file(self::$config->folders['dom_elements'] . $type . '/index.php')) {
                require_once(self::$config->folders['dom_elements'] . $type . '/index.php');
                $class_name = $type . '_dom';
                $class = $class_name::singleton($this->theme);
                $value = $class->getValue($params, $value);
            } else {
                $value = "";
            }
            $this->theme->assign('value', $value);

            $theme = $this->theme->fetch(self::$config->folders['dom_elements'] . $type . '/list.tpl');
            return $theme;
        }
        return null;
    }

    public function loadDomFromRelation($data,$value=0){
        //tür#elemanid.stilsinifi|sütun_adı__zorunlulu__modifier[açıklama|yardımmetni][seçenekleryadatabloayarları][seçilideğer][dosyaayarı]
        if(!empty($value)){
            $model = model::singleton();
            $x = $model->getByMaster_Module_Data_Id($value,'slave_module_data_id',$data['info']->table_name);
            $v = array();
            foreach($x as $key=>$value){
                $v[]=$value->slave_module_data_id;
            }
            $value = json_encode($v);
        }

        $db = db::singleton();

        $label_keys = array('label','title','name'); // sütun adı bunlardan biri olsunlar mı ? @todo hatalı burası
        $label_key = '';
        foreach($label_keys as $key){
            if($db->check_column($key,$data['info']->module_table_name)){
                $label_key = $key;
                break;
            }
        }
        $string = "multi_select#many_to_many_relation.full|{$data['info']->module_table_name}[|][{$data['info']->module_table_name}.{$label_key}&id][{$value}][]";
        $dom = $this->loadDOM($data['info']->module_name,$string);
        return $dom;
    }
}
