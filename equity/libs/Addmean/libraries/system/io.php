<?php
use Core\Config;
class IO {

    /**
     * $_GET değişkenindeki $var verisini döndürür. Güvenlik seviyesi config dosyasından veya 
     * $raw değeri ile ayarlanabilir. 
     * @param string $var
     * @param boolean $raw
     * @return string 
     */
    public static function get($var, $raw=false) {
        $config = Config::init();
        $get = isset($_GET[$var])?$_GET[$var]:'';

        if ($raw) {
            return $get;
        }

        if ($config->system['iosecure']) {

            if(is_array($get)){
                return array_map("utils::filter",$get);
            }

            return utils::filter($get);

        }
        return $get;
    }

    /**
     * $_POST değişkenindeki $var verisini döndürür. Güvenlik seviyesi config dosyasından veya 
     * $raw değeri ile ayarlanabilir. 
     * @param string $var
     * @param boolean $raw
     * @return string 
     */
    public static function post($var, $raw=false) {
        $config = Config::init();

        $post = isset($_POST[$var])?$_POST[$var]:'';
        if ($raw) {
            return $post;
        }

        if ($config->system['iosecure']) {

            if(is_array($post)){

                return array_map("utils::filter",$post);
            }

            return htmlspecialchars(strip_tags(($post)));
        }
        return $post;
    }

    /**
     * Adres satırından gelen veriyi parçalar. Controller,action ve fonksiyon parametlerini döndürür.
     * @return array 
     */
    public static function getUri($uri=null) {

        $uri = $uri==null?'/'.$_GET['c']:$uri;
        $uri = explode('/', $uri);
        if (!isset($uri[1]) || $uri[1] == '') {
            return null;
        }
        if($uri[1]=='root'){
            //root app uri getter
            $root_uri = array();
            foreach($uri as $u){
                if($u!='root'){
                    $root_uri[]=$u;
                }
            }
            $root_uri = join('/',$root_uri);
            if($root_uri=='') { $root_uri = '/'; }
            return self::getUri($root_uri);
        }
        $d = array();
        if (preg_match('~\.~', $uri[1])) {
            $tmp = array(0 => '');
            unset($uri[1]);
            $i = 1;
            foreach ($uri as $key => $val) {
                if ($key) {
                    $tmp[$i] = $uri[$key];
                    $i++;
                }
            }
            $uri = $tmp;
        }
        $d['controller'] = isset($uri[1])?$uri[1]:'index';
        $d['action'] = 'init';
        if (count($uri) > 2) {
            $d['action'] = $uri[2];
        }

        $d['params'] = array();
        if (count($uri) > 2) {
            for ($i = 3; $i < count($uri); $i++) {
                $d['params'][] = $uri[$i];
            }
        }
        return $d;
    }

    public static function isPost(){
        if($_POST){
            return true;
        }
        return false;
    }

    /**
     * @description Secure cookie get/set method
     * @param string $var  Cookie name
     * @param string $value  Cookie value
     * @param bool $raw  Unsecure return
     * @return array|string
     */
    public static function cookie($var,$value='',$raw=false){
        $config = Config::init();
        $cookie = isset($_COOKIE[$var])?$_COOKIE[$var]:'';
        if($raw){
            return $cookie;
        }
        if($config->system['iosecure']){
            if(is_array($cookie)){
                $cookie = array_map("utils::filter",$cookie);
            }else{
                $cookie = htmlspecialchars(strip_tags($cookie));
            }
        }

        if(!empty($value)){ //zero is empty
            setcookie($var,$value);
            $cookie = $value;
        }
        return $cookie;

    }
}