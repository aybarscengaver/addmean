<?php
/**
 * User: delirehberi
 * Date: 07.01.2013
 * Time: 20:32
 * Filename: authorize.php
 * Description:
 * Usage:
 */
class authorize{
    private $db,$user,$page_info;
    public $login_url,$controller,$action;
    public $exceptions,$reverse_exceptions,$level,$application;
    public function __construct($application){
        $this->db = DB::singleton();
        $this->application = $application;
        $this->user = $this->getUserInfo();
    }

    public function check_autorize(){
        $enter = true;
        if(!$this->user || $this->level!=$this->user->level ){
            $enter = false;
            if(isset($this->exceptions)&&!empty($this->exceptions)){
                foreach($this->exceptions as $key=>$value){
                    if($key==$this->controller&&$value==$this->action){
                        //giriş sayfası gibi exceptionlar burada tutuluyor.
                        //burası serbest
                        $enter = true;
                    }
                }
            }elseif(isset($this->reverse_exceptions)&&!empty($this->reverse_exceptions)){
                $enter = true;
                foreach($this->reverse_exceptions as $key=>$values){
                    if($key==$this->controller){
                        foreach($values as $k=>$v){
                            if($v==$this->action){
                                //burası yasak
                                $enter = false;
                            }
                        }
                    }
                }
            }
        }
        return $enter;
    }

    private function getUserInfo(){
        if($this->application=='root'){
          $user = isset($_SESSION['admin_user'])
              ?$_SESSION['admin_user']
              :null;

        }else{
            $user = isset($_SESSION['user'])
                ?$_SESSION['user']
                :null;
        }
            return $user;
    }

    public function checkIn(){
        $request_uri = $_SERVER['REQUEST_URI'];
        if(!$this->check_autorize()){
            $back_url = $_SERVER['REQUEST_URI'];
            header('Location: '.$this->login_url."?return={$back_url}");
        }
    }
}