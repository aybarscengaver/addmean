<?php
/**
 * User: emre
 * Date: 16.06.2012
 * Time: 00:09
 * Company: Deli
 */
/**
 * Pluginleri sisteme dahil etmek için kullanılır
 * @param $params
 * @param $smarty
 *
 * @return string
 */
function smarty_function_plugins($params,&$smarty){
    $plugins = theme::getPlugins($params['layout']);
    /**
     * Text olarak $data değişkenine html komutları yazılacak
     */
    $data = '';
    /**
     * Javascript dosyaları tek bir script tagi içinde çağrılabilmek için
     * dizi değişkende tutulacak.
     */
    $javascript_files = array();
    if(isset($plugins['defaults'])){
        foreach((array)$plugins['defaults'] as $plug){
            $plugin_files = theme::getPluginFiles($plug,$params['layout']);
            if($plugin_files['scripts']){
                foreach((array)$plugin_files['scripts'] as $s){
                    $javascript_files[] = $s;
                }
            }
            /**
             * Stil dosyaları tek tek çağrılmak zorunda olduğundan
             * diziye aktarılmayacak.
             */
            if($plugin_files['styles']){
                foreach((array)$plugin_files['styles'] as $s){
                    $data.="\t<link rel='stylesheet' type='text/css' href='{$s}' charset='utf-8' media='all'/>\n";
                }
            }
            $data.="\n";
        }
    }


    if(isset($plugins['extends'])){
        foreach((array)$plugins['extends'] as $plug){
            $plugin_files = theme::getPluginFiles($plug,$params['layout']);
            if($plugin_files['scripts']){
                foreach((array)$plugin_files['scripts'] as $s){
                    $javascript_files[] = $s;
                }
            }
            if($plugin_files['styles']){
                foreach((array)$plugin_files['styles'] as $s){
                    $data.="\t<link rel='stylesheet' type='text/css' href='{$s}' charset='utf-8' media='all'/>\n";
                }
            }
            $data.="\n";
        }
    }


    /**
     * Tüm javascriptleri tek bir tag içerisinde yazdıracağız.
     */
    $data.="<script type='text/javascript'>";
    foreach($javascript_files as $s){
        $data.="\thead.js('{$s}');\n";
    }
    $data.="</script>";

    return $data;
}