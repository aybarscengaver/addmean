<?php
function smarty_modifier_get_value($value,$table,$column,$value_column){
    $db =DB::singleton();
    $db->table=$table;
    $db->where = "{$column}='{$value}'";
    $db->limit = 1;
    $db->rows = ($value_column);
    $data = $db->read();
    return $data->$value_column;
}