<?php
/**
 * @desc Youtube video bağlantıları için önizleme resmi döndür.
 * @param $video
 * @return string
 * @usage <img src="{$image|youtube_preview}" onerror="this.src='/video.jpg'"/>
 */
function smarty_modifier_youtube_fix($video){
    $regex1 = "~http\:\/\/www\.youtube\.com\/watch\?v\=([^\&]+)~";
    $regex2 = "~http\:\/\/www\.youtube\.com\/embed\/([^\/]+)~";

    $v = "http://www.youtube.com/embed/#video#";
    preg_match_all($regex1,$video,$tmp);
    if(isset($tmp[1]) && isset($tmp[1][0])){
        $image =str_replace("#video#",$tmp[1][0],$v);
    } else{
        return $video;
    }
    return $v;
}