<?php
/**
 * @desc Resim tanımına göre resim boyutlarını döndürür.
 * @param $file_alias
 * @return string
 */
function smarty_modifier_get_sizes($file_alias){

    $file_alias = utils::file_alias($file_alias);
    if(!$file_alias)
        return "?x?";
    $sizes = "{$file_alias->image_height}x{$file_alias->image_width}";

    return $sizes;
}