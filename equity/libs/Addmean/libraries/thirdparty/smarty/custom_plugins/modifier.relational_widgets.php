<?php
function smarty_modifier_relational_widgets($module,$content,$application){

    /**
     * Modülle ilişkisi bulunan tüm modüllerin widgetları varsa çekeceğiz.
     */
    //Önce ilişkileri bir alalım hacım :

    $model = model::singleton();
    $widget = widget::singleton();

    $relations = $model->get_relations($module->table_name,$content->id);
    $mtm = isset($relations['many_to_many'])?$relations['many_to_many']:array();
    $otm = isset($relations['one_to_many'])?$relations['one_to_many']:array();
    /**
     * her iki türü de bi güzel döndürek
     */
    $_widgets = array();
    if(count($mtm)>0){
        //burada birşeyler varmış :|\
        foreach($mtm as $rel){
            /**
             * Gelen ilişkiye göre gidip widget çek.
             */
            $widgets = $widget->loadRelational($rel,$application);
            foreach($widgets as $w){
                $_widgets[] = widget::load($w->directory_name,$application,$rel['datas']);
            }
        }
    }
    if(count($otm)>0){
        //burada da birşeyler varmış :|
        /**
         * One To Many relational olanların bir widget olacağı konusu biraz karışık burası şimdilik duracak.
         */
    }
    return join('<!--widget-->',$_widgets);
}