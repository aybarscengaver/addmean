<?php
function smarty_function_styles($params, &$smarty)
{
    $layout  = $params['layout'];
    $files   = theme::getStyles($layout);
    $styles = "";
    /**
     * Defaults
     */
    foreach((array)$files['defaults'] as $s){
        $styles .= "\t<link rel='stylesheet' type='text/css' media='all' href='". theme::$config->folders['public_dir'].theme::$config->folders['resource_assets'] ."styles/{$s}'/>\n";
    }
    /**
     * Extends
     */
    foreach((array)$files['extends'] as $s){
        $styles .= "\t<link rel='stylesheet' type='text/css' media='all' href='".theme::$config->folders['public_dir'].theme::$config->folders['extends_assets'].$layout."/styles/{$s}'/>\n";
    }
    return $styles;
}