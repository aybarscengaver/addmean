<?php
/**
 * @param $label
 * @param $params
 * @param bool $string
 * @return string
 */
function smarty_modifier_many_to_many($relational_object,$selected=0){
    /**
     * Form elemanının multi select türüne gönder
     */
    $form = form::singleton();
    $dom=$form->loadDomFromRelation($relational_object,$selected);
    return $dom;
}