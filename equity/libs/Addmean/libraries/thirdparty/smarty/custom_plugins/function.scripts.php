<?php
function smarty_function_scripts( $params, &$smarty ) {
    $layout = $params['layout'];
    $files = theme::getScripts( $layout );
    $scripts = "<script type='text/javascript'>";
    /**
     * Defaults
     */
    foreach ( $files['defaults'] as $s ) {
        $scripts .= "\thead.js('" . theme::$config->folders['public_dir'].theme::$config->folders['resource_assets'] . "javascripts/{$s}');\n";
    }
    /**
     * Extends
     */
    foreach ( $files['extends'] as $s ) {
        $scripts .= "\thead.js('" . theme::$config->folders['public_dir'].theme::$config->folders['extends_assets'] . $layout . "/javascripts/{$s}');\n";
    }
    return $scripts . '</script>';
}