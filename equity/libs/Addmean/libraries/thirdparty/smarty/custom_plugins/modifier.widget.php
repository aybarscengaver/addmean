<?php
function smarty_modifier_widget($widget,$layout='default',$param=null){
    $widget = widget::load($widget,$layout,$param);
    $before = <<<BEFORE
<div class="__widget">
BEFORE;
    $after = <<<AFTER
</div>
AFTER;
    return $before.$widget.$after;
}