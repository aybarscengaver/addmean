<?php
/**
 * User: emre
 * Date: 16.06.2012
 * Time: 00:09
 * Company: Deli
 */
/**
 * Pluginleri sisteme dahil etmek için kullanılır
 * @param $params
 * @param $smarty
 *
 * @return string
 */
function smarty_function_path($params,&$smarty){
    $m_path = new m_path();
    $path = $m_path->get_path($params['key'],isset($params['original'])?$params['original']:false);
    return $path;
}