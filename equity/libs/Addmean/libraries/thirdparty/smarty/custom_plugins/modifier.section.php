<?php
/**
 * @param $label
 * @param $params
 * @param bool $string
 * @return string
 */
function smarty_modifier_section($label=null,$params=null,$string=false,$related_data_id=null){
    $module_info = get_module_info();
    $forms = form::singleton($module_info);
    /**
     * Eğer değer gösterilecekse $params içerisinde sütun özellikleri gelir
     * $label içerisinde tablodan çekilen veriler gelir.
     */


    if($string!==true){
        /**
         * Burada DOM olarak yüklenecek
         */
        $dom = $forms->loadDOM($label,$params,$string,$related_data_id);
    }elseif(is_bool($string)&&$string===true){
        /**
         * Burada sadece değer yazılacak
         */
        $dom = $forms->loadValue($label,$params);
    }
    return $dom;
}
function get_module_info(){
    $url = $_GET['c'];
    $params = explode('/',$url);
    if($params[0]!='root'&&$params[0]!='default'){
        $module = array(
            'application'=>'default',
            'table_name'=>$params[0],
            'action' => $params[1],
        );
    }else{
        $module = array(
            'application'=>$params[0],
            'table_name'=>$params[1],
            'action'=>$params[2],
        );
    }
    return $module;
}