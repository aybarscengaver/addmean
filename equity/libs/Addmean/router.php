<?php
namespace Addmean;
class Router{
    public function __construct(){
        try{
            $c = $_GET['c'];
            $c = explode('/',$c);

            if($c[0]!='root' && $c[0]!='ajax.php'){
                return $this->defaultApp();
            }elseif($this->isAjax()||$this->isFlash()||$this->isMultipart()){
                return $this->ajaxApp();
            }
            else{
                $m = $c[0].'App';
                return $this->$m();
            }
        }catch(AjaxCatcher $e) { //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->show();
            }else{
                echo $e->lite();
            }
        }
        catch(Catcher $e) { //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->show();
            }else{
                echo $e->lite();
            }
        }
    }

    public function isAjax(){
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])&&strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest';
    }

    public function isFlash(){
        return isset($_SERVER['HTTP_USER_AGENT'])&&strtolower($_SERVER['HTTP_USER_AGENT'])=='shockwave flash';
    }

    public function isMultipart(){
        return isset($_SERVER['CONTENT_TYPE'])&&strstr(strtolower($_SERVER['CONTENT_TYPE']),'multipart');
    }
    public function defaultApp(){
        try {
            //yapıyı kur
            $c = \Core::singleton('default');
        } catch (Catcher $e) { //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->show();
            }else{
                echo $e->lite();
            }
        }  catch(SmartyException $e){
            //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->getMessage();
            }else{
                echo $e->getMessage();
            }
        }
    }
    public function rootApp(){
        try {
            //yapıyı kur
            $c = \Core::singleton('root');
        } catch (Catcher $e) { //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->show();
            }else{
                echo $e->lite();
            }
        }  catch(SmartyException $e){
            //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->getMessage();
            }else{
                echo $e->getMessage();
            }
        }
    }

    public function ajaxApp(){
        try {
            $ajaxCheck = isset($_SERVER['HTTP_X_REQUESTED_WITH'])&&strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest';;
            $flashCheck = isset($_SERVER['HTTP_USER_AGENT'])&&strtolower($_SERVER['HTTP_USER_AGENT'])=='shockwave flash';
            $fileCheck = isset($_SERVER['CONTENT_TYPE'])&&strstr(strtolower($_SERVER['CONTENT_TYPE']),'multipart');
            if(!$ajaxCheck&&!$flashCheck&&!$fileCheck){
                throw new AjaxCatcher("Yanlızca ajax bağlantı kabul edilir.");
            }
            $data = $_GET;
            $c = \Core::ajax($data);
        }catch(AjaxCatcher $e) { //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->show();
            }else{
                echo $e->lite();
            }
        }
        catch(Catcher $e) { //olmazsa
            //temizle
            ob_clean();
            //charset
            @header("content-type:text/html;charset=" . charset);
            //çıkart
            if(devel===true){
                echo $e->show();
            }else{
                echo $e->lite();
            }
        }
    }
}