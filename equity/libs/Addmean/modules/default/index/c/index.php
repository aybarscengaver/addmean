<?php
class index extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function indexAction()
    {
        parent::display('index');
    }

}
