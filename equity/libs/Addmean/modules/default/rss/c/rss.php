<?php
/**
 * Module Name : press
 * Create Date : 03/09/2013
 * Package : AddMean
 *
 */
    class rss extends controller{
        public function indexAction(){
            header("Content-Type: text/xml; charset=UTF-8");
            $mproducts = m_products::singleton();
            $products = $mproducts->get();
            $rssfeed = '<?xml version="1.0" encoding="UTF-8" ?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/" version="2.0">'."\n";
            $rssfeed .= '<channel>'."\n";
            $rssfeed .= '<title>Addmean</title>'."\n";
            $rssfeed .= '<link>http://www.addmean.com</link>'."\n";
            $rssfeed .= '<description></description>'."\n";
            $rssfeed .= '<language>tr-tr</language>'."\n";
            $rssfeed .= '<copyright>Copyright (C) 2013 addmean.com</copyright>'."\n";
            foreach($products as $p){
                $rssfeed.="<item>"."\n";
                $rssfeed.="<title>{$p->title}</title>"."\n";
                $rssfeed.="<description>&lt;img src='".core::$config->site_url.m_path::get_path('product_image_list').$p->image."'/&gt;</description>";
                $rssfeed.="<link>".core::$config->site_url."/{$p->seo}</link>"."\n";
                $rssfeed.="</item>"."\n";
            }
            $rssfeed.="</channel>"."\n";
            $rssfeed.="</rss>"."\n";
            echo $rssfeed;exit;

        }
    }