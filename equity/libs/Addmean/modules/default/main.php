<?php
/**
 * User: aybarscengaver
 * Date: 16.04.2013
 * Time: 22:35
 * File: main.php
 * Package: AddMean
 */

class mainController{
    public function __construct(&$parent){
        $parent->module_info  =$parent->model->get_module_info();
        $parent->assign('module_info',$parent->module_info);
        $url = $_SERVER['REQUEST_URI'];
        $parent->assign('active_page_url',$url); //maybe wrong :\ @todo
        $language_key = \Core\Languages::get_language_key();
        $parent->assign('language_key',$language_key);
    }
}