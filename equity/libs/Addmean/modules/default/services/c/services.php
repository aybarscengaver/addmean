<?php
/**
 * User: mint
 * Date: 7/12/13
 * Time: 1:11 PM
 * File: services.php
 * Package: acsplus
 */
ini_set("soap.wsdl_cache_enabled", "0");

class services extends controller{
    public function clientAction($action){
        $client = new SoapClient(null,array(
            'uri'=>'http://acsplus.int',
            'location'=>'http://acsplus.int/api.php',
        ));
        $vars = func_get_args();
        unset($vars[0]);
        $params = $vars;
        echo json_encode($client->$action($params));
    }
}