<?php
/**
 * Created by JetBrains PhpStorm.
 * User: delirehberi
 * Date: 27.02.2013
 * Time: 19:34
 * To change this template use File | Settings | File Templates.
 */
class widgets extends Controller{
    public function listAction(){
        $widgets = $this->model->get();
        $this->assign('data',$widgets);
        $this->display();
    }
    public function createAction() {
        $this->display();
    }
    public function editAction($id){
        $widget = $this->model->get('*',"id='{$id}'",0,1);
        $this->assign('data',$widget);
        $this->display();
    }
}