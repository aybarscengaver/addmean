<?php
/**
 * User: delirehberi
 * Date: 09.01.2013
 * Time: 11:35
 * Filename: settings.php
 * Description:
 * Usage:
 */
class settings extends Controller
{
    public function listAction()
    {
        $data = $this->model->get();
        $this->assign('data', $data);
        $this->display();
    }

    public function createAction()
    {
        $this->display();
    }

    public function editAction($id)
    {
        $data = $this->model->get('*', "id='{$id}'", 0, 1);
        $this->assign('data', $data);
        $this->display();
    }

    public function parserAction(){
        $vars = func_get_args();
        $file = join('/',$vars);
        $editable_files = array('.htaccess');
        if(!in_array($file,$editable_files)){
            //Bu dosya izinsiz
            throw new Catcher("Bu dosyaya erişim izniniz bulunmamaktadır.");
        }
        $this->assign('file',$file);
        $this->display();
    }


}