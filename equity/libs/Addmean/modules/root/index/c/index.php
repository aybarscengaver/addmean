<?php
/**
 * Created by Emre YILMAZ [a.k.a. wyrus]
 * Date: 24.03.2012
 * Time: 14:13
 * Company: Deli Bilişim
 */
class index extends Controller
{
    public function __construct() {
        parent::__construct();
    }
    public function indexAction(){
        parent::display();
    }
}
