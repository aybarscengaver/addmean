<?php
/**
 * User: wyrus
 * Date: 7/24/12
 * Time: 2:26 AM
 * Company: Deli
 * Description: Modül yapısı
 */
class m_modules extends model
{
    protected $_table_name = "modules";
    protected $_fields_table_name = "module_fields";

    /**
     * @desc Dil olayına karşı bir hacktir kendileri.
     * @param string $rows
     * @param null $where
     * @param null $page_no
     * @param null $limit
     * @param null $order_by
     * @param null $table_name
     * @param null $join
     * @param null $group_by
     * @return array
     */
    public function get($rows = "*", $where = null, $page_no = null, $limit = null, $order_by = null, $table_name = null, $join = null, $group_by = null){
        $table_name = $table_name?$table_name:$this->_original_table_name;
        return parent::get($rows,$where,$page_no,$limit,$order_by,$table_name,$join,$group_by);
    }
    /**
     * @description Tüm modülleri döndürür.
     * @return object
     */
    public function list_data()
    {
        $data = $this->get("*", null,  null, null, null);
        return $data;
    }


    /**
     * @description Modülün genel bilgilerini döndürür.
     * @param $id
     * @return object
     * @throws Catcher
     */
    public function get_module($id)
    {
        if (!isset($id) && empty($id)) {
            throw new Catcher("Module not found!");
        }
        $data = $this->get('*', "id='{$id}'");
        if(!$data){
            return false;
        }
        return $data[0];
    }

    /**
     * @description Modül için oluşturulan özelliğin kontrol, kayıt, tabloda sütun açma işlemlerini yapar.
     * @param $rows
     * @return mixed
     */
    public function create_field($rows)
    {
        $this->check_column($rows);
        $table_name = $rows['module']->table_name;
        $multi_language = $rows['module']->multi_language;
        unset($rows['module']);

        $this->create_column($rows, $table_name);

        if(!empty($multi_language)){
            $langs = json_decode($multi_language);
            $language = new \Core\Languages($this);
            foreach($langs as $k=>$v){
                $table_suffix = $language->getTableSuffix($v);
                $this->create_column($rows, $table_name.$table_suffix);
            }
        }

        self::$db->rows = $rows;
        self::$db->table = $this->_fields_table_name;
        $insert = self::$db->insert();
        return $insert;
    }

    /**
     * @description Modül özelliğini güncellemek için bu kısmı kullanıyor.
     * @param $rows
     * @param $id
     */
    public function edit_field($rows, $id)
    {
        //@todo Sütun adı değiştirilirken var mı yok mu diye kontrol edilecek.

        $table_name = $rows['module']->table_name;
        $multi_language = $rows['module']->multi_language;
        unset($rows['module']);

        ///get old data for modify column on sql
        $old_data = $this->get_field($id);

        $this->update_column($rows, $old_data, $table_name);
        if(!empty($multi_language)){
            $langs = json_decode($multi_language);
            $language = new \Core\Languages($this);
            foreach($langs as $k=>$v){
                $table_suffix = $language->getTableSuffix($v);
                $this->update_column($rows,$old_data, $table_name.$table_suffix);
            }
        }

        //update field data
        self::$db->rows = $rows;
        self::$db->table = $this->_fields_table_name;
        self::$db->where = "id='{$id}'";
        $update = self::$db->update();
    }

    /**
     * @description Modül için oluşturulmak istenen özelliğin daha önce eklenip eklenmediğini sorgular.
     * @param $column
     * @throws AjaxCatcher
     */
    private function check_column($column)
    {
        $table_name = $column['module']->table_name;
        $data = self::$db->query("SHOW COLUMNS FROM `{$table_name}`", PDO::FETCH_OBJ)->fetchAll();
        foreach ($data as $key => $value) {
            if ($value->Field == $column['column_name']) {
                throw new AjaxCatcher("Bu isimde bir sütun mevcut! Lütfen kontrol edip tekrar deneyiniz.");
            }
        }
    }

    /**
     * @description Modül özelliği güncellenirken modülün tablosundaki sütunuda buradan güncelliyor.
     * @param $data
     * @param $old_data
     * @param $table_name
     */
    private function update_column($data, $old_data, $table_name)
    {
        $type = m_statics::column_types_sql($data['type']);
        $sql = <<<SQL
        ALTER TABLE `{$table_name}`
        CHANGE  `{$old_data->column_name}` `{$data['column_name']}` {$type}
SQL;
        $query = self::$db->query($sql);
    }

    /**
     * @description Oluşturulan bir modül özelliğini tablosuna dahil eder.
     * @param $column
     * @param $table_name
     */
    private function create_column($column, $table_name)
    {
        $type = m_statics::column_types_sql($column['type']);
        $sql = <<<SQL
ALTER TABLE `{$table_name}`
    ADD COLUMN `{$column['column_name']}` {$type} NULL AFTER `id`;
SQL;
        $query = self::$db->query($sql);
    }

    /**
     * @description Bir modülü silme işlemlerinin tümü burada hallediliyor.
     * @param $id
     * @param null $table
     * @return int|void
     */
    public function delete($id, $table=null)
    {
        $args = func_get_args();
        if(count($args)>2){
            $module=$args[2];
        }
        if(!isset($module->table_name))
            return false;
        parent::drop_table($module->table_name);
        //$this->drop_folders($module['table_name']);  @todo dosyaları silme işlemini daha sonra halledecez
        parent::delete($id, $table);
    }

    /**
     * @description Tek bir özelliği veritabanından almak için kullanılıyor. Düzenleme sayfalarında kullanıyorum.
     * @param $id
     * @return null
     */
    public function get_field($id)
    {
        self::$db->reset();
        self::$db->table = $this->_fields_table_name;
        self::$db->where = "id='{$id}'";
        $data = self::$db->read();
        if ($data) {
            return $data[0];
        }
        return null;
    }

    /**
     * @description Bir özelliği modülden ve tablosundan kaldırma işlemleri burada yapılıyor.
     * @param $id
     * @param $table
     * @param $field
     */
    public function delete_field($id, $table, $field)
    {
        $column_name = $field->column_name;
        $module_table = $this->get_module_info($field->module_id);
        $multi_language = $module_table->multi_language;

        parent::drop_column($module_table->table_name, $column_name);

        if(!empty($multi_language)){
            $langs = json_decode($multi_language);
            $language = new \Core\Languages($this);
            foreach($langs as $k=>$v){
                $table_suffix = $language->getTableSuffix($v);
                parent::drop_column($module_table->table_name.$table_suffix, $column_name);
            }
        }

        parent::delete($id, $table);
    }


    public function syncCheck($destination,$target){
        //show tables on origin
        $destination_columns = self::$db->query("SHOW COLUMNS FROM {$destination}")->fetchAll(PDO::FETCH_ASSOC);
        $target_columns = self::$db->query("SHOW COLUMNS FROM {$target}")->fetchAll(PDO::FETCH_ASSOC);
        foreach($destination_columns as $key=>$value){
            $x = false;
            foreach($target_columns as $t_key=>$t_value){
                if($t_value['Field']===$value['Field']){
                    $x=true;
                }
            }
            if(!$x){
                preg_match_all("~^([^\(]+)\(?([^\)]+)?\)?~",$value['Type'],$tmp);
                $type = $tmp[1][0];
                $length = isset($tmp[2])?$tmp[2][0]:'';
                self::$db->table = $target;
                self::$db->add_column($value['Field'],$type,$length);
            }
        }
    }
    /**
     * @description Modül özelliklerini sıralama işleminde kullanılır.
     * @param $data
     * @param $module_id
     */
    public function sort_fields($data, $module_id)
    {
        self::$db->table = $this->_fields_table_name;
        foreach ((array)$data as $order_no => $id) {
            self::$db->where = "id='{$id}' AND module_id='{$module_id}'";
            self::$db->rows = array('order_no' => $order_no);
            self::$db->update();
        }
    }

    public function sort_module_datas($data,$module_id){
        $module = $this->get_module($module_id);
        self::$db->table = $module->table_name;
        foreach((array)$data as $order_no => $id){
            self::$db->where = "id='{$id}'";
            self::$db->rows = array('order_no'=>$order_no);
            self::$db->update();
        }
    }
}
