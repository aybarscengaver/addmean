<?php
/**
 * User: wyrus
 * Date: 7/24/12
 * Time: 2:26 AM
 * Company: Deli
 */
class modules extends Controller
{
    public function createAction()
    {
        self::display();
    }

    public function editAction($id){
        $module = $this->model->get_module($id);
        $this->assign('module',$module);
        self::display();
    }



    /**
     *
     */
    public function listAction($page_no=0)
    {
        $data = $this->model->list_data();
        $this->assign('data', $data);
        $this->display();
    }

    public function fieldsAction($module_id)
    {
        $data = $this->model->list_fields($module_id);
        $this->assign('data', $data);
        $module = $this->model->get_module($module_id);
        $this->assign('module', $module);
        $this->display();
    }

    public function create_fieldAction($module_id)
    {

        $module = $this->model->get_module($module_id);
        $this->assign('module', $module);
        $this->display();

    }

    public function edit_fieldAction($field_id)
    {
        $field = $this->model->get_field($field_id);
        $module = $this->model->get_module($field->module_id);
        $this->assign('field', $field);
        $this->assign('module',$module);
        $this->display();
    }

    public function exportAction($module_id){
        $module =  $this->model->get_module($module_id);
        $fields = $this->model->list_fields($module->id);

        $data = array();
        $data['info'] = $module;
        unset($data['info']->id);

        $data['fields'] = array();

        /**
         * Modülün özellikleri
         */
        foreach($fields as $key=>$value){
            $field = array();
            foreach($value as $k=>$v){
                if($k!='id' && $k!='module_id'){
                    $field[$k]=$v;
                }
            }
            $data['fields'][]=$field;
        }

        /**
         * Modülün tablo yapısı
         */
        $table_rows = $this->db->show_columns($module->table_name);
        $data['table'] = array();
        foreach($table_rows as $key=>$value){
            $column = array();
            foreach($value as $k=>$v){
                $column[$k]=$v;
            }
            $data['table'][]=$column;
        }

        /**
         * İlişkiler
         */
        $data['relations']=array();//coming soon
        header("Content-type:application/json;charset=utf-8");
        echo json_encode($data);
    }

}
