<?php
/**
 * User: wyrus
 * Date: 7/24/12
 * Time: 2:26 AM
 * Company: Deli
 */
class modules extends ajax
{
    protected $_table_name = 'modules';

    public function createAjax($msg="Başarıyla eklendi.")
    {
        $rows = $this->crud->post;
        try {
            $this->createTable($rows['table_name']);
            $this->createModuleFiles($rows['table_name']);
            $this->createModule($rows);
            $this->result('Modül başarıyla oluşturuldu', true);
        } catch (AjaxCatcher $e) {
            $this->result($e->getMessage(), false);
        } catch (PDOException $e) {
            $this->result($e->getMessage(), false);
        }
    }

    public function checkNewLanguage($rows){
        $multi_language = isset($rows['multi_language'])?$rows['multi_language']:null;
        if($multi_language){
            $data = array();
            foreach($multi_language as $key=>$value){
                $lang_key = $value;
                $data[$lang_key] = $rows;
                $data[$lang_key]['table_name'] = $rows['table_name'].'_'.$lang_key;
                if(!$this->db->table_exists($data[$lang_key]['table_name'])){
                    $this->createTable($data[$lang_key]['table_name']);
                    $this->checkNewOrder($data[$lang_key]);
                    $this->model->syncCheck($rows['table_name'],$data[$lang_key]['table_name']);
                }
            }
        }
    }
    public function after_createAjax(){
        try{
            $rows = $this->crud->post;
            $this->checkNewOrder($rows);
            $this->checkNewLanguage($rows);

        }catch(Exception $e){
            $this->result($e->getMessage(),false);
        }
    }
    public function checkNewOrder($rows=null){
        if(isset($rows['order'])){
            $this->db->table=$rows['table_name'];
            $this->db->add_column('order_no','float');
        }
    }

    public function editAjax($id)
    {
        $rows = $this->crud->post;
        if(isset($rows['order'])){
            $order=TRUE;
            $rows['order']=1;
        }else{
            $order=FALSE;
            $rows['order']=0;
        }

        if(!isset($rows['multi_language'])){
            $rows['multi_language']='';
        }

        $this->model->edit($rows, $id);
        $this->result('İçerik başarıyla düzenlendi.', true);
    }

    public function before_editAjax($id)
    {
        $module = $this->model->get_module($id);
        utils::check_all_seo($module);
        parent::before_editAjax($id);
    }

    public function after_editAjax($id)
    {
        $this->model->check_table_name($this->crud->old[$this->_table_name][$id], $this->crud->post);
        $this->model->check_order_column($this->crud->old[$this->_table_name][$id],$this->crud->post);
        $this->checkNewLanguage($this->crud->post);
    }

    /**
     * @todo Sıralama için bir alan eklenecek.
     * @param $table_name
     */
    protected function createTable($table_name)
    {
        $sql = <<<SQL
CREATE TABLE `{$table_name}` (
`id`  int(10) NULL AUTO_INCREMENT ,
`seo`  varchar(255) NOT NULL,
PRIMARY KEY (`id`)
)
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
;
SQL;
        $this->db->query($sql);
    }

    /**
     * @description Modülü veritabanına kaydeder.
     * @param $rows
     */
    protected function createModule($rows)
    {
        $this->db->table = $this->_table_name;
        $this->db->rows = $rows;
        $this->db->insert();
    }

    /**
     * @description Modül için oluşturulacak dosyaları tanımlar.
     * @param $alias
     */
    protected function createModuleFiles($alias)
    {
        //Default kısmına php controller dosyası oluşturulacak.
        $dirs = array();
        $dirs['modules/default/' . $alias . '/a'] = $alias . ".php";
        $dirs['modules/default/' . $alias . '/m'] = $alias . '.php';
        $dirs['modules/default/' . $alias . '/c'] = $alias . '.php';

        $dirs['modules/root/' . $alias . '/a'] = $alias . '.php';
        $dirs['modules/root/' . $alias . '/m'] = $alias . '.php';
        $dirs['modules/root/' . $alias . '/c'] = $alias . '.php';

        $dirs['templates/root/' . $alias] = 'index.tpl';
        $dirs['templates/default/' . $alias] = 'index.tpl';

        $this->createFiles($dirs);

    }

    /**
     * @description Modülün öntanımlı dosyalarını oluşturur.
     * @param $files
     * @return int
     * @throws AjaxCatcher
     */
    protected function createFiles($files)
    {
        foreach ($files as $key => $value) {
            if (!is_dir($key)) {
                mkdir(self::$config->folders['cms']['addmean'] . $key, 0777, true);
            }
            chmod(self::$config->folders['cms']['addmean'] . $key, 0777);
            $alias = explode('.', $value);
            $alias = $alias[0];
            $type = explode('/', $key);
            $length = count($type);
            $type = $type[$length-1];
            switch ($type) {
                case 'a';
                case 'c';
                case 'm';
                    $content = $this->default_contents($alias, $type);
                    break;
                default:
                    $content = $this->default_contents($alias, 'tpl');
                    break;
            }
            $file = fopen(self::$config->folders['cms']['addmean'] . $key . '/' . $value, 'w+');
            $write = fwrite($file, $content);
            if (!$write) {
                throw new AjaxCatcher("" . self::$config->folders['cms']['addmean'] . $key . '/' . $value . " oluşturulurken beklenmedik bir hata oluştu. Lütfen tekrar deneyiniz. ");
            }

        }
        return 1;
    }

    /**
     * @description Modül oluştururken oluşturulacak dosyaların öntanımlı içerikleri
     * @param $alias
     * @param $which
     * @return mixed
     */
    private function default_contents($alias, $which)
    {
        $time = date('d/m/Y');
        $content = array();
        $content['a'] = <<<AJAX
<?php
/**
 * Module Name : {$alias}
 * Create Date : {$time}
 * Package : AddMean
 *
 */
class {$alias} extends ajax{
    /**
     * Autocreated
     **/
    public function initAjax(){

    }
}
AJAX;
        $content['m'] = <<<MODEL
<?php
/**
 * Module Name : {$alias}
 * Create Date : {$time}
 * Package : AddMean
 *
 */
     class m_{$alias} extends model{
        protected \$_table_name='{$alias}';

     }

MODEL;
        $content['c'] = <<<CONTROLLER
<?php
/**
 * Module Name : {$alias}
 * Create Date : {$time}
 * Package : AddMean
 *
 */
    class {$alias} extends controller{
        public function indexAction(){
            \$this->display();
        }
    }
CONTROLLER;
        $content['tpl'] = <<<TPL
<!--{$time}-->
<!--AddMean-->
<!--AutoCreated File-->
TPL;

        return $content[$which];
    }

    public function create_fieldAjax($module_id)
    {
        $rows = $this->crud->post;
        $rows['column_name'] = utils::seo($this->crud->post('column_name'));
        $rows['list'] = $this->crud->post('list') ? 1 : 0;
        $rows['filter'] = $this->crud->post('filter') ? 1 : 0;
        $rows['edit'] = $this->crud->post('edit') ? 1 : 0;
        $rows['module'] = $this->model->get_module($module_id);
        $rows['module_id'] = $module_id;
        $rows['required'] = $this->crud->post('required') ? 1 : 0;
        $rows['frontend_form'] = $this->crud->post('frontend_form') ? 1 : 0;
        $this->model->create_field($rows);
        return $this->result("Özellik başarıyla oluşturuldu.", true);
    }

    public function edit_fieldAjax($field_id, $module_id)
    {
        $rows = $this->crud->post;
        $rows['column_name'] = utils::seo($this->crud->post('column_name'));
        $rows['list'] = $this->crud->post('list') ? 1 : 0;
        $rows['filter'] = $this->crud->post('filter') ? 1 : 0;
        $rows['edit'] = $this->crud->post('edit') ? 1 : 0;
        $rows['required'] = $this->crud->post('required') ? 1 : 0;
        $rows['frontend_form'] = $this->crud->post('frontend_form') ? 1 : 0;
        $rows['module'] = $this->model->get_module($module_id);
        $rows['module_id'] = $module_id;
        $this->model->edit_field($rows, $field_id);
        return $this->result("Özellik başarıyla güncelleştirildi.", true);
    }

    public function deleteAjax()
    {
        $model = new m_modules();
        $data = IO::post('data');
        $table = IO::post('table');
        $data = explode("&", $data);
        $datas = array();
        foreach ($data as $key => $value) {
            $x = explode('=', $value);
            $datas[$x[0]] = $x[1];
        }
        $module = $model->get_module($datas['id']);
        $model->delete($datas['id'], $table, $module);

        return $this->result("İçerik başarıyla silindi.", true);
    }

    public function delete_fieldAjax()
    {
        $model = new m_modules();
        $data = IO::post('data');
        $table = IO::post('table');
        $data = explode("&", $data);
        $datas = array();
        foreach ($data as $key => $value) {
            $x = explode('=', $value);
            $datas[$x[0]] = $x[1];
        }
        $field = $model->get_field($datas['id']);
        $model->delete_field($datas['id'], $table, $field);
        return $this->result("Özellik başarıyla silindi.", true);
    }

    public function sort_fieldsAjax($module_id)
    {
        $data = IO::post('id');
        $this->model->sort_fields($data, $module_id);
        $this->result('Sıralama başarılı', true);
    }

    public function sort_module_datasAjax($module_id){
        $data = IO::post('id');
        $this->model->sort_module_datas($data,$module_id);
        $this->result('Sıralama Başarılı',true);
    }
}
