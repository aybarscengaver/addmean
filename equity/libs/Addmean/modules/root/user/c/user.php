<?php
class user extends Controller{
    public function init(){
        $this->display('index');
    }
    public function loginAction(){
        parent::setKeywords('Giriş');
        parent::setDescription('Giriş');
        parent::setTitle('Giriş');
        $this->assign('return',IO::get('return'));
        self::viewContent('login');
    }
    public function logoutAction(){
        unset($this->session->admin_user);
        header('Location: /root');
    }
    public function homeAction(){
        echo self::display('home');
    }
    public function listAction($page_no=0){
        $data = $this->model->get();
        $this->assign('data',$data);
        $this->display();
    }
    public function editAction($id){
        if($id!=$this->session->admin_user->id){
            throw new Catcher("Suistimal");
        }
        $data = $this->model->get("*","id='{$id}'",null,1);
        $this->assign('data',$data);
        $this->display();

    }
    public function createAction(){
        $this->display();
    }
}