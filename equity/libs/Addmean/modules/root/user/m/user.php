<?php
/**
 * User: Wyrus
 * Date: 18.07.2012
 * Time: 01:01
 * Company: Deli
 * SQL:
 * CREATE TABLE `users` (
        `id`  int(10) NULL ,
        `email`  varchar(255) NOT NULL ,
        `password`  varchar(32) NULL ,
        `status`  enum('1','2','3','4') NULL ,
        `create_date`  timestamp(20) NULL DEFAULT NULL ,
    PRIMARY KEY (`id`)
    );
 * ALTER TABLE `users`
MODIFY COLUMN `id`  int(10) NOT NULL AUTO_INCREMENT FIRST ;

 */
class m_user extends Model {
    protected $_table_name='users';


    /**
     * @description Kullanıcı girişi için.
     * @param $username
     * @param $password
     * @return array
     */
    public function login( $username, $password ) {
        if ( isset($this->session->admin_user)&&!empty($this->session->admin_user) ) {
            $data = array();
            $data['msg'] = 'Giriş yapmış bir kullanıcı tekrar giriş yapamaz.';
            $data['type'] = false;
            return $data;
        }
        parent::$db->table = 'users';
        parent::$db->rows = "*";
        parent::$db->limit = 1;
        parent::$db->where = "email='{$username}' AND password=MD5('{$password}')";
        $user = parent::$db->read();
        if ( !$user ) {
            $data = array();
            $data['msg'] = "Kullanıcı adı / şifre hatalı!";
            $data['type'] = false;
        } else if ( $user->status == 1 ) {
            $data = array();
            $data['msg'] = "Hesabınız henüz etkinleştirilmemiş";
            $data['type'] = false;
        } else {
            $data = array();
            $data['msg'] = 'Giriş başarılı';
            $data['type'] = true;
            $this->session->admin_user = $user;
        }
        return $data;
    }

    /**
     * @description Veritabanında aynı emaile sahip kullanıcı olup olmadığını kontrol eder.
     * @param $email
     * @return bool
     */
    public function check_user($email){
        self::$db->where="email='{$email}'";
        self::$db->count = true;
        self::$db->table = $this->_table_name;
        $check = self::$db->read();
        self::$db->reset();
        if($check->count>0){
            return false;
        }
        return true;
    }
}
