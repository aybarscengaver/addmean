<?php
/**
 * User: emre
 * Date: 26.06.2012
 * Time: 22:28
 * Company: Deli
 */
class user extends ajax {

    public function __construct() {
        parent::__construct();
    }

    public  function loginAjax() {
        $username = IO::post( 'username' );
        $password = IO::post( 'password' );
        $m_user = new m_user();
        $data = $m_user->login( $username, $password );
        $this->result( $data['msg'], $data['type'] );
    }

    public function before_createAjax(){
        $this->crud->set("create_date",time());
        $email = $this->crud->post('email');
        $check = $this->model->check_user($email);
        if(!$check){
            throw new AjaxCatcher("Bu emaile sahip bir kullanıcı zaten mevcut!");
        }
        parent::before_createAjax();
    }

    public function before_editAjax($id){
        if($id!=$this->session->admin_user->id){
            throw new AjaxCatcher("Suistimal");
        }
        $password = $this->crud->post('password');
        if(empty($password)){
            $this->crud->remove('password');
        }
        parent::before_editAjax($id);
    }

}
