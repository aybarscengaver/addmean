<?php
/**
 * User: delirehberi
 * Date: 10.01.2013
 * Time: 23:33
 * Filename: module_relations.php
 * Description:
 * Usage:
 */
class module_relations extends Controller
{
    public function listAction($module_id=0)
    {
        $module_model = new m_modules();
        $module = $module_model->get_module($module_id);
        $data = $this->model->get('*',"master_module='{$module->table_name}'");
        $this->assign('data', $data);
        $this->assign('id', $module_id);
        $this->display();
    }

    public function createAction($module_id){
        $this->assign('id',$module_id);
        $this->assign('module',$this->model->get_module_info($module_id));
        $this->display();
    }

    public function editAction($id){
        $data = $this->model->get("*","id='{$id}'",0,1);
        $module = $this->model->get_module_info($data->master_module);
        $this->assign('module',$module);
        $this->assign('data',$data);
        $this->display();
    }
}