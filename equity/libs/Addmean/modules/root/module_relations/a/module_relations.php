<?php
/**
 * User: delirehberi
 * Date: 10.01.2013
 * Time: 23:33
 * Filename: module_relations.php
 * Description:
 * Usage:
 */
class module_relations extends ajax
{

    public function before_createAjax($id)
    {
        $table_name = $this->crud->post('table_name');
        $master_module = $this->crud->post('master_module');
        $slave_module = $this->crud->post('slave_module');
        $type = $this->crud->post('type');
        $master_module_info = $this->model->get_module_info($master_module);
        $slave_module_info = $this->model->get_module_info($slave_module);

        if ($type == 1) {
            //manytomany
            $rows = array();
            $rows['master_module_column_name'] = 'master_module_data_id';
            $rows['slave_module_column_name'] = 'slave_module_data_id';
            $this->create_table($table_name, $rows);

        } elseif ($type == 2) {
            //onetomany
            if(isset($table_name)&&!empty($table_name)){
            }else{
                $table_name=$master_module_info->table_name.'_id';
                $this->crud->set('table_name',$table_name);
            }
            $this->alter_column($slave_module_info->table_name, $table_name);
        }
    }

    /**
     * @description OneToMany ilişkilerde kullanılacak sütunu slave modülün tablosuna ekler.
     * @param $slave_table_name
     * @param $column_name
     */
    protected function alter_column($slave_table_name, $column_name)
    {
        $sql = <<<SQL
        ALTER TABLE `{$slave_table_name}` ADD COLUMN `{$column_name}` INT(10) NOT NULL
SQL;
        $this->db->query($sql);
    }

    /**
     * @description ManyToMany ilişkilerde ortak tabloyu oluşturur.
     * @param $table_name
     * @param $rows
     */
    protected function create_table($table_name, $rows)
    {
        $one_to_one_sql = <<<SQL
CREATE TABLE `{$table_name}` ( `{$rows['master_module_column_name']}`  int(10) NOT NULL ,`{$rows['slave_module_column_name']}`  int(10) NOT NULL )
SQL;
        $this->db->query($one_to_one_sql);
    }

    public function after_editAjax($id)
    {
        $this->model->check_table_name($this->crud->old['module_relations'][$id], $this->crud->post);
    }

}