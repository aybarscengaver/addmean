<?php
/**
 * User: delirehberi
 * Date: 07.01.2013
 * Time: 15:56
 * Filename: index.php
 * Description:
 * Usage:
 */
class medias extends ajax
{
    public function uploaderAjax($file_alias = 'general')
    {
        $upload = new upload('Filedata');
        $upload->_file_alias = $file_alias;
        $upload->save();
        $data = array();
        $data['filename'] = $upload->_date_archive . $upload->file_name;
        $this->result('Resim başarıyla yüklendi.', true, '', $data);
    }

    public function upload_from_fileAjax($file_alias='general'){
        $file_url = IO::post('url');
        $upload = new upload($file_url,true);
        $upload->_file_alias=$file_alias;
        $upload->save();
        $data = array();
        $data['filename'] = $upload->_date_archive . $upload->file_name;
        $this->result('Resim başarıyla yüklendi.',true,'',$data);
    }
}