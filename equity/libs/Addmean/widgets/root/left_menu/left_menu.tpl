<div id="left_menu">
    <ul id="main_menu" class="main_menu">
        <li class="limenu {if $selected_menu===false}select{/if}"><a href="/root"><span
                        class="ico gray shadow home"></span><b>Başlangıç</b></a></li>

        {foreach $modules as $key=>$value}
            {if $value->type==1}
                <li class="limenu {if $selected_menu==$value->table_name}select{/if}"><a
                            href="/root/{$value->table_name}/list">{$value->name}</a>
                </li>
            {/if}
        {/foreach}
    </ul>
</div>