<?php
class left_menu extends widget
{
    public function init(&$param=null)
    {
        $model = new m_modules();
        $modules = $model->list_data();
        $this->assign('modules',$modules);
        $query_string = $_SERVER['REQUEST_URI'];
        preg_match("~\/[^\/]+\/([^\/]+)~",$query_string,$module);
        if(isset($module[1])){
            $this->assign('selected_menu',$module[1]);
        }else{
            $this->assign('selected_menu',false);
        }
        return parent::init($param);
    }

}