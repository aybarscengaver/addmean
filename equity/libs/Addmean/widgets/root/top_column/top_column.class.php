<?php
class top_column extends widget
{
    public function beforeInit(&$params = null)
    {
        $admin_user = isset($_SESSION['admin_user'])?$_SESSION['admin_user']:new stdClass();
        $this->assign('user', $admin_user);
        parent::beforeInit($params);
    }
}
