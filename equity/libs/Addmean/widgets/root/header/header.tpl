<div id="alertMessage" class="error"></div>
<!-- Header -->
<div id="header">
    {if isset( $user->id )}

    <div id="account_info">
        <div class="setting topmenu">
            <b>Diller</b>
            <ul class="subnav">
                <li><a href="?l=">Orjinal</a></li>
                {foreach $languages as $key=>$value}
                    <li><a href="/language/change/{$key}">{$value}</a></li>
                {/foreach}
                <br class="clear"/>
            </ul>
        </div>
        
        <div class="setting topmenu"><b>Kısayollar</b>
            <img src="/assets/root/images/gear.png" class="gear" alt="Hesap Ayarları">
            <ul class="subnav ">
                {if isset( $user->id ) && $user->id==1}
                    <li><a href="/root/settings/parser/.htaccess">.htaccess Güncelle</a></li>
                {/if}
                <li><a href="/root/user/edit/{$user->id}">Hesabım</a></li>
                <br class="clear"/>
            </ul>
        </div>

        <div class="logout" title="Çıkış"><b class="red">{if isset($user) && isset($user->name)  }( {$user->name}
                    {$user->surname} ){/if}</b> <b>Çıkış</b>
            <img src="/assets/root/images/connect.png" name="connect" class="disconnect" alt="disconnect"></div>
    </div>
</div>

{/if}
<!-- End Header -->
<div id="shadowhead"></div>