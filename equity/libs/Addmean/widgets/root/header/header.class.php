<?php
/**
 * User: wyrus
 * Date: 7/24/12
 * Time: 2:34 AM
 * Company: Deli
 */
use Core\Languages;
class header extends  widget{
    public function init(&$param=null){
        $user = isset($_SESSION['admin_user'])?$_SESSION['admin_user']:array();
        $this->assign('user',$user);
        $languages  = m_statics::languages();
        $this->assign('languages',$languages);
        return parent::init($param);
    }
}
