<?php
/**
 * User: mint
 * Date: 7/28/13
 * Time: 11:55 AM
 * File: Languages.php
 * Package: genetik
 */

namespace Core;


class Languages {


    CONST KEY=1;
    CONST CODE=2;
    /**
     * @var \Model
     */
    public $model;
    /**
     * @var Config
     */
    private $config;

    public static $language_key,$models=array();

    /**
     * @description Language class
     * @param \Model $model  Defined model class
     * @param string $language Wanted language key
     */
    public function __construct( &$model , $language = null){
        $this->config = \Core\Config::init();

        if(!$language){
            $language = $this->getLanguage();
        }
        $language_code = $this->getLanguage(self::CODE,$language);

        self::$language_key = $language;
        $this->setLanguage($language);
        $this->addSuffixToModel($model,$language);
        $this->setLocale( $language_code );
        self::$models[] = get_class($model);
    }

    /**
     * @description Set static(po) language files
     * @param string $language Wanted language key
     */
    public  function setLocale($language=null){
        if(!$language){
            $language = $this->config->locale['default'];
        }
        $language = "{$language}.utf8";
        $domain = "messages";
        $locale_dir = $this->config->locale['dir'];
        putenv("LC_ALL={$language}");
        setlocale(LC_ALL, $language);
        bindtextdomain($domain, $locale_dir);
        setlocale(LC_CTYPE,'en_US.UTF-8');

        textdomain($domain);
    }

    /**
     * @description Language checker
     * @param string $key Language key
     * @return bool
     */
    public function checkLang($key=null){
        $langs = m_statics::languages();
        if(array_key_exists($key,$langs)){
            return true;
        }
        return false;
    }

    /**
     * @description Get module table suffix for wanted language
     * @param string $key Language key
     * @return string
     */
    public function getTableSuffix($key=null){
        if(!$key){
            return '';
        }
        if($key=='tr'){
            return ''; // if lang is turkish
        }
        $langs = \m_statics::languages();
        if(array_key_exists($key,$langs)){
            return "_".$key;
        }
        return '';
    }

    /**
     * @description Change model table name for wanted language
     * @param Model $model  Module model class
     * @param string $key Language key
     * @return $this
     */
    public function addSuffixToModel(&$model,$key=''){
        $languages = $model->get_module_languages();
        if(in_array($key,$languages)){
            $table_suffix = $this->getTableSuffix($key);
            $model->set_table_suffix($table_suffix);
            return $this;
        }
        $table_suffix = $this->getTableSuffix(\m_statics::languages($this->config->locale['default']));
        $model->set_table_suffix($table_suffix);
        return $this;
    }

    /**
     * @description Set language key cookie
     * @param string $language
     * @return $this
     */
    public function setLanguage($language='tr'){
        setcookie('language',$language,null,'/');
        return $this;
    }


    public static function get_language_key(){
        return self::$language_key;
    }

    /**
     * @description Get defined language
     * @param int $type
     * @return string
     */
    public function getLanguage($type=self::KEY,$lang=null){
         $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $language = !is_null(\IO::cookie('language'))?\IO::cookie('language'):$lang;
        if(!$language){
            if($browser_lang){
                $language = $browser_lang;
            }else{
                $language = $this->config->locale['default'];
            }
        }
        if($type==self::KEY){
            $language = \m_statics::languages($language);
            $language = \m_statics::languages($language);
        }elseif($type==self::CODE){
            $language = \m_statics::languages($language);
        }
        return $language;
    }
}