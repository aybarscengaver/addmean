<?php
/**
 * User: aybarscengaver
 * Date: 03.04.2013
 * Time: 00:07
 * File: System.php
 * Package: AddMean
 */

namespace Core;
use Core\Config;
class System {
    private $config;
    public  $router;
    public function __construct(){
        $this->config = Config::init();
        if($this->config->system['cms']!=null){
            require_once($this->config->folders['cms'][$this->config->system['cms']].'/'.$this->config->files['cms']['index']);
        }
    }
    public function route(){
        $sys = $this->config->system['cms'];
        $forc = ucfirst($sys);
        $router = $this->config->system['router'];
        $this->setLanguage();

        require_once $this->config->folders['libs'].$router;
        if(!$this->router){
            $class = "$forc\\Router";
            $this->router = new $class();
        }
    }
    private function setLanguage(){
        if(\IO::get('l')){
            $l = \IO::get('l');
            $this->config->set('locale/default',\m_statics::languages($l));
        }
    }
}