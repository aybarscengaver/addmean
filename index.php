<?php
/**
 * User: aybarscengaver
 * Date: 02.04.2013
 * Time: 19:35
 * File: core.php
 * Package: AddMean
 * Description: Sistem başlangıç noktası.
 */
namespace Core;
//Ayar dosyasını yükle
require_once    __DIR__."/config/config.php";

use Core\Config;

try{
//Ayar sınıfını getir
//Ayarları çek
    $config = config::init();
//Sınıf yükleyicileri getir.
    require_once __DIR__."/equity/loaders/autoloader.php";
    include( $config->folders['cms']['libraries']. 'system/core.php' );

    $autoloader = new Autoloader($config->system['cms'],$config->system['framework']);
    $autoloader->load();

    $system = new System();
    $system->route();

}catch(\Exception $e){
    if(\utils::is_ajax()){
        $data = array();
        $data['msg'] = $e->getMessage();
        $data['type'] =  'failed';
        $data['title'] = ('Hata');
        $data['extra'] = array();
        $data = json_encode($data);
        echo $data;
    }else{
        header("Content-type:text/html;charset=utf-8");
        echo "<div style='display:block;width:500px;margin:100px auto; border:1px solid #999;border-radius:6px;
    padding:10px;
    background:#fefefe;text-align:center;font-family:Ubuntu,Arial;font-size:12px;color:#333'>"
            .$e->getMessage()."</div>";
    }
}
