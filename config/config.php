<?php
/**
 * User: aybarscengaver
 * Date: 02.04.2013
 * Time: 19:38
 * File: config.php
 * Package: AddMean
 * Description: Config dizininde bulunan ayar dosyalarını toplayıp bir
 *              dizi değişkene atma konusunda gerekli işlemleri yapar.
 * Example: $config = \Core\Config::init(); print_r($config->database);
 * Version: 0.1
 * Author: Aybars Cengaver < ac@labonmoon.com >
 */
namespace Core;
class Config{
    /**
     * @description Config sınıfının yansısı.
     * @var \Core\Config()
     */
    private static $instance;
    /**
     * @description Tüm ayar dosyalarından çekilen ayarların tutulduğu dizi değişken.
     * @var array
     */
    private $config=array();

    /**
     * @description Ayar dosyalarından ayarları $config dizisine ekler.
     */
    public function __construct(){
        $all_config_files = $this->check_dir();
        foreach($all_config_files as $config_file){
            $this->config[$config_file['class']]=require($config_file['file']);
        }
    }

    /**
     * @description Config sınıfından herhangi bir ayar sınıfını alır.
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name){
        if(array_key_exists($name,$this->config)){
            return $this->config[$name];
        }
        return $this->fromDb($name);
        throw new \Exception("Undefined variable : $name");
    }

    /**
     * @description Geçici olarak en azından tek boyutlu ayarı güncellemeyi sağlıyor @todo
     * @param $var
     * @param $value
     */
    public function set($var,$value){
        $var = explode('/',$var);
        $obj = $var[0];
        $arr = $var[1];
        $this->config[$obj][$arr]=$value;
    }

    /**
     * @description Singleton yapısında sınıfı döndürür.
     * @return Config
     */
    public static function init(){
        if(!self::$instance){
            self::$instance=new config();
        }
        return self::$instance;
    }

    /**
     * @description Kendi dizini içerisinde bulunan tüm ayar dosyalarının bilgileri yükler
     * @return array
     */
    public function check_dir(){
        $config_dir = opendir(__DIR__);
        $files = array();
        while(false!== ($file = readdir($config_dir))){
            $x = explode('.',$file);
            if(count($x)>2){
                if($x[1]=='config'){
                    $files[]=array(
                        'class'=>$x[0],
                        'file'=>__DIR__.'/'.$file,
                    );
                }
            }
        }
        return $files;
    }

    private function fromDb($name){
        $db = \db::singleton();
        $db->table = 'settings';
        $db->where = "`key`='{$name}'";
        $db->limit = 1;
        $option = $db->read();
        if($option){
            return $option->value;
        }
        throw new \Exception("Undefined option !!".$name);
    }
}