<?php
/**
 * User: aybarscengaver
 * Date: 02.04.2013
 * Time: 20:11
 * File: files.config.php
 * Package: AddMean
 * Description: Belirli dosyalar için yol ayarları
 */
$files = array();
$files['cms'] = array();
$files['cms']['index'] = 'index.php';

$files['htaccess'] = $_SERVER['DOCUMENT_ROOT'].'/.htaccess';

return $files;