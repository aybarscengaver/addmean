<?php
/**
 * User: mint
 * Date: 7/12/13
 * Time: 4:55 PM
 * File: soap.php
 * Package: acsplus
 */
return array(
    'uri'=>'http://addmean.int',
    'location'=>'http://addmean.int/api.php',
    'public_datas'=>array(
        'countries',
        'cities',
        'locales',
        'sectors'
    )
);