<?php
/**
 * User: aybarscengaver
 * Date: 02.04.2013
 * Time: 20:12
 * File: folders.config.php
 * Package: AddMean
 * Description: Belirli dizinler için yol ayarları.
 */
$folders = array();

$folders['public_dir'] = '/';
$folders['document_root'] = $_SERVER['DOCUMENT_ROOT'].'/';
$folders['resource_assets'] = 'assets/resources/';
$folders['extends_assets'] = 'assets/';
$folders['libs'] = dirname(__FILE__) . "/../equity/libs/";

$folders['cms'] = array();
$folders['cms']['addmean'] = $folders['libs'] . "Addmean/";
$folders['cms']['libraries'] = $folders['cms']['addmean'] . 'libraries/';


$folders['framework'] = array();
$folders['framework']['laravel'] = $folders['libs'] . 'Laravel/';

$folders['upload_dir'] = array();
$folders['upload_dir']['frontend'] = $folders['public_dir'] . 'uploads/';
$folders['upload_dir']['backend'] = $folders['document_root'] . 'uploads/';
$folders['upload_dir']['origins'] = $folders['upload_dir']['backend'] . 'origins/';
$folders['upload_dir']['origins_frontend'] = $folders['upload_dir']['frontend'] . 'origins/';


$folders['smarty'] = array();
$folders['smarty']['main'] = $folders['cms']['libraries'] . 'thirdparty/smarty/Smarty.class.php';
$folders['smarty']['cache'] = $folders['document_root'] . '/cache/';
$folders['smarty']['compile'] = $folders['document_root'] . '/compile/';
$folders['smarty']['plugins'] = $folders['cms']['libraries'] . 'thirdparty/smarty/custom_plugins/';
$folders['smarty']['themes'] = $folders['cms']['addmean'] . 'templates/';

$folders['widgets'] = $folders['cms']['addmean'] . 'widgets/';

$folders['dom_elements'] = $folders['smarty']['themes'] . 'root/_doms/';

return $folders;