<?php
/**
 * User: aybarscengaver
 * Date: 02.04.2013
 * Time: 21:02
 * File: system.config.php
 * Package: AddMean
 */

$system = array();
$system['framework']='';
$system['cms']='addmean';
$system['router']='Addmean/router.php';
$system['iosecure']=true;
$system['default_module'] = 'index';
$system['charset'] = "utf-8";



return $system;