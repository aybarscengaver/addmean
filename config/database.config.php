<?php
/**
 * User: AddMean Setup Script
 * Date: date
 * File: database.config.php
 * Package: AddMean
 * Description: Veritabanı ayar dosyası
 */

$database = array();
$database['addmean.int'] = array();
$database['addmean.int']['host'] = 'localhost';
$database['addmean.int']['port'] = '3306';
$database['addmean.int']['database'] = 'addmean';
$database['addmean.int']['username'] = 'root';
$database['addmean.int']['password'] = '15420077';
$database['addmean.int']['engine'] = 'mysql';
$database['addmean.int']['charset'] = 'utf8';

$database['default'] = array();
$database['default']['host'] = 'localhost';
$database['default']['port'] = '3306';
$database['default']['database'] = 'addmean';
$database['default']['username'] = 'root';
$database['default']['password'] = '-';
$database['default']['engine'] = 'mysql';
$database['default']['charset'] = 'utf8';

$domain = isset($_SERVER['SERVER_NAME']) && array_key_exists($_SERVER['SERVER_NAME'], $database) ? $_SERVER['SERVER_NAME'] : 'default';

return $database[$domain];
