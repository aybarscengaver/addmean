-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 01, 2013 at 06:31 PM
-- Server version: 5.5.32-0ubuntu0.13.04.1
-- PHP Version: 5.4.9-4ubuntu2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `addmean`
--

-- --------------------------------------------------------

--
-- Table structure for table `archive`
--

CREATE TABLE IF NOT EXISTS `archive` (
  `id` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `content` text COLLATE utf8_turkish_ci,
  `content_id` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_turkish_ci,
  `module_name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name_archive` (`name`) USING BTREE,
  KEY `data_archive` (`data`(333)) USING BTREE,
  KEY `data_title` (`title`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Dumping data for table `archive`
--

INSERT INTO `archive` (`id`, `name`, `title`, `content`, `content_id`, `data`, `module_name`) VALUES
('settings4', 'Ä°letiÅŸim mailleri', '', '', 4, '{"name":"u0130letiu015fim mailleri","key":"admins_emails","group":"1","value":"emre.yilmaz@yandex.com,aybarscengaver@gmail.com","id":"4"}', '');

-- --------------------------------------------------------

--
-- Table structure for table `email_layouts`
--

CREATE TABLE IF NOT EXISTS `email_layouts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `layout` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_queue`
--

CREATE TABLE IF NOT EXISTS `email_queue` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `create_date` varchar(20) COLLATE utf8_turkish_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `body` text COLLATE utf8_turkish_ci NOT NULL,
  `to` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `from` varchar(11) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `file_settings`
--

CREATE TABLE IF NOT EXISTS `file_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  `folder` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `modifier` int(5) DEFAULT NULL,
  `extensions` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `image_height` float(5,0) DEFAULT NULL,
  `image_width` float(5,0) DEFAULT NULL,
  `max_file_size` float(5,0) DEFAULT NULL,
  `rename` enum('0','1') COLLATE utf8_turkish_ci DEFAULT '0',
  `sub_settings` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=14 ;


-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `t` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `seo` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `order` enum('0','1') COLLATE utf8_turkish_ci DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `menus` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `multi_language` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

CREATE TABLE IF NOT EXISTS `module_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `column_name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `module_id` int(10) DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `type` int(3) DEFAULT NULL,
  `values` text COLLATE utf8_turkish_ci,
  `default_value` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `file_alias` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `key_row` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `value_row` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `where_clause` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `modifier` int(10) DEFAULT NULL,
  `required` enum('0','1') COLLATE utf8_turkish_ci DEFAULT '0',
  `list` enum('0','1') COLLATE utf8_turkish_ci DEFAULT NULL,
  `filter` enum('0','1') COLLATE utf8_turkish_ci DEFAULT NULL,
  `edit` enum('0','1') COLLATE utf8_turkish_ci DEFAULT NULL,
  `frontend_form` enum('0','1') COLLATE utf8_turkish_ci DEFAULT '1',
  `description` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `help_text` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `order_no` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

--
-- Table structure for table `module_relations`
--

CREATE TABLE IF NOT EXISTS `module_relations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `master_module` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `slave_module` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `repeat_count` int(3) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `value` text COLLATE utf8_turkish_ci,
  `group` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `key`, `value`, `group`) VALUES
(1, 'SMTP BaÄŸlantÄ± Metni', 'connection_string', 'smtp://test@addmean.com:test@mail.addmean.com:587', 1),
(2, 'Site BaÅŸlÄ±ÄŸÄ±', 'site_title', 'Addmean', 4),
(3, 'Site Adresi', 'site_url', 'http://addmean.com/', 4),
(4, 'Ä°letiÅŸim mailleri', 'admins_emails', 'emre.yilmaz@yandex.com,aybarscengaver@gmail.com', 1),
(5, 'Fiyat GÃ¶sterimi', 'price_status', '1', 4),
(6, 'Sayfa BaÅŸlÄ±ÄŸÄ±', 'pageTitle', 'Addmean', 4),
(7, 'Sayfa AÃ§Ä±klamasÄ±', 'pageDescription', 'Sayfa aÃ§Ä±klamasÄ±', 4),
(8, 'Anahtar Kelimeler', 'pageKeywords', 'a,b,c,d,', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_turkish_ci DEFAULT NULL,
  `status` enum('1','2','3','4') COLLATE utf8_turkish_ci DEFAULT NULL,
  `create_date` varchar(0) COLLATE utf8_turkish_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `level` int(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `status`, `create_date`, `name`, `surname`, `level`) VALUES
(1, 'root@addmean.com', '202cb962ac59075b964b07152d234b70', '2', '', 'Aybars', 'Cengaver', 2);
-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `main_module` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `directory_name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `application` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `status` enum('1','2','3') COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE IF NOT EXISTS `workers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `worker_file` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `status` enum('1','2','3') COLLATE utf8_turkish_ci NOT NULL,
  `interval` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `last_work` varchar(20) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`id`, `name`, `worker_file`, `status`, `interval`, `last_work`) VALUES
(1, 'Email Ä°ÅŸleyici', 'email.worker.php', '2', '*/15 * * * *', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
